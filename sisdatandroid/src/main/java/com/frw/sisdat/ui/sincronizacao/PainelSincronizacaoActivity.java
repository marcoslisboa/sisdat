package com.frw.sisdat.ui.sincronizacao;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.frw.sisdat.R;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.negocio.PesquisaFacade;
import com.frw.sisdat.ui.BaseFragmentActivity;
import com.frw.sisdat.ui.DashboardActivity;
import com.frw.sisdat.ui.service.BaseServiceFragment;
import com.frw.sisdat.ui.service.sincronizacao.SincronizacaoDiariaService;
import com.frw.sisdat.ui.service.sincronizacao.SincronizacaoPesquisaService;
import com.frw.sisdat.ui.util.FuncoesUteis;

import java.util.List;

public class PainelSincronizacaoActivity extends BaseFragmentActivity {

	private SincronizacaoDiariaServiceFragment sincronizacaoDiariaServiceFragment;
	private SincronizacaoPesquisaServiceFragment sincronizacaoPesquisaServiceFragment;
	
	private StateFragment stateFragment;
	
	private ViewFlipper vfTroca;	
	
	private static final int VIEW_FLIPPER_PAINEL = 0;
	private static final int VIEW_FLIPPER_SINCRONIZACAO = 1;
	private TextView textoAguarde;
	
	private static boolean sincronizando = false;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_painel_sincronizacao);
        
        sincronizando = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
        
			final ActionBar ab = getActionBar();
			ab.setDisplayShowTitleEnabled(true);
			ab.setDisplayShowHomeEnabled(true);
			ab.setDisplayHomeAsUpEnabled(true);
			ab.setDisplayUseLogoEnabled(true);
        }
        
		vfTroca = (ViewFlipper) this.findViewById(R.id.vfTroca);
		textoAguarde = (TextView) this.findViewById(R.id.txtSincronizacaoDiaria);
		
		FragmentManager fm = getSupportFragmentManager();

		stateFragment = (StateFragment) fm.findFragmentByTag(StateFragment.TAG);

		if (stateFragment == null) {
			stateFragment = new StateFragment();
			fm.beginTransaction().add(stateFragment, StateFragment.TAG).commit();
		}
		
		try {
			sincronizacaoDiariaServiceFragment = (SincronizacaoDiariaServiceFragment) fm.findFragmentByTag(SincronizacaoDiariaServiceFragment.TAG);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (sincronizacaoDiariaServiceFragment == null) {
			sincronizacaoDiariaServiceFragment = new SincronizacaoDiariaServiceFragment();
			fm.beginTransaction().add(sincronizacaoDiariaServiceFragment, SincronizacaoDiariaServiceFragment.TAG).commit();
		}
		
		try {
			sincronizacaoPesquisaServiceFragment = (SincronizacaoPesquisaServiceFragment) fm.findFragmentByTag(SincronizacaoDiariaServiceFragment.TAG);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (sincronizacaoPesquisaServiceFragment == null) {
			sincronizacaoPesquisaServiceFragment = new SincronizacaoPesquisaServiceFragment();
			fm.beginTransaction().add(sincronizacaoPesquisaServiceFragment, SincronizacaoPesquisaServiceFragment.TAG).commit();
		}

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_sincronizacao, menu);
        return true;
    }

    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {

    	onBackPressed();
    	return true;
    }
    
    /**
     * 
     * @param view
     */
    public void onClickSincronizacaoPesquisa(View view) {

		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(getString(R.string.app_name));
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				
				PesquisaFacade facade = PesquisaFacade.getInstance(PainelSincronizacaoActivity.this);
				List<Pesquisa> list = facade.findAllPesquisaNotSynchronied();
				
				// Testa se possui pesquisas a serem sincronizadas
				if ((list != null) && (list.size() > 0)) {
					sincronizacaoPesquisaServiceFragment.inicializaServico();
				} else {

					final AlertDialog alertDialogPesquisa = new AlertDialog.Builder(PainelSincronizacaoActivity.this).create();
					alertDialogPesquisa.setTitle(getString(R.string.app_name));
					alertDialogPesquisa.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							alertDialogPesquisa.dismiss();
						}
					});
					
					alertDialogPesquisa.setIcon(R.drawable.ic_launcher);

					alertDialogPesquisa.setMessage("Não existem pesquisas a serem sincronizadas!");
					alertDialogPesquisa.show();
				}
			}
		});
		
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.dismiss();
			}
		});
		
		alertDialog.setIcon(R.drawable.ic_launcher);

		alertDialog.setMessage("Confirma o envio das pesquisas para o servidor?");
		alertDialog.show();

    }
    
    /**
     * 
     * @param view
     */
    public void onClickSincronizacaoDiaria(View view) {
    	
		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
		alertDialog.setTitle(getString(R.string.app_name));
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				sincronizacaoDiariaServiceFragment.inicializaServico();
			}
		});
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.dismiss();
			}
		});
		
		alertDialog.setIcon(R.drawable.ic_launcher);

		alertDialog.setMessage("Confirma a importação diária?");
		alertDialog.show();

    }

    /**
     * 
     * @author glaporta
     * Este fragment contem as chamadas para a sincronizacao
     * diaria.
     */
    public static class SincronizacaoDiariaServiceFragment extends BaseServiceFragment {

		public final static String TAG = SincronizacaoDiariaServiceFragment.class.getName();

		private void inicializaServico() {
			sincronizando = true;
			super.inicializaServico(SincronizacaoDiariaService.class);
		}

		@Override
		protected void insereParametrosServico(Intent intent) {
		}

		@Override
		protected void servicoInicializado() {
			sincronizando = true;
			PainelSincronizacaoActivity activity = (PainelSincronizacaoActivity) getActivity();
			activity.servicoSincronizacaoDiariaInicializado();
		}

		@Override
		protected void servicoFinalizadoSucesso() {
			sincronizando = false;
			PainelSincronizacaoActivity activity = (PainelSincronizacaoActivity) getActivity();
			activity.servicoSincronizacaoDiariaSucesso();
		}

		@Override
		protected void servicoFinalizado() {
			sincronizando = false;
			PainelSincronizacaoActivity activity = (PainelSincronizacaoActivity) getActivity();
			activity.servicoSincronizacaoDiariaFinalizado();
		}

	} // Fim do fragment

    /**
     * 
     * @author glaporta
     * Este fragment contem as chamadas para a sincronizacao
     * das pesquisas.
     */
    public static class SincronizacaoPesquisaServiceFragment extends BaseServiceFragment {

		public final static String TAG = SincronizacaoPesquisaServiceFragment.class.getName();
		
		private void inicializaServico() {
			sincronizando = true;
			super.inicializaServico(SincronizacaoPesquisaService.class);
		}

		@Override
		protected void insereParametrosServico(Intent intent) {
		}

		@Override
		protected void servicoInicializado() {
			sincronizando = true;
			PainelSincronizacaoActivity activity = (PainelSincronizacaoActivity) getActivity();
			activity.servicoSincronizacaoPesquisaInicializado();
		}

		@Override
		protected void servicoFinalizadoSucesso() {
			sincronizando = false;
			PainelSincronizacaoActivity activity = (PainelSincronizacaoActivity) getActivity();
			activity.servicoSincronizacaoPesquisaSucesso();
		}

		@Override
		protected void servicoFinalizado() {
			sincronizando = false;
			PainelSincronizacaoActivity activity = (PainelSincronizacaoActivity) getActivity();
			activity.servicoSincronizacaoPesquisaFinalizado();
		}

	} // Fim do fragment

	private void trocarView(int pPagina) {
		vfTroca.setDisplayedChild(pPagina);
	}  
	
	/**
	 * Blocos de servico para sincronizacao diaria
	 */
	private void servicoSincronizacaoDiariaInicializado() {
    	if(stateFragment.ultimaMensagemEnviada != null) {
    		textoAguarde.setText(stateFragment.ultimaMensagemEnviada);
    	} else {
    		textoAguarde.setText("Mantenha o dispositivo imóvel enquanto as tabelas diárias são atualizadas...");
    	}
    	trocarView(VIEW_FLIPPER_SINCRONIZACAO);
    }
    
    private void servicoSincronizacaoDiariaSucesso() {
		trocarView(VIEW_FLIPPER_PAINEL);
		FuncoesUteis.showDialog(this, "Importação diária executada com sucesso!");
    }
    
    private void servicoSincronizacaoDiariaFinalizado() {
		trocarView(VIEW_FLIPPER_PAINEL);
    }    

	/**
	 * Blocos de servico para sincronizacao de pesquisas
	 */
	private void servicoSincronizacaoPesquisaInicializado() {
    	if(stateFragment.ultimaMensagemEnviada != null) {
    		textoAguarde.setText(stateFragment.ultimaMensagemEnviada);
    	} else {
    		textoAguarde.setText("Mantenha o dispositivo imóvel enquanto as pesquisas são sincronizadas...");
    	}
    	trocarView(VIEW_FLIPPER_SINCRONIZACAO);
    }
    
    private void servicoSincronizacaoPesquisaSucesso() {
		trocarView(VIEW_FLIPPER_PAINEL);
		FuncoesUteis.showDialog(this, "Sincronização de pesquisas executada com sucesso!");
    }
    
    private void servicoSincronizacaoPesquisaFinalizado() {
		trocarView(VIEW_FLIPPER_PAINEL);
    }    

    @Override
    protected void onResume() {
    	
    	sincronizando = false;
    	
        IntentFilter filter = new IntentFilter();
        filter.addAction(SincronizacaoDiariaService.ACTION_INTENT_TABLE_NAME);
        registerReceiver(statusImportacaoDiariaReceiver, filter);

        if(sincronizacaoDiariaServiceFragment.servicoExecutando) {
        	servicoSincronizacaoDiariaInicializado();
        }
        
        super.onResume();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(statusImportacaoDiariaReceiver);
        super.onPause();
    }
    
    @Override
    public Object onRetainCustomNonConfigurationInstance() {
    	return textoAguarde.getText().toString();
    }
    
    private BroadcastReceiver statusImportacaoDiariaReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
        	String msg = intent.getStringExtra("MSG");
        	stateFragment.ultimaMensagemEnviada = msg;
        	textoAguarde.setText(msg);
        }
    };    
    
    
    public void onBackPressed() {
    	if (sincronizando) {
    		Toast.makeText(getApplicationContext(), "Sincronizando. Aguarde...", Toast.LENGTH_SHORT).show();
    	} else {
    		sincronizando = false;
    		Intent intent = new Intent(this, DashboardActivity.class);
    		startActivity(intent);
    		finish();
    	}
    };
    
    public static class StateFragment extends Fragment {    
    	
		public final static String TAG = StateFragment.class.getName();    	
    	
    	public String ultimaMensagemEnviada;
    	
    	public StateFragment() {
    		setRetainInstance(true);
		}
    }
}
