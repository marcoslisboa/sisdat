package com.frw.sisdat.ui.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.frw.sisdat.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FuncoesUteis {

	static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private static final int CPF_LENGTH = 11;
    private static final int CNPJ_LENGTH = 14;
	
	private FuncoesUteis() {
		
	}

	private static Typeface mTypefaceLight;

	public static Typeface fontLight(Context pContext) {

		if (mTypefaceLight == null)
			mTypefaceLight = Typeface.createFromAsset(pContext.getAssets(),
					"fonts/Roboto-Light.ttf");

		return mTypefaceLight;

	}

	private static Typeface mTypefaceMedium;

	public static Typeface fontMedium(Context pContext) {

		if (mTypefaceMedium == null)
			mTypefaceMedium = Typeface.createFromAsset(pContext.getAssets(),
					"fonts/Roboto-Medium.ttf");

		return mTypefaceMedium;
	}
	
	public static boolean confereStringNula(String pStr) {
		if (pStr == null || "".equals(pStr.trim()))
			return true;

		return false;
	}

	public static boolean confereStringNula(CharSequence pCharSequence) {
		if (pCharSequence == null || "".equals(pCharSequence))
			return true;

		return false;
	}
	
	private static Toast mToast;

	public static void mostrarToast(FragmentActivity pActivity, int pTextoID) {

		mostrarToast(pActivity, pActivity.getResources().getString(pTextoID));
	} 

	public static void mostrarToast(FragmentActivity pActivity, String pTexto) {

		if (mToast == null) {
			mToast = new Toast(pActivity.getApplicationContext());

			mToast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
			mToast.setDuration(Toast.LENGTH_LONG);

			LayoutInflater inflater = pActivity.getLayoutInflater();

			View layout = inflater.inflate(R.layout.toast_layout,
					(ViewGroup) pActivity.findViewById(R.id.toast_layout_root));

			TextView text = (TextView) layout.findViewById(R.id.txtToast);
			text.setTypeface(fontMedium(pActivity));

			mToast.setView(layout);
		} else
			mToast.cancel(); // para não ficar chamando muitas mensagens Toast
								// de uma vez

		// mToast.setGravity(Gravity.CENTER, msg.getXOffset() / 2,
		// mToast.getYOffset() / 2);

		TextView text = (TextView) mToast.getView().findViewById(R.id.txtToast);
		text.setText(pTexto);

		mToast.show();
	}
	
	
	public static void showDialog(final Context context, final String message) {
		final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(context.getString(R.string.app_name));
		alertDialog.setButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.hide();
			}
		});
		alertDialog.setIcon(R.drawable.ic_launcher);

		alertDialog.setMessage(message);
		alertDialog.show();
	}
	
	public static void showDialog(final Context context, final String titulo, final String message) {
		final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(titulo);
		alertDialog.setButton(context.getString(R.string.ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				alertDialog.hide();
			}
		});
		alertDialog.setIcon(R.drawable.ic_launcher);

		alertDialog.setMessage(message);
		alertDialog.show();
	}
	
	public static Date stringToDate(String dateString) {
		try {
			return sdf.parse(dateString);
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static String formatDate(Date date) {
		return sdf.format(date);
	}
	
    public static String formatCpf(String cpf) {
        if (cpf == null || cpf.length() != CPF_LENGTH) {
            return "";
        }

        StringBuilder cpfFormatado = new StringBuilder(cpf);
        cpfFormatado.insert(9, "-");
        cpfFormatado.insert(6, ".");
        cpfFormatado.insert(3, ".");

        return cpfFormatado.toString();
    }
	
    public static String formatCnpj(String cnpj) {
        if (cnpj == null || cnpj.length() != CNPJ_LENGTH) {
            return "";
        }

        StringBuilder cnpjFormatado = new StringBuilder(cnpj);
        cnpjFormatado.insert(12, "-");
        cnpjFormatado.insert(8, "/");
        cnpjFormatado.insert(5, ".");
        cnpjFormatado.insert(2, ".");

        return cnpjFormatado.toString();
    }
}
