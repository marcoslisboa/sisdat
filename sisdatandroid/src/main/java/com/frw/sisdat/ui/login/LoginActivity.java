package com.frw.sisdat.ui.login;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.Secure;
import android.support.v4.app.FragmentManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.frw.sisdat.R;
import com.frw.sisdat.SettingsActivity;
import com.frw.sisdat.dao.UrlWSDAO;
import com.frw.sisdat.dao.ormlite.DatabaseManager;
import com.frw.sisdat.dominio.Usuario;
import com.frw.sisdat.location.GPSTracker;
import com.frw.sisdat.location.NetService;
import com.frw.sisdat.ui.BaseFragmentActivity;
import com.frw.sisdat.ui.DashboardActivity;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.service.BaseServiceFragment;
import com.frw.sisdat.ui.service.cadastro.CarregarUsuariosService;
import com.frw.sisdat.ui.service.login.LoginService;
import com.frw.sisdat.ui.util.FuncoesUteis;

public class LoginActivity extends BaseFragmentActivity {

	private LoginServiceFragment loginServiceFragment;

	private CarregarUsuariosServiceFragment carregarUsuariosServiceFragment;

	private static final int VIEW_FLIPPER_LOGIN = 0;
	private static final int VIEW_FLIPPER_ESPERANDO = 1;
	private static final int VIEW_FLIPPER_CARREGANDO_USUARIOS = 2;

	private ViewFlipper vfTroca;
	private Button loginButton;
	private Button closeButton;
	private EditText txtLogin;
	private EditText txtSenha;
	private TextView txtIMEI;

	private MobileApplication application;

	private GPSTracker gps;
	
	private String TAG = LoginActivity.class.getSimpleName();

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		application = (MobileApplication) getApplication();

		setContentView(R.layout.activity_login);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			
			final ActionBar ab = getActionBar();

			// set defaults for logo & home up
			ab.setTitle("SISDAT");
			ab.setDisplayShowTitleEnabled(true);
			ab.setDisplayShowHomeEnabled(true);
		}

		DatabaseManager.init(this);

		vfTroca = (ViewFlipper) this.findViewById(R.id.vfTroca);

		txtLogin = (EditText) this.findViewById(R.id.txtLogin);
		txtSenha = (EditText) this.findViewById(R.id.txtSenha);
		txtIMEI = (TextView) this.findViewById(R.id.txtIMEI);

		loginButton = (Button) this.findViewById(R.id.btnLogar);
		loginButton.setOnClickListener(onClickListenerLogin);

		closeButton = (Button) this.findViewById(R.id.btnCancelar);
		closeButton.setOnClickListener(onClickListenerClose);

		FragmentManager fm = getSupportFragmentManager();

		loginServiceFragment = (LoginServiceFragment) fm
				.findFragmentByTag(LoginServiceFragment.TAG);
		if (loginServiceFragment == null) {
			loginServiceFragment = new LoginServiceFragment();
			fm.beginTransaction()
					.add(loginServiceFragment, LoginServiceFragment.TAG)
					.commit();
		}

		carregarUsuariosServiceFragment = (CarregarUsuariosServiceFragment) fm
				.findFragmentByTag(CarregarUsuariosServiceFragment.TAG);
		if (carregarUsuariosServiceFragment == null) {
			carregarUsuariosServiceFragment = new CarregarUsuariosServiceFragment();
			fm.beginTransaction()
					.add(carregarUsuariosServiceFragment,
							CarregarUsuariosServiceFragment.TAG).commit();
		}

		// Popula Objetos de Tela

		// Obtem o IMEI
		try {
			String device = Secure.getString(this.getContentResolver(),Secure.ANDROID_ID);
			txtIMEI.setText("DEVICE: " + device.toUpperCase());
		} catch (Exception e) {
			txtIMEI.setText("-");
		}

		// Testa GPS
		gps = new GPSTracker(this);
		if(!gps.canGetLocation())
			 gps.showSettingsAlert();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu_login, menu);
		return true;
	}

	@Override
	protected void lazyAction() {
		carregarUsuariosServiceFragment.inicializaServico();
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_carregar_usuarios:

			// Testa configuracoes de rede do dispositivo
			if (NetService.isDeviceOnline(this)) {
				// Dispositivo com acesso wifi ou 3G
				ServidorAsynkTask servidorAsynkTask = new ServidorAsynkTask();
				servidorAsynkTask.execute(UrlWSDAO.getInstance(LoginActivity.this).getIp());
			} else {
				dialogMessage(
						"Dispositivo Sem Wifi",
						"Seu dispositivo está fora de uma rede wifi. Caso deseje trabalhar no modo on-line, verifique suas configurações de rede ou clique no botão abaixo para continuar no modo off-line.",
						"Clique aqui para continuar...");
			}

			return true;

		case R.id.menu_settings:
			Intent intent = new Intent(this, SettingsActivity.class);
			startActivity(intent);
			return true;

		case R.id.menu_settings_net:
			startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private OnClickListener onClickListenerLogin = new OnClickListener() {

		@Override
		public void onClick(View v) {
			String usuario = txtLogin.getText().toString();
			String senha = txtSenha.getText().toString();

			if (usuario.trim().equals("")) {
				FuncoesUteis.mostrarToast(LoginActivity.this,
						R.string.login_incorrect_username);
				return;
			}

			if (senha.trim().equals("")) {
				FuncoesUteis.mostrarToast(LoginActivity.this,
						R.string.login_incorrect_password);
				return;
			}

			escondeTeclado();

			loginServiceFragment.inicializaServico(usuario, senha);

		}
	};

	private OnClickListener onClickListenerClose = new OnClickListener() {

		@Override
		public void onClick(View v) {
			setResult(RESULT_OK);

			finish();

			Intent startMain = new Intent(Intent.ACTION_MAIN);
			startMain.addCategory(Intent.CATEGORY_HOME);
			startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(startMain);

			/*android.os.Process.killProcess(android.os.Process.myPid());*/
		}
	};

	public void onBackPressed() {

		finish();

		Intent startMain = new Intent(Intent.ACTION_MAIN);
		startMain.addCategory(Intent.CATEGORY_HOME);
		startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(startMain);

		/*android.os.Process.killProcess(android.os.Process.myPid());*/
	};

	private void trocarView(int pPagina) {
		vfTroca.setDisplayedChild(pPagina);
	}

	private void escondeTeclado() {
		// esconde o teclado depois que clicar em receber
		InputMethodManager inputManager = (InputMethodManager) LoginActivity.this
				.getSystemService(Context.INPUT_METHOD_SERVICE);

		if (LoginActivity.this.getCurrentFocus() != null)
			inputManager.hideSoftInputFromWindow(LoginActivity.this
					.getCurrentFocus().getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
	}

	private void loginSucesso(Usuario u) {
		application.usuarioLogado.set(u);
		Intent intentLogin = new Intent(this, DashboardActivity.class);
		startActivity(intentLogin);
		finish();
	}

	private void loginErro() {
		setResult(RESULT_OK);
		application.usuarioLogado.set(null);

		FuncoesUteis.showDialog(this, "Usuário ou senha inválidos.");
		trocarView(VIEW_FLIPPER_LOGIN);
	}

	private void servicoSucesso() {
		Usuario u = application.usuarioLogado.get();
		if (u != null) {
			loginSucesso(u);
		} else {
			loginErro();
		}
	}

	private void servicoCarregarUsuariosFinalizado() {
		trocarView(VIEW_FLIPPER_LOGIN);
	}

	private void servicoCarregarUsuariosInicializado() {
		escondeTeclado();
		trocarView(VIEW_FLIPPER_CARREGANDO_USUARIOS);
	}

	private void servicoCarregarUsuariosSucesso() {
		FuncoesUteis.showDialog(this, "Usuários carregados com sucesso!");
		trocarView(VIEW_FLIPPER_LOGIN);
	}

	private void servicoFinalizado() {
		trocarView(VIEW_FLIPPER_LOGIN);
	}

	private void servicoInicializado() {
		escondeTeclado();
		trocarView(VIEW_FLIPPER_ESPERANDO);
	}

	/**
	 * Um fragment sem UI, para manter as mudanças de configurações, atualiza a
	 * UI da Activity no LoginService
	 */
	public static class LoginServiceFragment extends BaseServiceFragment {

		public final static String TAG = LoginServiceFragment.class.getName();

		public String usuario;
		public String senha;

		private void inicializaServico(String usuario, String senha) {

			this.usuario = usuario;
			this.senha = senha;

			super.inicializaServico(LoginService.class);
		}

		@Override
		protected void insereParametrosServico(Intent intent) {

			intent.putExtra(LoginService.EXTRA_USUARIO, usuario);
			intent.putExtra(LoginService.EXTRA_SENHA, senha);
		}

		@Override
		protected void servicoInicializado() {

			LoginActivity activity = (LoginActivity) getActivity();
			activity.servicoInicializado();
		}

		@Override
		protected void servicoFinalizadoSucesso() {
			LoginActivity activity = (LoginActivity) getActivity();
			activity.servicoSucesso();
		}

		@Override
		protected void servicoFinalizado() {
			LoginActivity activity = (LoginActivity) getActivity();
			activity.servicoFinalizado();
		}

	}

	/**
	 * Um fragment sem UI, para manter as mudanças de configurações, atualiza a
	 * UI da Activity no CarregarUsuariosService
	 */
	public static class CarregarUsuariosServiceFragment extends
			BaseServiceFragment {

		public final static String TAG = CarregarUsuariosServiceFragment.class
				.getName();

		private void inicializaServico() {
			super.inicializaServico(CarregarUsuariosService.class);
		}

		@Override
		protected void insereParametrosServico(Intent intent) {
		}

		@Override
		protected void servicoInicializado() {
			LoginActivity activity = (LoginActivity) getActivity();
			activity.servicoCarregarUsuariosInicializado();
		}

		@Override
		protected void servicoFinalizadoSucesso() {
			LoginActivity activity = (LoginActivity) getActivity();
			activity.servicoCarregarUsuariosSucesso();
		}

		@Override
		protected void servicoFinalizado() {
			LoginActivity activity = (LoginActivity) getActivity();
			activity.servicoCarregarUsuariosFinalizado();
		}

	}

	@Override
	protected void onResume() {

		if (loginServiceFragment.servicoExecutando) {
			trocarView(VIEW_FLIPPER_ESPERANDO);
		}

		if (carregarUsuariosServiceFragment.servicoExecutando) {
			trocarView(VIEW_FLIPPER_CARREGANDO_USUARIOS);
		}

		super.onResume();
	}

}