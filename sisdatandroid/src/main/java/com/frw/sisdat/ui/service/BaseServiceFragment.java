package com.frw.sisdat.ui.service;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;

import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.util.FuncoesUteis;

import java.util.Map;

public abstract class BaseServiceFragment extends Fragment implements
		ResultReceiverEx.Receiver {

	private ResultReceiverEx mReceiver;

	private Long mServicoID;
	
	public Boolean servicoExecutando = false;
	
	// variáveis para controlar a volta da activity de um pause
	private boolean mActivityPaused = false;
	private Integer mServiceStatus;
	private String mServiceMensagem;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		mReceiver = new ResultReceiverEx(new Handler());
		mReceiver.setReceiver(this);
	}

	// @Override
	// public void onAttach(Activity activity) {
	// super.onAttach(activity);
	//
	// if (!(activity instanceof ActivityServiceInterface)) {
	// throw new ClassCastException(activity.toString()
	// + " deve implementar ActivityServiceInterface");
	// }

	// try {
	// mListener = (OnBarraOKListener) activity;
	// } catch (ClassCastException e) {
	// throw new ClassCastException(activity.toString()
	// + " must implement OnBarraOKListener");
	// }
	// }

	protected void inicializaServico(Class<?> servicoClass) {

		mServicoID = System.currentTimeMillis();
		servicosStatus().put(mServicoID, BaseService.STATUS_INICIADO);

		final Intent intent = new Intent(Intent.ACTION_SYNC, null,
				getActivity(), servicoClass);

		intent.putExtra(BaseService.EXTRA_STATUS_RECEIVER, mReceiver);
		intent.putExtra(BaseService.EXTRA_SERVICO_ID, mServicoID);

		insereParametrosServico(intent);

		servicoInicializado();

		servicoExecutando = true;
		getActivity().startService(intent);
	}

	protected abstract void insereParametrosServico(Intent intent);
	protected abstract void servicoInicializado();
	protected abstract void servicoFinalizadoSucesso();
	protected abstract void servicoFinalizado();
	
	private void cancelaServico() {

		servicoExecutando = false;
		
		if (mServicoID == null) // nenhum serviço foi
								// realizado
			return;

		Map<Long, Integer> servicosStatusTemp = servicosStatus();

		if (!servicosStatusTemp.containsKey(mServicoID))
			return;

		if (servicosStatusTemp.get(mServicoID) == BaseService.STATUS_INICIADO)
			servicosStatusTemp.put(mServicoID, BaseService.STATUS_CANCELADO);
	}

	/** {@inheritDoc} */
	public void onReceiveResult(int resultCode, Bundle resultData) {

		servicoExecutando = false;
		
		if (getActivity() == null) {
			return;
		}

		if (mActivityPaused) {

			// Se a atividade estiver pausada deve guardar os valores dentro
			// dela mesmo para retornar depois do OnResume
			mServiceStatus = resultCode;

			if (resultCode == BaseService.STATUS_ERRO
					|| resultCode == BaseService.STATUS_DESLOGADO) {
				mServiceMensagem = resultData.getString(Intent.EXTRA_TEXT);
			}

		} else {
			if (resultCode == BaseService.STATUS_ERRO
					|| resultCode == BaseService.STATUS_DESLOGADO) {

				FuncoesUteis.showDialog(getActivity(), resultData.getString(Intent.EXTRA_TEXT));
				
				servicoFinalizado();

			} else if (resultCode == BaseService.STATUS_CONCLUIDO) {

				servicoFinalizadoSucesso();
			}
		}

		mServicoID = null;
	}

	@Override
	public void onResume() {

		super.onResume();

		// checa se o serviço finalizou

		retornaActivity();

	}

	@Override
	public void onPause() {

		super.onPause();

		if (getActivity() != null && getActivity().isFinishing()) // Reconhece
																	// que a
																	// atividade
																	// está
		// fechando por
		// causa
		// de um finish.
		{

			cancelaServico();
			mReceiver.setReceiver(null);

		} else {
			mActivityPaused = true;
		}
	}

	private void retornaActivity() {

		if (!mActivityPaused)
			return;

		mActivityPaused = false;

		if (getActivity() == null) {
			return;
		}

		if (mServiceStatus != null) {

			if (mServiceStatus == BaseService.STATUS_CONCLUIDO) {

				servicoFinalizadoSucesso();

			} else if (mServiceStatus == BaseService.STATUS_ERRO
					|| mServiceStatus == BaseService.STATUS_DESLOGADO) {

				FuncoesUteis.mostrarToast(getActivity(), mServiceMensagem);
				servicoFinalizado();
			}

			mServiceStatus = null;
			mServiceMensagem = null;
		}

	}

	public Map<Long, Integer> servicosStatus() {

		return ((MobileApplication) getActivity().getApplication()).servicosStatus;
	}

}
