package com.frw.sisdat.ui.service.cadastro;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Base64;

import com.frw.sisdat.dominio.AbstractColeta;
import com.frw.sisdat.dominio.ColetaOpiniao;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.dominio.PesquisaFoto;
import com.frw.sisdat.negocio.PesquisaFacade;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ws.JsonHandler;

public class EnviarPesquisaOpiniaoWSHandler extends JsonHandler {

	private MobileApplication application;
	private Long servicoID;
	private Context context;
	
	private Pesquisa pesquisa;

	private SimpleDateFormat dateHourFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

	public EnviarPesquisaOpiniaoWSHandler(Long servicoID, MobileApplication application, Pesquisa pesquisa, Context context) {
		super();

		this.servicoID = servicoID;
		this.application = application;
		this.pesquisa = pesquisa;
		this.context = context;
	}

	@Override
	public void parseAndInsert(JSONObject pJSONObject) throws JsonHandlerException, IOException, JSONException {
		
		
		String status = pJSONObject.getString("status");
		
		if (status.equals("SINCRONIZADO")) {
		
			System.out.println("pesquisas sucesso true");
			
			String dataSincronizacao = pJSONObject.getString("dataSincronizacao");

			PesquisaFacade facade = PesquisaFacade.getInstance(context);
			try {
				pesquisa.setDataSincronizacao(dateHourFormat.parse(dataSincronizacao));
			} catch (Exception e) {
				e.printStackTrace();
				pesquisa.setDataSincronizacao(new Date());
			}
			
			facade.Synchronize(pesquisa);
			
		} else {
			System.out.println("pesquisas sucesso false");
		}
		
	}

	@Override
	public List<NameValuePair> getPostParameters() throws JSONException {

		System.out.println("Enviando pesquisa...");
		
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		
		// Prepara o objeto pesquisa
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("id", pesquisa.getId());
		jsonObject.put("dataAbertura", dateHourFormat.format(pesquisa.getDataAbertura()));
		jsonObject.put("dataFehamento", dateHourFormat.format(pesquisa.getDataFechamento()));
		jsonObject.put("latitudeInicial", pesquisa.getLatitudeInicial());
		jsonObject.put("longitudeInicial", pesquisa.getLongitudeInicial());
		jsonObject.put("latitudeFinal", pesquisa.getLatitudeFinal());
		jsonObject.put("longitudeFinal", pesquisa.getLongitudeFinal());
		jsonObject.put("usuario", pesquisa.getUsuario().getCodigo());
		jsonObject.put("codigoQuestionario", pesquisa.getQuestionario().getCodigo());
		jsonObject.put("device", pesquisa.getDevice());
		jsonObject.put("fechamento", pesquisa.getFechamento());

		/**
		 * @TODO: implementar envio de fotos 
		 * */
		if (pesquisa.fotos != null && !pesquisa.fotos.isEmpty()) {
			JSONArray jsonArrayFotos = new JSONArray();
			for (PesquisaFoto pesquisaFoto : pesquisa.fotos) {
				JSONObject jsonObjectFoto = new JSONObject();
				jsonObjectFoto.put("foto", Base64.encodeToString(pesquisaFoto.getFoto(), Base64.DEFAULT));
				jsonArrayFotos.put(jsonObjectFoto);
			}
			jsonObject.put("fotos", jsonArrayFotos);
		}

		if ((pesquisa.getObservacao() != null) && (!pesquisa.getObservacao().equals("")))
				jsonObject.put("observacao", pesquisa.getObservacao());
		
		// Monta as coletas para a pesquisa
		JSONArray jsonArray = new JSONArray();
		
		for (AbstractColeta ac: pesquisa.getColetas()) {
			
			ColetaOpiniao c = (ColetaOpiniao)ac;
			
			JSONObject jsonObjectColeta = new JSONObject();
			
			jsonObjectColeta.put("id", c.getId());
			jsonObjectColeta.put("pergunta", c.getPergunta().getId());

			if (c.getCampoLivre() == null)
				jsonObjectColeta.put("campoLivre", "");
			else
				jsonObjectColeta.put("campoLivre", c.getCampoLivre());
			
			if (c.getResposta() != null)
				jsonObjectColeta.put("resposta", c.getResposta().getId());
			else
				jsonObjectColeta.put("resposta", "");
			
			jsonArray.put(jsonObjectColeta);
		}
		
		jsonObject.put("coletas", jsonArray);
		
		params.add(new BasicNameValuePair("json", jsonObject.toString()));
		
		return params;

	}
	
}
