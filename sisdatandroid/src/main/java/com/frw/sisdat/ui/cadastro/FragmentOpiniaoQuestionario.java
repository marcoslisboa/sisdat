package com.frw.sisdat.ui.cadastro;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.frw.sisdat.R;
import com.frw.sisdat.adapter.PerguntaRespostaAdapter;
import com.frw.sisdat.dominio.AbstractColeta;
import com.frw.sisdat.dominio.ColetaOpiniao;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.negocio.PerguntaRespostaFacade;
import com.frw.sisdat.ui.MobileApplication;

@SuppressLint("ValidFragment")
public class FragmentOpiniaoQuestionario extends android.support.v4.app.Fragment{
	
	public static String TAG_FRAGMENT = "forcavendamobile.fragment.pedido";
	
	private PerguntaRespostaAdapter perguntaRespostaAdapter;

	private MobileApplication application;
	private Pesquisa pesquisa;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_opiniao_questionario, container, false);
		application = ((MobileApplication)getActivity().getApplication());
		pesquisa = ((PesquisaOpiniaoActivity)getActivity()).pesquisa;
		
		ListView listViewPerguntas = (ListView) root.findViewById(R.id.listViewPerguntas);
		listViewPerguntas.setFocusable(false);
		listViewPerguntas.setClickable(false);
		listViewPerguntas.setFocusable(false);
		
		application.listPerguntas = PerguntaRespostaFacade.getInstance(getActivity()).buscarTodasPerguntas(pesquisa.getQuestionario());

		if (pesquisa.getId() != null && pesquisa.getColetas() != null && !pesquisa.getColetas().isEmpty()) {
			try {
				for (Pergunta pergunta : application.listPerguntas) {
					for (AbstractColeta item : pesquisa.getColetas()) {
						ColetaOpiniao coleta = (ColetaOpiniao) item;
						if (pergunta.getId().equals(coleta.getPergunta().getId())) {
							pergunta.getRespostas().add(coleta.getResposta());
							pergunta.setCampoLivre(coleta.getCampoLivre());
						}
						
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		perguntaRespostaAdapter = new PerguntaRespostaAdapter(getActivity(), application.listPerguntas, getActivity().getSupportFragmentManager());
		
		listViewPerguntas.setAdapter(perguntaRespostaAdapter);

		return root;
	}

	public PerguntaRespostaAdapter getPerguntaRespostaAdapter() {
		return perguntaRespostaAdapter;
	}

	public void setPerguntaRespostaAdapter(
			PerguntaRespostaAdapter perguntaRespostaAdapter) {
		this.perguntaRespostaAdapter = perguntaRespostaAdapter;
	}


}
