package com.frw.sisdat.ui.service.cadastro;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.frw.sisdat.dominio.Usuario;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.service.BaseService;
import com.frw.sisdat.ws.GsonHandler;
import com.google.gson.Gson;

public class CarregarUsuariosWSHandler extends GsonHandler {

	private MobileApplication application;

	private Long servicoID;

	public CarregarUsuariosWSHandler(Long servicoID, MobileApplication application) {

		this.servicoID = servicoID;
		this.application = application;
	}

	@Override
	public void parse(Gson gson, String resultado) {

		try {
			if (application.servicosStatus.get(servicoID) == BaseService.STATUS_CANCELADO) {
				return;
			}

			List<Usuario> usuarios = new ArrayList<Usuario>();

			JSONArray jsonArray = null;
			
			if (resultado.contains("UsuarioWSDTO")) {
				JSONObject jsonObject1 = new JSONObject(resultado);
				jsonArray = jsonObject1.getJSONArray("UsuarioWSDTO");
			} else {
				jsonArray = new JSONArray(resultado);
			}	

			for (int i = 0; i < jsonArray.length(); i++) {
				
				JSONObject jsonObject = null;
				
				jsonObject = jsonArray.getJSONObject(i);

				Usuario u = new Usuario();
				
				u.setCodigo(jsonObject.getString("codigo"));
				u.setLogin(jsonObject.getString("login"));
				u.setNome(jsonObject.getString("nome"));
				u.setPassword(jsonObject.getString("password"));
				
				usuarios.add(u);

			}
			
			dataMapper.put("usuarios", usuarios);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
