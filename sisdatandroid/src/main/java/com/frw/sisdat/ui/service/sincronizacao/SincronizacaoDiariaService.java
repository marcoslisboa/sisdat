package com.frw.sisdat.ui.service.sincronizacao;

import android.content.Intent;

import com.frw.sisdat.R;
import com.frw.sisdat.dao.cadastro.ControleSincronizacao;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Questionario;
import com.frw.sisdat.dominio.Resposta;
import com.frw.sisdat.dominio.Usuario;
import com.frw.sisdat.negocio.ControleSincronizacaoFacade;
import com.frw.sisdat.negocio.PerguntaFacade;
import com.frw.sisdat.negocio.QuestionarioFacade;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.service.BaseService;
import com.frw.sisdat.ui.service.cadastro.CarregarPerguntasWSHandler;
import com.frw.sisdat.ui.service.cadastro.CarregarQuestionariosWSHandler;
import com.frw.sisdat.ui.service.cadastro.CarregarRespostasWSHandler;
import com.frw.sisdat.ws.GsonHandler.JsonHandlerException;

import java.util.Date;
import java.util.List;

public class SincronizacaoDiariaService extends BaseService {

	private static final String TAG = SincronizacaoDiariaService.class.getName();
	
	public static final String ACTION_INTENT_TABLE_NAME = "com.framework.quiz.intente.action.msg";

	public SincronizacaoDiariaService() {
		super(TAG);
	}

	@Override
	protected void executarServico(Intent intent) {
		
		MobileApplication application = (MobileApplication) getApplication();

		System.out.println("Iniciando sincronização diária...");

		// Busca de Tipos de Pesquisa
		try {
			System.out.println("Carregando Questionarios...");
			
			atualizarStatusActivity("Buscando Questionarios...");
			
			CarregarQuestionariosWSHandler handler = new CarregarQuestionariosWSHandler(servicoID, application);

			Usuario usuario = application.usuarioLogado.get();
			if(usuario != null && usuario.getCodigo() != null){
				mRemoteExecutorGSON.executeGet(WS_URL + "ws/quiz/buscarQuestionariosValidos?idUsuario=" + usuario.getCodigo(), handler);
			} else {
				mRemoteExecutorGSON.executeGet(WS_URL + "ws/quiz/buscarQuestionarios", handler);
			}
			
			atualizarStatusActivity("Atualizando banco de dados de questionarios...");
			
			//chama o Bussines passando a lista de usuarios
			QuestionarioFacade.getInstance(getApplicationContext()).deleteQuestionarios();
			QuestionarioFacade.getInstance(getApplicationContext()).insereQuestionarios((List<Questionario>) handler.getData("questionarios"));
			
			//release memory
			handler.dataMapper.clear();
			
			atualizarStatusActivity("Questionarios atualizados.");
			
			System.out.println("Questionarios carregados com sucesso!");
			
		} catch (JsonHandlerException e) {
			//Tratar erros 
			enviaMensagemReceiver(e);
			return;
		}

		// Busca de Perguntas
		try {
			System.out.println("Carregando Perguntas...");
			
			atualizarStatusActivity("Buscando Perguntas...");
			
			CarregarPerguntasWSHandler handler = new CarregarPerguntasWSHandler(servicoID, application);
			mRemoteExecutorGSON.executeGet(WS_URL + "ws/quiz/buscarPerguntas", handler);
			
			atualizarStatusActivity("Atualizando banco de dados de perguntas...");
			
			//chama o Bussines passando a lista de usuarios
			PerguntaFacade.getInstance(getApplicationContext()).inserePerguntas((List<Pergunta>) handler.getData("perguntas"));
			
			//release memory
			handler.dataMapper.clear();
			
			atualizarStatusActivity("Perguntas atualizadas.");
			
			System.out.println("Perguntas carregadas com sucesso!");
			
		} catch (JsonHandlerException e) {
			//Tratar erros 
			enviaMensagemReceiver(e);
			return;
		}

		// Busca de Respostas
		try {
			System.out.println("Carregando Respostas...");
			
			atualizarStatusActivity("Buscando Respostas...");
			
			CarregarRespostasWSHandler handler = new CarregarRespostasWSHandler(servicoID, application);
			mRemoteExecutorGSON.executeGet(WS_URL + "ws/quiz/buscarRespostas", handler);
			
			atualizarStatusActivity("Atualizando banco de dados de respostas...");
			
			//chama o Bussines passando a lista de respostas
			PerguntaFacade.getInstance(getApplicationContext()).insereRespostas((List<Resposta>) handler.getData("respostas"));
			
			//release memory
			handler.dataMapper.clear();
			
			atualizarStatusActivity("Respostas atualizadas.");
			
			System.out.println("Respostas carregadas com sucesso!");
			
		} catch (JsonHandlerException e) {
			//Tratar erros 
			enviaMensagemReceiver(e);
			return;
		}

		

		atualizarStatusActivity("Liberando acesso off-line.");
		
		ControleSincronizacao cs = new ControleSincronizacao();
		cs.dataUltimaSincronizacao = new Date();
		
		ControleSincronizacaoFacade.getInstance(getApplicationContext()).salva(cs);
		
		System.out.println("Sincronização diária finalizada!");
		
		enviaMensagemReceiverConcluido();
	}

	private void atualizarStatusActivity(String nome) {
		Intent broadcast = new Intent();
		broadcast.putExtra("MSG", nome);
        broadcast.setAction(ACTION_INTENT_TABLE_NAME);
        sendBroadcast(broadcast);		
	}
	
	@Override
	protected String mensagemErroGenerica() {
		return getResources().getString(R.string.sincronizacao_msg_problemas);
	}

}
