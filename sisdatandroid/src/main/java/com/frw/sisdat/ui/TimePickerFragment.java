package com.frw.sisdat.ui;

import java.util.Calendar;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;

public abstract class TimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current time as the default values for the picker
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute, true);
    }

	@Override
	public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
		
		int[] horaMinuto = new int[2];
		horaMinuto[0] = hourOfDay;
		horaMinuto[1] = minute;
		
		onChanged(horaMinuto);
	}
	
	public abstract void onChanged(int[] horaMinuto);

}
