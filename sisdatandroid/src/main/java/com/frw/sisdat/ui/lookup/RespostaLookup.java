package com.frw.sisdat.ui.lookup;

import java.util.List;

import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.frw.sisdat.adapter.RespostaAdapter;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Resposta;
import com.frw.sisdat.dto.RespostaDTO;

public class RespostaLookup extends AbstractLookup<Resposta, RespostaDTO>{

	private RespostaAdapter adapter;
	public RespostaDTO dto;
	private Pergunta pergunta;

	public RespostaLookup(EditText editText, Pergunta pergunta) {
		editTextLookup = (EditText) editText;
		dto = new RespostaDTO();
		this.pergunta = pergunta;
	}

	@Override
	protected String getTitle() {

		if ((pergunta.getDescricaoResumida() != null) && (!pergunta.getDescricaoResumida().trim().equals("")))
			return pergunta.getDescricaoResumida();
		
		return pergunta.getDescricao();
	}

	@Override
	public List<Resposta> loadData(RespostaDTO dto) {
		if(adapter != null){
			adapter.clear();
			adapter.addAll(pergunta.getOpcoes());
			adapter.notifyDataSetChanged();
		}
		return pergunta.getOpcoes();
	}

	@Override
	protected ArrayAdapter<Resposta> getAdapter() {
		if (adapter == null) {
			adapter = new RespostaAdapter(context, loadData(dto));
		}
		return adapter;
	}

	@Override
	protected void onItemSelected(Resposta item) {
		editTextLookup.setText(item.getDescricao());
	}
	
	@Override
	public void setItemSelecionado(Resposta item) {
		itemSelecionado = item;
		editTextLookup.setText(item.getDescricao());
	}

	@Override
	protected LinearLayout getFilterFormView() {
		return null;
	}
	

}
