package com.frw.sisdat.ui.service.cadastro;

import java.util.List;

import android.content.Intent;

import com.frw.sisdat.R;
import com.frw.sisdat.dominio.Usuario;
import com.frw.sisdat.negocio.UsuarioFacade;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.service.BaseService;
import com.frw.sisdat.ws.GsonHandler.JsonHandlerException;

public class CarregarUsuariosService extends BaseService {

	private static final String TAG = CarregarUsuariosService.class.getName();

	public CarregarUsuariosService() {
		super(TAG);
	}

	@Override
	protected void executarServico(Intent intent) {
		
		MobileApplication application = (MobileApplication) getApplication();

		try {
			CarregarUsuariosWSHandler handler = new CarregarUsuariosWSHandler(servicoID, application);
			mRemoteExecutorGSON.executeGet(WS_URL + "ws/quiz/buscarUsuarios", handler);
			
			//chama o Bussines passando a lista de usuarios
			UsuarioFacade.getInstance(getApplicationContext()).insereUsuarios((List<Usuario>) handler.getData("usuarios"));
			
		} catch (JsonHandlerException e) {
			enviaMensagemReceiver(e);
			return;
		}
		
		enviaMensagemReceiverConcluido();
	}

	@Override
	protected String mensagemErroGenerica() {
		return getResources().getString(R.string.sincronizacao_msg_problemas);
	}

}
