package com.frw.sisdat.ui;
//package com.framework.forcavendasmobile.ui;
//
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;
//
//import com.framework.forcavendasmobile.R;
//import com.google.android.gms.maps.CameraUpdateFactory;
//import com.google.android.gms.maps.GoogleMap;
//import com.google.android.gms.maps.MapFragment;
//import com.google.android.gms.maps.model.LatLng;
//import com.google.android.gms.maps.model.Marker;
//import com.google.android.gms.maps.model.MarkerOptions;
//import com.google.android.maps.MapActivity;
//
//public class MapVisitasActivity extends MapActivity {
//	
//	private LatLng frameworkSystemLocation = new LatLng(-19.92550, -43.64058);
//	private GoogleMap map;
//
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_map_visitas);
//        getActionBar().setDisplayHomeAsUpEnabled(true);
//        
//        map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
//                .getMap();
//        map.setMyLocationEnabled(true);
//        if (map.getMyLocation() != null) {
//        	frameworkSystemLocation = new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude());
//        }
//        Marker frameworkSystem = map.addMarker(new MarkerOptions()
//                                                   .position(frameworkSystemLocation)
//                                                   .title("Framework System"));
//         // Move a câmera para Framework System com zoom 15.
//        if (map.getMyLocation() != null) {
//        	map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(map.getMyLocation().getLatitude(), map.getMyLocation().getLongitude()) , 10));
//        	map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.activity_map_visitas, menu);
//        return true;
//    }
//
//    
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        switch (item.getItemId()) {
//            case android.R.id.home:
//            	Intent intent = new Intent(this, DashboardActivity.class);
//        		startActivity(intent);
//        		finish();
//                return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
//
//	@Override
//	protected boolean isRouteDisplayed() {
//		// TODO Auto-generated method stub
//		return false;
//	}
//
//
//	@Override
//	public void onBackPressed() {
//		Intent intent = new Intent(this, DashboardActivity.class);
//		startActivity(intent);
//		finish();
//	}
//
//}
