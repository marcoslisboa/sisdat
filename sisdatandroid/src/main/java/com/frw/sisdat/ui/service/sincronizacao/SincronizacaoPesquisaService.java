package com.frw.sisdat.ui.service.sincronizacao;

import android.content.Intent;

import com.frw.sisdat.R;
import com.frw.sisdat.dao.cadastro.ControleSincronizacao;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.negocio.ControleSincronizacaoFacade;
import com.frw.sisdat.negocio.PesquisaFacade;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.service.BaseService;
import com.frw.sisdat.ui.service.cadastro.EnviarPesquisaOpiniaoWSHandler;

import java.util.Date;
import java.util.List;

public class SincronizacaoPesquisaService extends BaseService {

	private static final String TAG = SincronizacaoPesquisaService.class
			.getName();

	public static final String ACTION_INTENT_TABLE_NAME = "com.framework.quiz.intente.action.msg";

	public SincronizacaoPesquisaService() {
		super(TAG);
	}

	@Override
	protected void executarServico(Intent intent) {

		MobileApplication application = (MobileApplication) getApplication();

		System.out.println("Iniciando sincronização das pesquisas...");

		try {

			atualizarStatusActivity("Atualizando pesquisas...");

			PesquisaFacade facade = PesquisaFacade
					.getInstance(getApplicationContext());

			// Busca todas as pesquisas não sincronizadas
			List<Pesquisa> pesquisas = facade.findAllPesquisaNotSynchronied();

			if ((pesquisas != null) && (pesquisas.size() > 0)) {

				for (Pesquisa pesquisa : pesquisas) {

					// Fecha pesquisa de forma abrupta
					if (pesquisa.getDataFechamento() == null)
						facade.fecharPesquisaManualmente(pesquisa, "A");

					if ((pesquisa.getColetas() != null)
							&& (pesquisa.getColetas().size() > 0)) {

						// Define o handle correto para cada tipo de pesquisa
						EnviarPesquisaOpiniaoWSHandler handler = new EnviarPesquisaOpiniaoWSHandler(
								servicoID, application, pesquisa,
								getApplicationContext());
						mRemoteExecutorJSON.executePost(WS_URL + "ws/quiz/enviarPesquisa", handler);

						atualizarStatusActivity("Pesquisa enviada com sucesso!");

					} else {
						try {
							// Remove a pesquisa do banco caso não
							// possua coletas
							atualizarStatusActivity("Removendo pesquisa sem coletas");

							facade.removePesquisa(pesquisa);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}

					// release memory
					// handler.dataMapper.clear();
				}
			} else {
				// Informa ao usu�rio que n�o existem pesquisas
				// a serem sincronizadas
				atualizarStatusActivity("Nenhuma pesquisa disponível para ser sincronizada");
			}

		} catch (Exception e) {
			// Tratar erros
			atualizarStatusActivity("Ocorreu uma exceção ao enviar a pesquisa!");
			e.printStackTrace();
			enviaMensagemReceiver(e);
			return;
		}

		// Sincroniza Pesquisas
		atualizarStatusActivity("Liberando acesso off-line.");

		ControleSincronizacao cs = new ControleSincronizacao();
		cs.dataUltimaSincronizacao = new Date();

		ControleSincronizacaoFacade.getInstance(getApplicationContext()).salva(
				cs);

		System.out.println("Sincronização de pesquisas finalizada!");

		enviaMensagemReceiverConcluido();
	}

	private void atualizarStatusActivity(String nome) {
		Intent broadcast = new Intent();
		broadcast.putExtra("MSG", nome);
		broadcast.setAction(ACTION_INTENT_TABLE_NAME);
		sendBroadcast(broadcast);
	}

	@Override
	protected String mensagemErroGenerica() {
		return getResources().getString(R.string.sincronizacao_msg_problemas);
	}

}
