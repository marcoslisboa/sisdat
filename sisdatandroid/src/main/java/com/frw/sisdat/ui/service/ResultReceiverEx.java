package com.frw.sisdat.ui.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;

public class ResultReceiverEx extends ResultReceiver {

	private static final String TAG = "ResultReceiverEx";

	private Receiver mReceiver;

	public ResultReceiverEx(Handler handler) {
		super(handler);
	}

	public void setReceiver(Receiver receiver) {
		mReceiver = receiver;
	}

	public interface Receiver {
		public void onReceiveResult(int resultCode, Bundle resultData);
	}

	@Override
	protected void onReceiveResult(int resultCode, Bundle resultData) {
		if (mReceiver != null) {
			mReceiver.onReceiveResult(resultCode, resultData);
		} else {
			Log.w(TAG, "Deixando resultado para trás no código " + resultCode + ": "
					+ resultData.toString());
		}
	}
}