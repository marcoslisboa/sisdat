package com.frw.sisdat.ui.cadastro;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.Gallery.LayoutParams;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import com.frw.sisdat.R;

public class FotosActivity extends Activity implements AdapterView.OnItemSelectedListener, ViewSwitcher.ViewFactory{

	private Uri imageUri;
	private Bitmap fotoBitmap;
	private List<Bitmap> fotos = new ArrayList<Bitmap>();
	
	private static final int CAMERA_REQUEST = 1888;
	private ImageAdapter imageAdapter;
	
	private ImageSwitcher mSwitcher;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fotos);
        
        mSwitcher = (ImageSwitcher) findViewById(R.id.switcher);
        mSwitcher.setFactory(this);
        mSwitcher.setInAnimation(AnimationUtils.loadAnimation(this,
                android.R.anim.fade_in));
        mSwitcher.setOutAnimation(AnimationUtils.loadAnimation(this,
                android.R.anim.fade_out));
 
        Gallery g = (Gallery) findViewById(R.id.gallery);
        imageAdapter = new ImageAdapter(this);
        g.setAdapter(imageAdapter);
        g.setOnItemSelectedListener(this);
        
        ImageView btnTirarFoto = (ImageView) findViewById(R.id.btnTirarFoto);
        btnTirarFoto.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				ContentValues values = new ContentValues();
				values.put(MediaStore.Images.Media.TITLE, "foto");
				values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
				imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
				startActivityForResult(intent, CAMERA_REQUEST);
			}
		});
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	switch (item.getItemId()) {
		case R.id.menu_settings:
			ContentValues values = new ContentValues();
			values.put(MediaStore.Images.Media.TITLE, "foto");
			values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
			imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
			startActivityForResult(intent, CAMERA_REQUEST);
			return true;
			
		default:
			return true;
		}
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    	switch (requestCode) {

        case CAMERA_REQUEST:
            if (requestCode == CAMERA_REQUEST) {
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Bitmap photoBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
                        Bitmap bitmap = resizeImage(photoBitmap, 640);
                        fotoBitmap = bitmap;
                        fotos.add(bitmap);
                        imageAdapter.notifyDataSetChanged();
                        
                        
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    
    private Bitmap resizeImage(Bitmap bitmap, int newSize){
        int width = bitmap.getWidth();
        int height = bitmap.getHeight(); 

        int newWidth = 0;
        int newHeight = 0;

        if(width > height){
            newWidth = newSize;
            newHeight = (newSize * height)/width;
        } else if(width < height){
            newHeight = newSize;
            newWidth = (newSize * width)/height;
        } else if (width == height){
            newHeight = newSize;
            newWidth = newSize;
        }

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, 
                width, height, matrix, true); 

        return resizedBitmap;
    }

    public void onItemSelected(AdapterView<?> parent, View v, int position, long id) {
    	Drawable drawable = new BitmapDrawable(fotos.get(position));
        mSwitcher.setImageDrawable(drawable);
    }
 
    public void onNothingSelected(AdapterView<?> parent) {
    }
 
    public View makeView() {
        ImageView i = new ImageView(this);
        i.setBackgroundColor(0xFF000000);
        i.setScaleType(ImageView.ScaleType.FIT_CENTER);
        i.setLayoutParams(new ImageSwitcher.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        return i;
    }
 
    
 
    public class ImageAdapter extends BaseAdapter {
        public ImageAdapter(Context c) {
            mContext = c;
        }
 
        public int getCount() {
        	if (fotos != null) {
        		return fotos.size();
        	} else {
        		return 0;
        	}
        }
 
        public Object getItem(int position) {
            return position;
        }
 
        public long getItemId(int position) {
            return position;
        }
 
        public View getView(int position, View convertView, ViewGroup parent) {
            ImageView i = new ImageView(mContext);
 
            
            i.setImageBitmap(fotos.get(position));
            i.setAdjustViewBounds(true);
            i.setLayoutParams(new Gallery.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
//            i.setBackgroundResource(R.drawable.picture_frame);
            return i;
        }
 
        private Context mContext;
 
    }

    
}
