package com.frw.sisdat.ui.service.cadastro;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.frw.sisdat.dominio.DataType;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Questionario;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.service.BaseService;
import com.frw.sisdat.ws.GsonHandler;
import com.google.gson.Gson;
import com.j256.ormlite.field.DatabaseField;

public class CarregarPerguntasWSHandler extends GsonHandler {

	private MobileApplication application;

	private Long servicoID;

	public CarregarPerguntasWSHandler(Long servicoID, MobileApplication application) {

		this.servicoID = servicoID;
		this.application = application;
	}

	@Override
	public void parse(Gson gson, String resultado) {

		try {
			if (application.servicosStatus.get(servicoID) == BaseService.STATUS_CANCELADO) {
				return;
			}

			List<Pergunta> perguntas = new ArrayList<Pergunta>();

			JSONArray jsonArray = null;
			
			if (resultado.contains("PerguntaWSDTO")) {
				JSONObject jsonObject1 = new JSONObject(resultado);
				jsonArray = jsonObject1.getJSONArray("PerguntaWSDTO");
			} else {
				jsonArray = new JSONArray(resultado);
			}	

			for (int i = 0; i < jsonArray.length(); i++) {
				
				JSONObject jsonObject = null;
				
				jsonObject = jsonArray.getJSONObject(i);

				Pergunta u = new Pergunta();

				u.setId(jsonObject.getLong("codigo"));
				u.setDescricao(jsonObject.getString("descricao"));
				if (jsonObject.has("ordem")) {
					u.setOrdem(jsonObject.getInt("ordem"));
				} else {
					u.setOrdem(i);
				}
				
				if ((jsonObject.has("subDescricao")) && (jsonObject.getString("subDescricao") != null) && (!jsonObject.getString("subDescricao").equals("null")))
					u.setSubDescricao(jsonObject.getString("subDescricao"));

				if ((jsonObject.has("descricaoResumida")) && (jsonObject.getString("descricaoResumida") != null) && (!jsonObject.getString("descricaoResumida").equals("null")))
					u.setDescricaoResumida(jsonObject.getString("descricaoResumida"));

				u.setTipo(jsonObject.getInt("tipo"));
				
				if ((jsonObject.has("dataType")) && (jsonObject.getString("dataType") != null) && (!jsonObject.getString("dataType").equals("null"))) 
					u.setDataType(jsonObject.getInt("dataType"));
				else
					u.setDataType(DataType.TEXTO);
				
				if ((jsonObject.has("precisaoInicial")) && (jsonObject.getString("precisaoInicial") != null) && (!jsonObject.getString("precisaoInicial").equals("null")))
					u.setPrecisaoInicial(jsonObject.getDouble("precisaoInicial"));
				
				if ((jsonObject.has("precisaoFinal")) && (jsonObject.getString("precisaoFinal") != null) && (!jsonObject.getString("precisaoFinal").equals("null")))
					u.setPrecisaoFinal(jsonObject.getDouble("precisaoFinal"));
				
				if ((jsonObject.has("comentario")) && (jsonObject.getString("comentario") != null))
				u.setComentario(jsonObject.getString("comentario"));
				
				if ((jsonObject.has("obrigatoria")) && (jsonObject.getString("obrigatoria") != null) && (!jsonObject.getString("obrigatoria").equals("null")))
					u.setObrigatoria(jsonObject.getBoolean("obrigatoria"));
				else
					u.setObrigatoria(true);
				
				Questionario questionario = new Questionario();
				questionario.setCodigo(jsonObject.getString("codigoQuestionario"));
				u.setQuestionario(questionario);
				
				perguntas.add(u);

			}
			
			dataMapper.put("perguntas", perguntas);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
