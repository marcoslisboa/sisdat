package com.frw.sisdat.ui.cadastro;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.Gallery.LayoutParams;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import com.frw.sisdat.R;
import com.frw.sisdat.adapter.ImageAdapter;
import com.frw.sisdat.dao.PesquisaFotoDAO;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.dominio.PesquisaFoto;

public class FragmentFotos extends android.support.v4.app.Fragment implements AdapterView.OnItemSelectedListener, ViewSwitcher.ViewFactory{
	
	public static String TAG_FRAGMENT = "com.frw.sisdat.ui.cadastro.FragmentFotos";

	private ImageSwitcher mSwitcher;
	private Pesquisa pesquisa;
	private ImageAdapter adapter;
	private List<Bitmap> fotos = new ArrayList<Bitmap>();

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_image_gallery, container, false);
		pesquisa = ((PesquisaOpiniaoActivity)getActivity()).pesquisa;

		mSwitcher = (ImageSwitcher) view.findViewById(R.id.switcher);
        mSwitcher.setFactory(this);
        mSwitcher.setInAnimation(AnimationUtils.loadAnimation(getActivity(),
                android.R.anim.fade_in));
        mSwitcher.setOutAnimation(AnimationUtils.loadAnimation(getActivity(),
                android.R.anim.fade_out));
 
        Gallery g = (Gallery) view.findViewById(R.id.gallery);
        for (PesquisaFoto foto : pesquisa.fotos) {
        	fotos.add(BitmapFactory.decodeByteArray(foto.getFoto(), 0, foto.getFoto().length));
        }
        adapter = new ImageAdapter(getActivity(), fotos);
        g.setAdapter(adapter);
        g.setOnItemSelectedListener(this);

		return view;
	}

	public void onItemSelected(AdapterView<?> adapter, View v, int position, long id) {
		Bitmap bitmap = (Bitmap) adapter.getItemAtPosition(position);
		Drawable d = new BitmapDrawable(getResources(),bitmap);
		mSwitcher.setImageDrawable(d);
    }
 
    public void onNothingSelected(AdapterView<?> parent) {
    }
 
    public View makeView() {
        ImageView i = new ImageView(getActivity());
        i.setBackgroundColor(0xFF000000);
        i.setScaleType(ImageView.ScaleType.FIT_CENTER);
        i.setLayoutParams(new ImageSwitcher.LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        return i;
    }
    
    @Override
    public void onResume() {
    	if (adapter != null) {
    		fotos.clear();
    		for (PesquisaFoto foto : PesquisaFotoDAO.getInstance(getActivity()).findByPesquisa(pesquisa)) {
            	fotos.add(BitmapFactory.decodeByteArray(foto.getFoto(), 0, foto.getFoto().length));
            }
    		adapter.notifyDataSetChanged();
    	}
    	super.onResume();
    }
}
