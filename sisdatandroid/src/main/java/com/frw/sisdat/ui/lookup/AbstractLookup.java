package com.frw.sisdat.ui.lookup;

import java.util.List;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.frw.sisdat.R;
import com.frw.sisdat.dominio.IEntidade;
import com.frw.sisdat.dto.AbstractDTO;

public abstract class AbstractLookup<T extends IEntidade, Y extends AbstractDTO> {
	
	protected Context context;
	protected EditText editTextLookup;
	protected T itemSelecionado;
	private Dialog dialog;
	
	public void show(Context context) {
		this.context = context;
		
		dialog = new Dialog(context);
		dialog.setTitle(getTitle());
		dialog.setContentView(R.layout.lookup_dialog);
		LinearLayout contentFilter = (LinearLayout)dialog.findViewById(R.id.contentFilter);
		if (getFilterFormView() != null) {
			contentFilter.addView(getFilterFormView());
		} else {
			contentFilter.setVisibility(View.GONE);
		}

		ListView listView = (ListView) dialog.findViewById(R.id.listView);
		listView.setAdapter(getAdapter());

		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1, int position, long arg3) {
				T item = (T) adapter.getItemAtPosition(position);
				setItemSelecionado(item);
				if (onBeforeItemSelected()) {
					onItemSelected(item);
					dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
					if (isAutoCloseOnItemSelect()) {
						dialog.dismiss();
					}
					onAfterItemSelected(item);
				}
			}
			
		});
		
//		Button btnPesquisa = (Button) dialog.findViewById(R.id.btnPesquisa);
//		EditText txtNome = (EditText) dialog.findViewById(R.id.nomeCliente);
//		final String nome = txtNome.getText().toString();
//		btnPesquisa.setOnClickListener(new OnClickListener() {
//			
//			@Override
//			public void onClick(View v) {
//				Toast.makeText(AbstractLookup.this.context, "Certo", Toast.LENGTH_LONG).show();
//				getAdapter().notifyDataSetChanged();
//			}
//		});
		
		dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
		dialog.show();
//		dialogLoading.dismiss();
	}

	protected abstract String getTitle();
	
	public abstract List<T> loadData(Y dto);
	
	protected abstract ArrayAdapter<T> getAdapter();
	
	protected abstract void onItemSelected(T item);
	
	protected abstract LinearLayout getFilterFormView();
	
	protected boolean onBeforeItemSelected() {
		return true;
	}
	
	protected  void onAfterItemSelected(T item) {}
	
	
	
	protected boolean isAutoCloseOnItemSelect() {
		return true;
	}
	
	public T getItemSelecionado() {
		return itemSelecionado;
	}
	
	public void setItemSelecionado(T item) {
		itemSelecionado = item;
	}
	
	public Context getContext() {
		return context;
	}

	public void dismiss() {
		dialog.dismiss();
	}
	

}
