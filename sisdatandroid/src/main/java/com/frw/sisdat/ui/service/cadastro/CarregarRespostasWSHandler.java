package com.frw.sisdat.ui.service.cadastro;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Resposta;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.service.BaseService;
import com.frw.sisdat.ws.GsonHandler;
import com.google.gson.Gson;

public class CarregarRespostasWSHandler extends GsonHandler {

	private MobileApplication application;

	private Long servicoID;

	public CarregarRespostasWSHandler(Long servicoID, MobileApplication application) {

		this.servicoID = servicoID;
		this.application = application;
	}

	@Override
	public void parse(Gson gson, String resultado) {

		try {
			if (application.servicosStatus.get(servicoID) == BaseService.STATUS_CANCELADO) {
				return;
			}

			List<Resposta> respostas = new ArrayList<Resposta>();

			JSONArray jsonArray = null;
			
			if (resultado.contains("RespostaWSDTO")) {
				JSONObject jsonObject1 = new JSONObject(resultado);
				jsonArray = jsonObject1.getJSONArray("RespostaWSDTO");
			} else {
				jsonArray = new JSONArray(resultado);
			}	

			for (int i = 0; i < jsonArray.length(); i++) {
				
				JSONObject jsonObject = null;
				
				jsonObject = jsonArray.getJSONObject(i);

				Resposta u = new Resposta();

				u.setId(jsonObject.getLong("codigo"));
				if ((jsonObject.has("descricao")) && (jsonObject.getString("descricao") != null) && (!jsonObject.getString("descricao").equals("null"))) {
					u.setDescricao(jsonObject.getString("descricao"));
				}
				if ((jsonObject.has("ordem")) && (jsonObject.getString("ordem") != null) && (!jsonObject.getString("ordem").equals("null"))) {
					u.setOrdem(jsonObject.getInt("ordem"));
				}
				if ((jsonObject.has("valor")) && (jsonObject.getString("valor") != null) && (!jsonObject.getString("valor").equals("null"))) {
					u.setValor(jsonObject.getString("valor"));
				}
				
				if ((jsonObject.has("correta")) && (jsonObject.getString("correta") != null) && (!jsonObject.getString("correta").equals("null")))
					u.setCorreta(jsonObject.getBoolean("correta"));
				else
					u.setCorreta(false);
				
				Pergunta pergunta = new Pergunta();
				pergunta.setId(jsonObject.getLong("codigoPergunta"));
				u.setPergunta(pergunta);
				
				respostas.add(u);

			}
			
			dataMapper.put("respostas", respostas);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
