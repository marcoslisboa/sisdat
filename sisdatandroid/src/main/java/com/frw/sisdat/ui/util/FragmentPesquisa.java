package com.frw.sisdat.ui.util;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.frw.sisdat.R;

public class FragmentPesquisa extends Fragment {
	
	private PesquisaComFiltro pesquisaComFiltro;
	
	private EditText txtProcura;
	private AssetManager assetActiviy;
	private Activity activity;
	private Button btnFiltro;
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_pesquisa, container, false);
		
		btnFiltro = (Button) root.findViewById(R.id.btnFiltro);
		btnFiltro.setOnClickListener(onClickListenerFiltro);
		
		txtProcura = (EditText) root.findViewById(R.id.txtProcura);
		txtProcura.setTypeface(Typeface.createFromAsset(assetActiviy,"fonts/Roboto-Light.ttf"));
		txtProcura.setBackgroundColor(getResources().getColor(R.color.black));
		txtProcura.setTextColor(getResources().getColor(R.color.white));
		txtProcura.setOnKeyListener(onKeyListener);
		
		
		
		return root;
	}
	
	private void escondeTeclado() {
		InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		if(activity.getCurrentFocus() != null)
			inputManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);				
	}
	
	private OnClickListener onClickListenerFiltro = new OnClickListener() {

		public void onClick(View pView) {
			escondeTeclado();
			pesquisaComFiltro.updateListaComFiltro(txtProcura.getText().toString());
		}
	};
	
	private OnKeyListener onKeyListener = new OnKeyListener() {
		
		@Override
		public boolean onKey(View v, int keyCode, KeyEvent event) {
			if(keyCode == KeyEvent.KEYCODE_ENTER && event.getAction() == KeyEvent.ACTION_DOWN){
				escondeTeclado();
				pesquisaComFiltro.updateListaComFiltro(txtProcura.getText().toString());
			}	
			return false;
		}
	};
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		assetActiviy = activity.getAssets();
		this.activity = activity;
		try {
			pesquisaComFiltro = (PesquisaComFiltro) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException(activity.toString()
					+ " must implement PesquisaComFiltro");
		}
	}
	
	public interface PesquisaComFiltro {
		public void updateListaComFiltro(String filtro);
	}

}
