package com.frw.sisdat.ui;

import android.app.Application;
import android.net.Uri;

import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Usuario;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

public class MobileApplication extends Application {

	// Hashmap utilizado para salvar os status dos serviços rodando
	public Map<Long, Integer> servicosStatus = Collections.synchronizedMap(new HashMap<Long, Integer>());
	
	public AtomicReference<Usuario> usuarioLogado = new AtomicReference<Usuario>();
	
//	public Bitmap fotoApplication;
	public Uri imageUri;
	
	public List<Pergunta> listPerguntas = new ArrayList<Pergunta>();
	
	@Override
	public void onCreate() {
		super.onCreate();
	}

}
