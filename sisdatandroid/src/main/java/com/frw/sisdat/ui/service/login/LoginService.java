package com.frw.sisdat.ui.service.login;

import android.content.Intent;

import com.frw.sisdat.R;
import com.frw.sisdat.dominio.Usuario;
import com.frw.sisdat.negocio.UsuarioFacade;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.service.BaseService;

public class LoginService extends BaseService {

	private static final String TAG = LoginService.class.getName();

	public static final String EXTRA_USUARIO = "com.framework.gemobile.extra.USUARIO";
	public static final String EXTRA_SENHA = "com.framework.gemobile.extra.SENHA";

	public LoginService() {
		super(TAG);
	}

	@Override
	protected void executarServico(Intent intent) {

		String usuario = intent.getStringExtra(EXTRA_USUARIO);
		String senha = intent.getStringExtra(EXTRA_SENHA);

		Usuario u = UsuarioFacade.getInstance(getApplicationContext()).login(usuario, senha);
		
		MobileApplication application = (MobileApplication) getApplication();
		application.usuarioLogado.set(u);
		
		enviaMensagemReceiverConcluido();
		
	}

	@Override
	protected String mensagemErroGenerica() {
		return getResources().getString(R.string.login_msg_problemas);
	}

}
