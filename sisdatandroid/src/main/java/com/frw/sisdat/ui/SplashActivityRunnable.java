package com.frw.sisdat.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;

import com.frw.sisdat.R;
import com.frw.sisdat.ui.login.LoginActivity;

public class SplashActivityRunnable extends Activity{

	private final static int tempoEspera = 3000;
	
	private Thread splashThread;
	private boolean blnClicou = false;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);
		
		// thread para mostrar uma tela de Splash
		splashThread = new Thread() {
			@Override
			public void run() {
				try {
					synchronized (this) {
						// Espera por 5 segundos or sai quando
						// o usuário tocar na tela
						wait(tempoEspera);
						blnClicou = true;
					}
				} catch (InterruptedException ex) {
					blnClicou = false;
					finish();
				}

				if (blnClicou) {
					// fechar a tela de Splash
					finish();

					// Carrega a Activity Principal
					Intent i = new Intent();
					i.setClass(SplashActivityRunnable.this, LoginActivity.class);
					startActivity(i);
					
				}
			}
		};

		//splashThread.start();
	}

	@Override
	protected void onStart() {

		super.onStart();
	}

	@Override
	public void onPause() {
		// garante que quando o usuário clicar no botão
		// "Voltar" o sistema deve finalizar a thread
		splashThread.interrupt();
		super.onPause();
	}
	
	@Override
	public void onBackPressed() {
		fecharSplash();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			fecharSplash();
		}

		return true;
	}

	private void fecharSplash() {
		// o método abaixo está relacionado a thread de splash
		synchronized (splashThread) {
			
			if(blnClicou)
				return;
			
			blnClicou = true;

			// o método abaixo finaliza o comando wait
			// mesmo que ele não tenha terminado sua espera
			splashThread.notifyAll();
		}
	}
	
	@Override
	protected void onResume() {
		splashThread.start();
		super.onResume();
	}
	

}
