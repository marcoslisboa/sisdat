package com.frw.sisdat.ui.cadastro;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings.Secure;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.frw.sisdat.R;
import com.frw.sisdat.dao.PesquisaFotoDAO;
import com.frw.sisdat.dominio.AbstractColeta;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.dominio.PesquisaFoto;
import com.frw.sisdat.dominio.Usuario;
import com.frw.sisdat.enumeration.TipoPerguntaEnum;
import com.frw.sisdat.location.GPSTracker;
import com.frw.sisdat.negocio.PesquisaFacade;
import com.frw.sisdat.ui.DashboardActivity;
import com.frw.sisdat.ui.MobileApplication;
import com.viewpagerindicator.PageIndicator;
import com.viewpagerindicator.TabPageIndicator;
import com.viewpagerindicator.TitleProvider;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PesquisaOpiniaoActivity extends FragmentActivity {
	
	private Usuario usuario;
	private MobileApplication application;
	
	private static final String[] ITENS_MENU = new String[] {"Cabeçalho", "Responder", "Fotos"};
	private FragmentPagerAdapter fragmentPagerAdapter;
	private ViewPager viewPager;
	private PageIndicator pageIndicator;
	private FragmentOpiniaoCabecalho fragmentCabecalho;
	public FragmentOpiniaoQuestionario fragmentQuestionario;

	private static final int CAMERA_REQUEST = 1888;

	public Pesquisa pesquisa;
	private GPSTracker gpsTracker;

	public Uri imageUri;

	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// Obtem a application
		application = (MobileApplication) getApplication();
		gpsTracker = new GPSTracker(this);
		gpsTracker.refresh();

		// Obtem o usuario logado
		usuario = application.usuarioLogado.get();

		// Obtem a pesquisa do parâmetro
		Intent intent = getIntent();
		Bundle params = intent.getExtras();
		if (savedInstanceState != null) {
			pesquisa = (Pesquisa) savedInstanceState.getSerializable("pesquisa");
		}
		if (pesquisa == null) {
			try {
				this.pesquisa = (Pesquisa)params.getSerializable("pesquisa");
				List<AbstractColeta> coletas = PesquisaFacade.getInstance(this).obterColetasOpiniaoByPesquisa(pesquisa.getId());
				if (coletas != null) {
					pesquisa.setColetas(coletas);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		setTitle(pesquisa.getQuestionario().getDescricao());
		
		// Inicia regras de negocio;
		criaPesquisa();

		setContentView(R.layout.activity_pequisa_opiniao);
	
		// Obtem a action bar

        final ActionBar ab = getActionBar();
        ab.setDisplayShowTitleEnabled(true);
        ab.setDisplayShowHomeEnabled(true);
        ab.setDisplayHomeAsUpEnabled(true);

		// Cria os cabeçalhos
		FragmentManager fm = getSupportFragmentManager();
		fragmentPagerAdapter = new ViewPagerAdapter(fm);
		viewPager = (ViewPager) findViewById(R.id.pager);
		viewPager.setAdapter(fragmentPagerAdapter);
		pageIndicator = (TabPageIndicator) findViewById(R.id.indicator);
        pageIndicator.setViewPager(viewPager);
    }

    @Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("pesquisa", pesquisa);
		super.onSaveInstanceState(outState);
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//    	switch (requestCode) {
//
//        case CAMERA_REQUEST:
//            if (requestCode == CAMERA_REQUEST) {
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Bitmap photoBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(),application.imageUri);
                        Bitmap bitmap = resizeImage(photoBitmap, 640);
//                        ((MobileApplication)getApplication()).fotoApplication = bitmap;
                        if (bitmap != null) {
            				ByteArrayOutputStream stream = new ByteArrayOutputStream();
            				bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream);
//            				pesquisa.foto = stream.toByteArray();
            				PesquisaFoto pesquisaFoto = new PesquisaFoto(stream.toByteArray(), pesquisa);
            				PesquisaFotoDAO.getInstance(PesquisaOpiniaoActivity.this).createOrUpdate(pesquisaFoto);
//            				pesquisa.fotos.add(pesquisaFoto);
            				try {
            					stream.close();
            				} catch (IOException e) {
            					e.printStackTrace();
            				}
            			}
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
//            }
//        }
    }
	
	private Bitmap resizeImage(Bitmap bitmap, int newSize) {
        int width = bitmap.getWidth();
        int height = bitmap.getHeight(); 

        int newWidth = 0;
        int newHeight = 0;

        if(width > height){
            newWidth = newSize;
            newHeight = (newSize * height)/width;
        } else if(width < height){
            newHeight = newSize;
            newWidth = (newSize * width)/height;
        } else if (width == height){
            newHeight = newSize;
            newWidth = newSize;
        }

        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;

        Matrix matrix = new Matrix();
        matrix.postScale(scaleWidth, scaleHeight);

        Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, 
                width, height, matrix, true); 

        return resizedBitmap;
    }

	//Class ViewPagerAdapter ==============
	class ViewPagerAdapter extends FragmentPagerAdapter implements TitleProvider {
		public ViewPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public android.support.v4.app.Fragment getItem(int position) {
			
			
			switch (position) {
			
			case 0:
				if (fragmentCabecalho == null) {
					fragmentCabecalho = new FragmentOpiniaoCabecalho();
				}
				return fragmentCabecalho;
			case 1:
				if (fragmentQuestionario == null) {
					fragmentQuestionario = new FragmentOpiniaoQuestionario();
				}
				return fragmentQuestionario;
				
			case 2:
				return new FragmentFotos();
			default:
				return new FragmentOpiniaoCabecalho();
			}
		}
		
		@Override
		public int getCount() {
			return PesquisaOpiniaoActivity.ITENS_MENU.length;
		}

        //substituida pela getPageTitle em versões posteriores da android support
        @Override
        public String getTitle(int position) {
            return PesquisaOpiniaoActivity.ITENS_MENU[position
                    % PesquisaOpiniaoActivity.ITENS_MENU.length].toUpperCase();
        }

		@Override
		public String getPageTitle(int position) {
			return PesquisaOpiniaoActivity.ITENS_MENU[position
					% PesquisaOpiniaoActivity.ITENS_MENU.length].toUpperCase();
		}
		
	}
	//=======================================

	/**
	 * 
	 */
	private void criaPesquisa() {

		// Obtem a fachada
		PesquisaFacade facade = PesquisaFacade.getInstance(this);

		if (pesquisa.getId() == null) {

			pesquisa.setUsuario(usuario);
			pesquisa.setDataAbertura(new Date());
			
			// Obtem o IMEI
			try {
				String device = Secure.getString(this.getContentResolver(),Secure.ANDROID_ID);
				pesquisa.setDevice(device);
			} catch (Exception e) {
				pesquisa.setDevice("NI");
			}
			pesquisa.setColetas(new ArrayList<AbstractColeta>());
			if (gpsTracker.getLocation() != null) {
				pesquisa.setLatitudeInicial(gpsTracker.getLatitude());
				pesquisa.setLongitudeInicial(gpsTracker.getLongitude());
			}
			pesquisa.setStatus(PesquisaFacade.ABERTA);
			
			if ((pesquisa.getLatitudeInicial() == 0) || (pesquisa.getLongitudeInicial() == 0)) 
				Toast.makeText(PesquisaOpiniaoActivity.this, "O GPS gerou uma localização inconsistente!", Toast.LENGTH_LONG).show();
	
			// Insere uma pesquisa no banco de dados
			facade.insere(pesquisa);
			
		} else {
			List<AbstractColeta> coletas = facade.obterColetasOpiniaoByPesquisa(pesquisa.getId());
			if ((coletas != null) && (coletas.size() > 0)) {
				pesquisa.setColetas(coletas);
			} else {
				pesquisa.setColetas(new ArrayList<AbstractColeta>());
			}
		}
	}

	private void fecharPesquisa() {
		// Testa se todas as perguntas foram respondidas
		int todasRespondidas = 0;

		// Popula a lista de coletas da pesquisa
//		List<Pergunta> listPerguntas = ((PesquisaOpiniaoActivity) getActivity()).fragmentQuestionario
//				.getPerguntaRespostaAdapter().getListPergunta();asd

		if ((application.listPerguntas != null) && (application.listPerguntas.size() > 0)) {

			for (Pergunta pergunta : application.listPerguntas) {
				if (TipoPerguntaEnum.CAMPO_LIVRE.getId() == pergunta.getTipo()) {
					if (pergunta.getCampoLivre() == null || pergunta.getCampoLivre().trim().equals("")) {
						todasRespondidas++;
					}
				} else {
					if (pergunta.getRespostas() == null || pergunta.getRespostas().size() == 0) {
						todasRespondidas++;
					}
				}
			}
		}

		// Caso todas as perguntas tenham sido respondidas
		// permite fechar a pesquisa normalmente
		if (todasRespondidas == 0) {
			gpsTracker.refresh();
			// Abre o dialog de fechar pesquisa
			FecharPesquisaOpiniaoDialog dialog = new FecharPesquisaOpiniaoDialog(this, pesquisa);
			dialog.show();

		} else {

			final AlertDialog alertDialog = new AlertDialog.Builder(
					this).create();
			alertDialog.setTitle(getString(R.string.app_name));
			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
					getString(R.string.sim),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							alertDialog.dismiss();

							FecharPesquisaOpiniaoDialog dialog1 = new FecharPesquisaOpiniaoDialog(
									PesquisaOpiniaoActivity.this, pesquisa);
							dialog1.show();

						}
					});
			alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
					getString(R.string.nao),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,
								int which) {
							alertDialog.dismiss();
						}
					});

			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog
					.setMessage("Ainda existem "
							+ todasRespondidas
							+ " perguntas não respondidas na pesquisa. Prosseguir com o fechamento da pesquisa?");
			alertDialog.show();
		}

	}
	
	/**
	 * 
	 * @author glaporta Este dialog fecha uma pesquisa existente
	 */
	private class FecharPesquisaOpiniaoDialog extends Dialog {

		private Button btnFechar;
		private Button btnCancel;

		private TextView txtDataAbertura;
		private TextView txtDataFechamento;

		private SimpleDateFormat dateHourFormat = new SimpleDateFormat(
				"dd/MM/yyyy - HH:mm");

		public FecharPesquisaOpiniaoDialog(final Context context,
				final Pesquisa pesquisa) {
			super(context);

			setContentView(R.layout.dialog_fechar_pesquisa_opiniao);

			// Obtem os elementos de tela
			setTitle("Fechamento de Pesquisa");

			txtDataAbertura = (TextView) findViewById(R.id.txtDataAbertura);
			txtDataFechamento = (TextView) findViewById(R.id.txtDataFechamento);

			btnFechar = (Button) findViewById(R.id.btnFechar);
			btnFechar.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					// Valida fechamento de pesquisa
					if (validacoes()) {

						// Fecha a pesquisa
						fechaPesquisa();

						if ((pesquisa.getLatitudeFinal() == 0)
								|| (pesquisa.getLongitudeFinal() == 0))
							Toast.makeText(PesquisaOpiniaoActivity.this,
									"O GPS gerou uma localização inconsistente!",
									Toast.LENGTH_LONG).show();

						// Redireciona para a tela de dashboard
						Intent intent = new Intent(PesquisaOpiniaoActivity.this,
								DashboardActivity.class);
						startActivity(intent);

						dismiss();


					}

				finish();
				};
			});

			btnCancel = (Button) findViewById(R.id.btnCancel);
			btnCancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

			// Popula valores nos elementos de tela
			// Pega a data de fechamento do ponto
			txtDataAbertura.setText(dateHourFormat.format(pesquisa
					.getDataAbertura()));
			txtDataAbertura.setText(dateHourFormat.format(new Date()));
			txtDataFechamento.setText(dateHourFormat.format(new Date()));
		}

		private boolean validacoes() {

			return true;
		}

		private void fechaPesquisa() {

			pesquisa.setDataFechamento(new Date());

			if (gpsTracker.getLocation() != null) {
				pesquisa.setLatitudeFinal(gpsTracker.getLatitude());
				pesquisa.setLongitudeFinal(gpsTracker.getLongitude());
			}

			pesquisa.setStatus(PesquisaFacade.FECHADA);
			pesquisa.setFechamento("N");

			// Insere uma pesquisa no banco de dados
			PesquisaFacade facade = PesquisaFacade.getInstance(PesquisaOpiniaoActivity.this);
			facade.insere(pesquisa);

			facade.removeColetas(pesquisa);
			facade.salvaColetas(application.listPerguntas, pesquisa);
		}
	}

	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_pesquisa, menu);
        return true;
    }
	
	@Override
	protected void onPause() {
		super.onPause();
		PesquisaFacade facade = PesquisaFacade.getInstance(this);
		facade.salvaColetas(application.listPerguntas, pesquisa);
	}
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	switch (item.getItemId()) {
    	case R.id.menu_salvar:
			gpsTracker.refresh();
			fecharPesquisa();
			return true;
    		
		case R.id.menu_gps:
			
			final ProgressDialog progressDialog = ProgressDialog.show(PesquisaOpiniaoActivity.this, null, "Carregando...");
			gpsTracker = new GPSTracker(application) {
    			@Override
    			public void onLocationChanged(Location location) {
    				super.onLocationChanged(location);
    				progressDialog.dismiss();
    			}
    		};
    		gpsTracker.refresh();
			return true;

		case R.id.menu_foto:
			ContentValues values = new ContentValues();
			values.put(MediaStore.Images.Media.TITLE, "foto");
			values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
			application.imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
			Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
			intent.putExtra(MediaStore.EXTRA_OUTPUT, application.imageUri);
			startActivityForResult(intent, CAMERA_REQUEST);
			return true;

		case android.R.id.home:
			onBackPressed();
			return true;

		default:
			return false;
		}
    }
    
    class BuscaLocalizacaoAsyncTask extends AsyncTask<Void, Void, Void> {
    	private ProgressDialog progressDialog;
    	@Override
    	protected void onPreExecute() {
    		progressDialog = ProgressDialog.show(PesquisaOpiniaoActivity.this, null, "Carregando...");
    		super.onPreExecute();
    	}
    	@Override
    	protected Void doInBackground(Void... params) {
    		// TODO Auto-generated method stub
    		return null;
    	}
    	
    	@Override
    	protected void onPostExecute(Void result) {
    		gpsTracker = new GPSTracker(application) {
    			@Override
    			public void onLocationChanged(Location location) {
    				super.onLocationChanged(location);
    				progressDialog.dismiss();
    			}
    		};
    		super.onPostExecute(result);
    	}
    }
}
