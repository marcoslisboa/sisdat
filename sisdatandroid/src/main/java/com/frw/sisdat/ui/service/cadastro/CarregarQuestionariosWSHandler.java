package com.frw.sisdat.ui.service.cadastro;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import com.frw.sisdat.dominio.Questionario;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.service.BaseService;
import com.frw.sisdat.ws.GsonHandler;
import com.google.gson.Gson;

public class CarregarQuestionariosWSHandler extends GsonHandler {

	private MobileApplication application;

	private Long servicoID;

	public CarregarQuestionariosWSHandler(Long servicoID, MobileApplication application) {
		this.servicoID = servicoID;
		this.application = application;
	}

	@Override
	public void parse(Gson gson, String resultado) {

		try {
			if (application.servicosStatus.get(servicoID) == BaseService.STATUS_CANCELADO) {
				return;
			}

			List<Questionario> questionarios = new ArrayList<Questionario>();

			JSONArray jsonArray = null;
			
			if (resultado.contains("QuestionarioWSDTO")) {
				JSONObject jsonObject1 = new JSONObject(resultado);
				jsonArray = jsonObject1.getJSONArray("QuestionarioWSDTO");
			} else {
				jsonArray = new JSONArray(resultado);
			}	

			for (int i = 0; i < jsonArray.length(); i++) {
				
				JSONObject jsonObject = null;
				
				jsonObject = jsonArray.getJSONObject(i);

				Questionario u = new Questionario();
				
				u.setCodigo(jsonObject.getString("id"));
				u.setDescricao(jsonObject.getString("descricao"));
				u.setAtivo(jsonObject.getBoolean("excluido"));
				if (jsonObject.has("ordem")) {
					u.setOrdem(jsonObject.getInt("ordem"));
				} else {
					u.setOrdem(i);
				}
				
				if ((jsonObject.has("resumo")) && (jsonObject.getString("resumo") != null) && (!jsonObject.getString("resumo").equals("null")))
					u.setResumo(jsonObject.getString("resumo"));

				if ((jsonObject.has("versao")) && (jsonObject.getString("versao") != null) && (!jsonObject.getString("versao").equals("null")))
					u.setVersao(jsonObject.getString("versao"));

				if ((jsonObject.has("orientacao")) && (jsonObject.getString("orientacao") != null) && (!jsonObject.getString("orientacao").equals("null")))
					u.setOrientacao(jsonObject.getString("orientacao"));

				if ((jsonObject.has("codigo")) && (jsonObject.getString("codigo") != null) && (!jsonObject.getString("codigo").equals("null")))
					u.setCodigoQuestionario(jsonObject.getString("codigo"));

				questionarios.add(u);

			}
			
			dataMapper.put("questionarios", questionarios);

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
