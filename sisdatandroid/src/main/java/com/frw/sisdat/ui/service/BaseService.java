package com.frw.sisdat.ui.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import com.frw.sisdat.dao.UrlWSDAO;
import com.frw.sisdat.enumeration.TipoRemoteExecutorEnum;
import com.frw.sisdat.ui.MobileApplication;
import com.frw.sisdat.ui.util.FuncoesUteis;
import com.frw.sisdat.ws.GsonHandler.JsonHandlerException;
import com.frw.sisdat.ws.RemoteExecutorGSON;
import com.frw.sisdat.ws.RemoteExecutorJSON;

import java.util.Map;

public abstract class BaseService extends IntentService {

	private String mNomeServico;
	private TipoRemoteExecutorEnum tipoRemoteExecutor;
	private NotificationManager mNotificationManager;

	public BaseService(String name, TipoRemoteExecutorEnum tipoRemoteExecutorEnum) {
		super(name);
		mNomeServico = name;
		this.tipoRemoteExecutor = tipoRemoteExecutorEnum;
	}
	
	public BaseService(String name) {
		super(name);
		mNomeServico = name;
	}

	public String WS_URL;
	
	public static final String EXTRA_STATUS_RECEIVER = "com.framework.gemobile.extra.STATUS_RECEIVER";
	public static final String EXTRA_SERVICO_ID = "com.framework.gemobile.extra.SERVICO_ID";

	public static final int STATUS_INICIADO = 0x1;
	public static final int STATUS_ERRO = 0x2;
	public static final int STATUS_CONCLUIDO = 0x3;
	public static final int STATUS_CANCELADO = 0x4;
	public static final int STATUS_DESLOGADO = 0x5;

	protected RemoteExecutorGSON mRemoteExecutorGSON;
	protected RemoteExecutorJSON mRemoteExecutorJSON;
	protected ResultReceiver mReceiver;

	protected long servicoID;

	@Override
	public void onCreate() {
		super.onCreate();
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		WS_URL = UrlWSDAO.getInstance(getApplication()).getIp();
		mRemoteExecutorJSON = new RemoteExecutorJSON(getContentResolver());
		mRemoteExecutorGSON = new RemoteExecutorGSON(getContentResolver());
	}

	@Override
	protected void onHandleIntent(Intent intent) {

			Log.d(mNomeServico, "onHandleIntent(intent=" + intent.toString() + ")");
	
			mReceiver = intent.getParcelableExtra(EXTRA_STATUS_RECEIVER);
	
			servicoID = intent.getLongExtra(EXTRA_SERVICO_ID, 0);
	
			executarServico(intent);
	
			mReceiver = null;
			((MobileApplication) getApplication()).servicosStatus
					.remove(servicoID);
	}

	protected abstract void executarServico(Intent intent);

	protected Map<Long, Integer> servicosStatus() {
		return ((MobileApplication) getApplication()).servicosStatus;
	}

	private void enviaMensagemReceiver(int pStatus, String pMensagem) {

		Map<Long, Integer> servicosStatusTemp = servicosStatus();

		if (servicosStatusTemp.containsKey(servicoID)
				&& (servicosStatusTemp.get(servicoID) == BaseService.STATUS_CANCELADO)) {

			Log.d(mNomeServico, "Serviço cancelado");
			return;
		}

		if (pStatus == STATUS_CONCLUIDO)
			Log.d(mNomeServico, "Concluido");

		Bundle bundleTemp;
		if (FuncoesUteis.confereStringNula(pMensagem)) {
			bundleTemp = Bundle.EMPTY;
		} else {
			bundleTemp = new Bundle();
			bundleTemp.putString(Intent.EXTRA_TEXT, pMensagem);
		}

		if (mReceiver != null)
			mReceiver.send(pStatus, bundleTemp);
	}

	protected void enviaMensagemReceiverConcluido() {
		enviaMensagemReceiver(STATUS_CONCLUIDO, null);
	}

	protected void enviaMensagemReceiver(JsonHandlerException e) {

		Log.d(mNomeServico, e.getMessage());
		e.printStackTrace();

		enviaMensagemReceiver(e.getStatusMensagemReceiver(),
				(e.mostrarMensagem ? e.getMessage() : mensagemErroGenerica()));
	}

	protected void enviaMensagemReceiver(Exception e) {

		Log.d(mNomeServico, e.getMessage());
		e.printStackTrace();

//		enviaMensagemReceiver(e.geMessage(),
//				(e.mostrarMensagem ? e.getMessage() : mensagemErroGenerica()));
	}

	protected abstract String mensagemErroGenerica();
	
	
	protected void sendNotification(int id, String tickerText, String title, String text, Integer flags) {		
/*		long when = System.currentTimeMillis();

		Notification notification = new Notification(R.drawable.icsupra, tickerText, when);
		
		Context context = getApplicationContext();
		Intent notificationIntent = new Intent(this, ListMensagemActivity.class);
		notificationIntent.putExtra("tipo_servico", 1);
		PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

		notification.setLatestEventInfo(context, title, text, contentIntent);	

		notification.sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		notification.vibrate = new long[]{100, 200, 100, 500};
		
		mNotificationManager.notify(id, notification);*/
	}

}
