package com.frw.sisdat.ui;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.frw.sisdat.R;
import com.frw.sisdat.adapter.ListaPesquisaAdapter;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.negocio.PesquisaFacade;
import com.frw.sisdat.ui.cadastro.PesquisaOpiniaoActivity;

public class ListPesquisaActivity extends BaseActivity {

	private Button btnFiltro;
	private ListaPesquisaAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list_pesquisa);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			final ActionBar ab = getActionBar();
			ab.setDisplayShowTitleEnabled(true);
			ab.setDisplayShowHomeEnabled(true);
			ab.setDisplayHomeAsUpEnabled(true);
			ab.setDisplayUseLogoEnabled(true);
		}

		ListView listViewPedido = (ListView) findViewById(R.id.listViewPedido);
		adapter = new ListaPesquisaAdapter(this, PesquisaFacade.getInstance(this).findAll());
		listViewPedido.setAdapter(adapter);
		listViewPedido.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1, int position, long arg3) {
				Pesquisa pesquisa = (Pesquisa) adapter.getItemAtPosition(position);
				if (pesquisa.getDataSincronizacao() == null) {
					Bundle params = new Bundle();
					params.putSerializable("pesquisa", pesquisa);
					Intent intent = new Intent(ListPesquisaActivity.this, PesquisaOpiniaoActivity.class);
					intent.putExtras(params);
					startActivity(intent);
				}
			}
		});
		
		listViewPedido.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapter, View view, int position, long arg3) {
				final Pesquisa pesquisa = (Pesquisa) adapter.getItemAtPosition(position);
				final AlertDialog alertDialog = new AlertDialog.Builder(ListPesquisaActivity.this).create();
				alertDialog.setTitle(getString(R.string.app_name));
				alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, getString(R.string.sim),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {

								PesquisaFacade facade = PesquisaFacade.getInstance(getApplicationContext());
								facade.removePesquisaCompleta(pesquisa);
								ListPesquisaActivity.this.adapter.remove(pesquisa);
								ListPesquisaActivity.this.adapter.notifyDataSetChanged();
							}
				});
				alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.nao),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {

								alertDialog.dismiss();

							}
				});

				alertDialog.setIcon(R.drawable.ic_launcher);
				alertDialog
						.setMessage("Deseja remover a pesquisa selecionada?");
				alertDialog.show();
				return true;
			}
		});

//		btnFiltro = (Button) findViewById(R.id.btnFiltro);
//
//		btnFiltro.setOnClickListener(new OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				updateLista();
//			}
//		});
//		View viewPesquisa = findViewById(R.id.llpesquisa);
//		viewPesquisa.setVisibility(View.GONE);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_list_pesquisa, menu);
		return true;
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {

		switch (item.getItemId()) {

		case android.R.id.home:
			Intent intent = new Intent(this, DashboardActivity.class);
			startActivity(intent);
			finish();
			return true;

		case R.id.menu_apagar:

			final AlertDialog alertDialog = new AlertDialog.Builder(
					ListPesquisaActivity.this).create();
			alertDialog.setTitle(getString(R.string.app_name));
			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
					getString(R.string.sim),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

							PesquisaFacade facade = PesquisaFacade.getInstance(getApplicationContext());
							facade.removePesquisasPassadas();

							Intent intent = new Intent(ListPesquisaActivity.this, DashboardActivity.class);
							startActivity(intent);
							finish();

						}
					});
			alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
					getString(R.string.nao),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {

							alertDialog.dismiss();

						}
					});

			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog
					.setMessage("Deseja remover todas as pesquisas sincronizadas?");
			alertDialog.show();

			return true;

		default:
			return false;
		}
	}

	@Override
	public void onBackPressed() {
		Intent intent = new Intent(this, DashboardActivity.class);
		startActivity(intent);
		finish();
	}

//	public void onClickActionBottomPesquisa(View view) {
//		View viewPesquisa = findViewById(R.id.llpesquisa);
//		if (viewPesquisa.isShown()) {
//			viewPesquisa.setVisibility(View.GONE);
//		} else {
//			viewPesquisa.setVisibility(View.VISIBLE);
//		}
//	}

//	private void updateLista() {
//		adapter.clear();
//		adapter.addAll(PesquisaFacade.getInstance(ListPesquisaActivity.this)
//				.findAll());
//		adapter.notifyDataSetChanged();
//	}

}
