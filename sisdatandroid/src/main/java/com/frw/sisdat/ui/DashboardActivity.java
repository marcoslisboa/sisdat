package com.frw.sisdat.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import com.frw.sisdat.R;
import com.frw.sisdat.adapter.ListaQuestionarioAdapter;
import com.frw.sisdat.dao.UrlWSDAO;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.dominio.Questionario;
import com.frw.sisdat.dominio.Usuario;
import com.frw.sisdat.location.NetService;
import com.frw.sisdat.negocio.PesquisaFacade;
import com.frw.sisdat.negocio.QuestionarioFacade;
import com.frw.sisdat.ui.cadastro.PesquisaOpiniaoActivity;
import com.frw.sisdat.ui.sincronizacao.PainelSincronizacaoActivity;

import java.text.SimpleDateFormat;
import java.util.List;

public class DashboardActivity extends BaseActivity {

	private Usuario usuario;
	private MobileApplication application;

	private ListaQuestionarioAdapter adapter;

	private SimpleDateFormat dateHourFormat = new SimpleDateFormat(
			"dd/MM/yyyy - HH:mm");

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);

		// Obtem o application
		application = (MobileApplication) getApplication();

		// Obtem o usuario logado
		usuario = application.usuarioLogado.get();

		// Monta o adapter de questionários
		ListView listViewPedido = (ListView) findViewById(R.id.listViewPedido);
		adapter = new ListaQuestionarioAdapter(this, QuestionarioFacade
				.getInstance(this).buscarTodosQuestionarios());
		listViewPedido.setAdapter(adapter);
		listViewPedido.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1,
					int position, long arg3) {

				final Questionario questionario = (Questionario) adapter
						.getItemAtPosition(position);

				// Testa se existem pesquisas abertas
				PesquisaFacade pesquisaFacade = PesquisaFacade
						.getInstance(DashboardActivity.this);
				final Pesquisa pesquisa = pesquisaFacade
						.findUltimaAbertaByUsuarioTipo(usuario.getCodigo(),
								questionario);

				if (pesquisa != null) {

					final AlertDialog alertDialog = new AlertDialog.Builder(
							DashboardActivity.this).create();
					alertDialog.setTitle(getString(R.string.app_name));
					alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
							getString(R.string.sim),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {

									// Reabre pesquisa
									Bundle params = new Bundle();
									params.putSerializable("pesquisa", pesquisa);

									Intent intent = new Intent(
											DashboardActivity.this,
											PesquisaOpiniaoActivity.class);
									intent.putExtras(params);
									startActivity(intent);

								}
							});
					alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
							getString(R.string.nao),
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface dialog,
										int which) {

									// Abre uma nova pesquisa e fecha a que está
									// aberta
									PesquisaFacade pesquisaFacade = PesquisaFacade
											.getInstance(DashboardActivity.this);
									pesquisaFacade.fecharPesquisaManualmente(
											pesquisa, "F");

									alertDialog.dismiss();

									CreateDetalhesPesquisaOpiniaoDialog dialogCreate = new CreateDetalhesPesquisaOpiniaoDialog(
											DashboardActivity.this,
											questionario);
									dialogCreate.show();

								}
							});

					alertDialog.setIcon(R.drawable.ic_launcher);
					alertDialog.setMessage("Você possui uma pesquisa aberta em "
							+ dateHourFormat.format(pesquisa.getDataAbertura())
							+ " e não fechada. Deseja reabrir esta pesquisa?");
					alertDialog.show();

				} else {

					// Abre uma nova pesquisa vazia
					CreateDetalhesPesquisaOpiniaoDialog dialogCreate = new CreateDetalhesPesquisaOpiniaoDialog(
							DashboardActivity.this, questionario);
					dialogCreate.show();

				}

			}
		});

		// Popula objetos de tela
		QuestionarioFacade facade = QuestionarioFacade
				.getInstance(DashboardActivity.this);
		List<Questionario> lista = facade.buscarTodosQuestionarios();

		if (lista.size() == 0) {

			final AlertDialog alertDialog = new AlertDialog.Builder(
					DashboardActivity.this).create();
			alertDialog.setTitle(getString(R.string.app_name));
			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
					getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							alertDialog.dismiss();
						}
					});

			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog
					.setMessage("Você deve fazer a sincronização antes de realizar uma pesquisa");
			alertDialog.show();

		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_dashboard, menu);
		return true;
	}

	@Override
	protected void lazyAction() {
		Intent intent = new Intent(this, PainelSincronizacaoActivity.class);
		startActivity(intent);
		finish();
	}

	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.menu_logout:

			final AlertDialog alertDialog = new AlertDialog.Builder(
					DashboardActivity.this).create();
			alertDialog.setTitle(getString(R.string.app_name));
			alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
					getString(R.string.ok),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							// Efetua logout
							logout();
						}
					});
			alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
					getString(R.string.cancel),
					new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							alertDialog.dismiss();
						}
					});

			alertDialog.setIcon(R.drawable.ic_launcher);
			alertDialog.setMessage("Deseja finalizar a sessão?");
			alertDialog.show();

			return true;

		case R.id.menu_List_Pesquisa:
			Intent intent = new Intent(this, ListPesquisaActivity.class);
			startActivity(intent);
			return true;

		case R.id.menu_sincronizacao:

			// Testa configuracoes de rede do dispositivo
			if (NetService.isDeviceOnline(this)) {
				// Dispositivo com acesso wifi ou 3G
				ServidorAsynkTask servidorAsynkTask = new ServidorAsynkTask();
				servidorAsynkTask.execute(UrlWSDAO.getInstance(
						DashboardActivity.this).getIp());
			} else {
				dialogMessage(
						"Dispositivo Sem Wifi",
						"Seu dispositivo está fora de uma rede wifi. Caso deseje trabalhar no modo on-line, verifique suas configurações de rede ou clique no botão abaixo para continuar no modo off-line.",
						"Clique aqui para continuar...");
			}

			return true;

		}
		return false;
	}

	@Override
	protected void onResume() {
		super.onResume();
	}

	@Override
	protected void onPause() {
		super.onPause();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
	}

	@Override
	public void onBackPressed() {
		final AlertDialog alertDialog = new AlertDialog.Builder(
				DashboardActivity.this).create();
		alertDialog.setTitle(getString(R.string.app_name));
		alertDialog.setButton(DialogInterface.BUTTON_POSITIVE,
				getString(R.string.ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// Efetua logout
						logout();
					}
				});
		alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE,
				getString(R.string.cancel),
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						alertDialog.dismiss();
					}
				});

		alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.setMessage("Deseja finalizar a sessão?");
		alertDialog.show();
	}

	/******************************************************************
	 * 
	 * @author glaporta Dialog para pesquisa de Opinião
	 * 
	 ******************************************************************/
	private class CreateDetalhesPesquisaOpiniaoDialog extends Dialog {

		private Button btnAddItem;
		private Button btnCancel;

		public CreateDetalhesPesquisaOpiniaoDialog(final Context context,
				Questionario questionario) {
			this(context, null, questionario);
		}

		public CreateDetalhesPesquisaOpiniaoDialog(final Context context,
				Pesquisa ultimaPesquisa, final Questionario questionario) {
			super(context);
			requestWindowFeature(Window.FEATURE_NO_TITLE);
			setContentView(R.layout.dialog_detalhe_pesquisa_opiniao);

			// Obtem os elementos de tela
			setTitle("Abertura de Nova Pesquisa");

			btnAddItem = (Button) findViewById(R.id.btnAddItem);
			btnAddItem.setOnClickListener(new View.OnClickListener() {
				public void onClick(View v) {

					if (validacoes()) {

						Pesquisa pesquisa = new Pesquisa();
						pesquisa.setQuestionario(questionario);

						Bundle params = new Bundle();
						params.putSerializable("pesquisa", pesquisa);

						Intent intent = new Intent(DashboardActivity.this,
								PesquisaOpiniaoActivity.class);
						intent.putExtras(params);
						startActivity(intent);

						dismiss();
					}
				};
			});

			btnCancel = (Button) findViewById(R.id.btnCancel);
			btnCancel.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					dismiss();
				}
			});

			// Popula campos
		}

		private boolean validacoes() {

			return true;
		}
	}

}
