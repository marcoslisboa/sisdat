package com.frw.sisdat.ui;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

public abstract class BaseFragmentActivity extends FragmentActivity {

	private String TAG = BaseFragmentActivity.class.getSimpleName();

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	protected MobileApplication geMobileApplication(){
		
		return (MobileApplication)getApplication();
	}

	protected class ServidorAsynkTask  extends AsyncTask<String, Void, String> {

		private ProgressDialog progressDialog;

		public ServidorAsynkTask() {
			
		}
		
		protected void onPreExecute() {
			progressDialog = new ProgressDialog(BaseFragmentActivity.this);
			progressDialog.setMessage("Verificando Servidor de Dados Remoto. Aguarde...");
			progressDialog.show();
		}

		protected String doInBackground(String... s) {

			try {

				HttpParams httpParameters = new BasicHttpParams();
				HttpConnectionParams.setConnectionTimeout(httpParameters, 5000);
				HttpConnectionParams.setSoTimeout(httpParameters, 5000);

				DefaultHttpClient httpClient = new DefaultHttpClient(
						httpParameters);
				HttpGet httpGet = new HttpGet(s[0] + "pages/LoginPage");

				HttpResponse httpResponse = httpClient.execute(httpGet);
				HttpEntity httpEntity = httpResponse.getEntity();

				final int online = httpResponse.getStatusLine().getStatusCode();

				if (online == HttpStatus.SC_OK)
					return "OK";

			} catch (Exception e) {
				e.printStackTrace();
			}

			return null;
		}

		protected void onPostExecute(String data) {

			try { progressDialog.dismiss(); } catch (Exception e) {}

			if (!"OK".equals(data)) {
				dialogMessage(
						"Dispositivo sem Acesso Remoto",
						"Não foi possível localizar o servidor remoto de dados. Caso deseje trabalhar no modo on-line, verifique as configurações remotas ou clique no botão abaixo para continuar no modo off-line.",
						"Clique aqui para continuar...");

			} else {
				lazyAction();
			}

		}

	}

	protected void dialogMessage(String title, String message, String button) {

		AlertDialog.Builder alert = new AlertDialog.Builder(BaseFragmentActivity.this);
		alert.setTitle(title);
		alert.setMessage(message);
		alert.setPositiveButton(button, new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int arg1) {
				dialog.dismiss();
			}
		});

		alert.show();

	}
	
	protected void lazyAction() {
		
	}
}