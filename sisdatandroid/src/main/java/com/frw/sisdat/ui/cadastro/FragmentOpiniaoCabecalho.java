package com.frw.sisdat.ui.cadastro;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.frw.sisdat.R;
import com.frw.sisdat.dominio.ColetaOpiniao;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.dominio.Resposta;
import com.frw.sisdat.enumeration.TipoPerguntaEnum;
import com.frw.sisdat.negocio.PesquisaFacade;
import com.frw.sisdat.ui.DashboardActivity;
import com.frw.sisdat.ui.MobileApplication;

@SuppressLint("ValidFragment")
public class FragmentOpiniaoCabecalho extends android.support.v4.app.Fragment {

	public static String TAG_FRAGMENT = "forcavendamobile.fragment.item";
	private Pesquisa pesquisa;

	private LocationManager locationManager;

	private double latitude;
	private double longitude;

	private TextView txtCronometro;
	private TextView txtPesquisador;
	private TextView txtDataHoraAbertura;
	private TextView txtLatitude;
	private TextView txtLongitude;
	private TextView txtOrientacao;
	private EditText txtObservacao;
	private MobileApplication application;
	private static final int CAMERA_REQUEST = 1888;

	private SimpleDateFormat dateHourFormat = new SimpleDateFormat(
			"dd/MM/yyyy - HH:mm");

	private Button btnFechar;
	private Button btnTirarFoto;
	

	private final BroadcastReceiver m_timeChangedReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String dataAtual = dateHourFormat.format(new Date());
			txtCronometro.setText(dataAtual);
		}
	};

	public FragmentOpiniaoCabecalho() {

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		application = ((MobileApplication)getActivity().getApplication());
		pesquisa = ((PesquisaOpiniaoActivity)getActivity()).pesquisa;
		if (pesquisa == null) {
			try {
				pesquisa = (Pesquisa)savedInstanceState.get("PESQUISA");
				if (pesquisa == null) {
					Intent intent = new Intent(getActivity(), DashboardActivity.class);
					getActivity().startActivity(intent);
				}
			} catch (Exception e) {
				e.printStackTrace();
				Intent intent = new Intent(getActivity(), DashboardActivity.class);
				getActivity().startActivity(intent);
			}
		}

		// Registra um cronometro
		IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction(Intent.ACTION_TIME_TICK);
		intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
		intentFilter.addAction(Intent.ACTION_TIME_CHANGED);

		getActivity().registerReceiver(m_timeChangedReceiver, intentFilter);

		// Obtem o GPS
		locationManager = (LocationManager) getActivity().getSystemService(
				Context.LOCATION_SERVICE);

		ViewGroup root = (ViewGroup) inflater.inflate(
				R.layout.fragment_opiniao_cabecalho, container, false);


		// Obtem itens de tela
		txtCronometro = (TextView) root.findViewById(R.id.txtCronometro);
		txtPesquisador = (TextView) root.findViewById(R.id.txtPesquisador);
		txtDataHoraAbertura = (TextView) root
				.findViewById(R.id.txtDataHoraAbertura);
		txtLatitude = (TextView) root.findViewById(R.id.txtLatitude);
		txtLongitude = (TextView) root.findViewById(R.id.txtLongitude);
		txtOrientacao = (TextView) root.findViewById(R.id.txtOrientacao);
		txtObservacao = (EditText) root.findViewById(R.id.txtObservacao);
		txtObservacao.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {}
			
			@Override
			public void afterTextChanged(Editable s) {
				try {
					pesquisa.setObservacao(s.toString());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		// Popula elementos de tela
		txtCronometro.setText(dateHourFormat.format(new Date()));
		txtPesquisador.setText(pesquisa.getUsuario().getNome());
		txtDataHoraAbertura.setText(dateHourFormat.format(pesquisa
				.getDataAbertura()));

		txtLatitude.setText(String.valueOf(pesquisa.getLatitudeInicial()));
		txtLongitude.setText(String.valueOf(pesquisa.getLongitudeInicial()));

		txtOrientacao.setText(pesquisa.getQuestionario().getOrientacao());

		return root;
	}
	
	

	@Override
	public void onDestroyView() {
		// TODO Auto-generated method stub
		getActivity().unregisterReceiver(m_timeChangedReceiver);
		super.onDestroyView();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putSerializable("PESQUISA", pesquisa);
	}

}
