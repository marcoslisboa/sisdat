package com.frw.sisdat.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.frw.sisdat.dao.ormlite.BaseDAO;
import com.frw.sisdat.dao.ormlite.DatabaseHelper;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dto.PerguntaDTO;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

public class PerguntaDAO extends BaseDAO<Pergunta> {

	private static PerguntaDAO dao;
	
	public static PerguntaDAO getInstance(Context ctx) {
		if(dao == null) {
			dao = new PerguntaDAO(ctx);
		}
		return dao;
	}
	
	private PerguntaDAO(Context ctx) {
		super();
		super.ctx = ctx; 
	}
	
	public boolean createOrUpdateAll(List<Pergunta> perguntas) {
		for (Pergunta pergunta : perguntas) {
			createOrUpdate(pergunta);
		}
		return true;
	}
	
	@Override
	public List<Pergunta> findAll() {
		QueryBuilder<Pergunta, Object> queryBuilder = getConnection().queryBuilder();

		queryBuilder.orderBy("ordem", true);

		try {
			return getConnection().query(queryBuilder.prepare());
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<Pergunta>();
		}
	}

	public List<Pergunta> findByTipoPesquisaId(String id) {
		
		List<Pergunta> listPergunta = null;
		
		try {
			QueryBuilder<Pergunta, Object> queryBuilder = getConnection().queryBuilder();
			queryBuilder.where().eq("questionario_id", id);
			queryBuilder.orderBy("ordem", true);
			
			listPergunta = getConnection().query(queryBuilder.prepare());
			
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during PesquisaDAO.findByPesquisaNaoSincronizada", e);
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return listPergunta;
	}

	public List<Pergunta> pesquisaComDTO(PerguntaDTO perguntaDTO) {
		
		List<Pergunta> listPergunta = null;
		
		try {
			if(perguntaDTO != null && perguntaDTO.getDescricao() != null && !perguntaDTO.getDescricao().equals("")){

				QueryBuilder<Pergunta, Object> queryBuilder = getConnection().queryBuilder();
				Where<Pergunta, Object> where = queryBuilder.where();
				where.like("descricao", "%"+perguntaDTO.getDescricao()+"%");
				queryBuilder.orderBy("descricao", true);
				listPergunta = getConnection().query(queryBuilder.prepare());
						
			} else{

				QueryBuilder<Pergunta, Object> queryBuilder = getConnection().queryBuilder();

				queryBuilder.orderBy("descricao", true);

				listPergunta = getConnection().query(queryBuilder.prepare());
			}
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during EstadoDAO.pesquisaComDTO", e);
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		return listPergunta;
	}

	

}
