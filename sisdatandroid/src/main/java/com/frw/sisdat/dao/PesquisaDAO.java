package com.frw.sisdat.dao;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.frw.sisdat.dao.ormlite.BaseDAO;
import com.frw.sisdat.dao.ormlite.DatabaseHelper;
import com.frw.sisdat.dominio.Pesquisa;
import com.j256.ormlite.stmt.QueryBuilder;

public class PesquisaDAO extends BaseDAO<Pesquisa> {

	private static PesquisaDAO dao;

	public static PesquisaDAO getInstance(Context ctx) {
		if (dao == null) {
			dao = new PesquisaDAO(ctx);
		}
		return dao;
	}

	private PesquisaDAO(Context ctx) {
		super();
		super.ctx = ctx;
	}

    public List<Pesquisa> findByPesquisaNaoSincronizada() {
    	
		List<Pesquisa> listPesquisa = null;
		
		try {
			QueryBuilder<Pesquisa, Object> queryBuilder = getConnection().queryBuilder();
			queryBuilder.where().isNull("dataSincronizacao");
			
			listPesquisa = getConnection().query(queryBuilder.prepare());
			
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during PesquisaDAO.findByPesquisaNaoSincronizada", e);
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return listPesquisa;

    }    

    public List<Pesquisa> findByPesquisaSincronizada() {
    	
		List<Pesquisa> listPesquisa = null;
		
		try {
			QueryBuilder<Pesquisa, Object> queryBuilder = getConnection().queryBuilder();
			queryBuilder.where().isNotNull("dataSincronizacao");
			
			listPesquisa = getConnection().query(queryBuilder.prepare());
			
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during PesquisaDAO.findByPesquisaNaoSincronizada", e);
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return listPesquisa;

    }    

    public List<Pesquisa> findByPesquisaNaoSincronizada(String tipoPesquisaId) {
    	
		List<Pesquisa> listPesquisa = null;
		
		try {
			QueryBuilder<Pesquisa, Object> queryBuilder = getConnection().queryBuilder();
			queryBuilder.where().isNull("dataSincronizacao").and().eq("tipoPesquisa_id", tipoPesquisaId);
			
			listPesquisa = getConnection().query(queryBuilder.prepare());
			
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during PesquisaDAO.findByPesquisaNaoSincronizada", e);
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return listPesquisa;

    }    

    public Pesquisa findUltimaAbertaByUsuario(String codigo) {
    	
		List<Pesquisa> listPesquisa = null;
		
		try {
			QueryBuilder<Pesquisa, Object> queryBuilder = getConnection().queryBuilder();
			queryBuilder.where().isNull("dataSincronizacao")
								.and().isNull("dataFechamento")
								.and().eq("usuario_id", codigo);
			
			queryBuilder.orderBy("dataAbertura", false);
			
			listPesquisa = getConnection().query(queryBuilder.prepare());
			
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during PesquisaDAO.findByPesquisaNaoSincronizada", e);
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		if ((listPesquisa != null) && (listPesquisa.size() > 0))
			return listPesquisa.get(0);
		
		return null;

    }    

    public Pesquisa findUltimaAbertaByUsuarioTipo(String codigo, String tipo) {
    	
		List<Pesquisa> listPesquisa = null;
		
		try {
			QueryBuilder<Pesquisa, Object> queryBuilder = getConnection().queryBuilder();
			queryBuilder.where().isNull("dataSincronizacao")
								.and().isNull("dataFechamento")
								.and().eq("usuario_id", codigo)
								.and().eq("questionario_id", tipo);
			
			queryBuilder.orderBy("dataAbertura", false);
			
			listPesquisa = getConnection().query(queryBuilder.prepare());
			
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during PesquisaDAO.findByPesquisaNaoSincronizada", e);
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		if ((listPesquisa != null) && (listPesquisa.size() > 0))
			return listPesquisa.get(0);
		
		return null;

    }    

    public Pesquisa findUltimaByUsuarioTipo(String codigo, String tipo) {
    	
		List<Pesquisa> listPesquisa = null;
		
		try {
			QueryBuilder<Pesquisa, Object> queryBuilder = getConnection().queryBuilder();
			queryBuilder.where().eq("usuario_id", codigo)
								.and().eq("questionario_id", tipo);
			
			queryBuilder.orderBy("dataAbertura", false);
			
			listPesquisa = getConnection().query(queryBuilder.prepare());
			
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during PesquisaDAO.findByPesquisaNaoSincronizada", e);
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		if ((listPesquisa != null) && (listPesquisa.size() > 0))
			return listPesquisa.get(0);
		
		return null;

    }    


}
