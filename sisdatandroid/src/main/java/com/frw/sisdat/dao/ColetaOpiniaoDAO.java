package com.frw.sisdat.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.frw.sisdat.dao.ormlite.BaseDAO;
import com.frw.sisdat.dao.ormlite.DatabaseHelper;
import com.frw.sisdat.dominio.AbstractColeta;
import com.frw.sisdat.dominio.ColetaOpiniao;
import com.j256.ormlite.stmt.QueryBuilder;

public class ColetaOpiniaoDAO extends BaseDAO<ColetaOpiniao> {

	private static ColetaOpiniaoDAO dao;

	public static ColetaOpiniaoDAO getInstance(Context ctx) {
		if (dao == null) {
			dao = new ColetaOpiniaoDAO(ctx);
		}
		return dao;
	}

	private ColetaOpiniaoDAO(Context ctx) {
		super();
		super.ctx = ctx;
	}

    public List<AbstractColeta> findByPesquisaId(int id) {
    	
		List<ColetaOpiniao> listColeta = null;
		
		try {
			QueryBuilder<ColetaOpiniao, Object> queryBuilder = getConnection().queryBuilder();
			queryBuilder.where().eq("pesquisa_id", id);
			listColeta = getConnection().query(queryBuilder.prepare());
			
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during PesquisaDAO.findByPesquisaId", e);
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		List<AbstractColeta> list = new ArrayList<AbstractColeta>(); 
		
		for (ColetaOpiniao c: listColeta)
			list.add(c);
		
		return list;

    }    


}
