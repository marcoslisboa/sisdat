package com.frw.sisdat.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.frw.sisdat.dao.ormlite.BaseDAO;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.dominio.PesquisaFoto;
import com.j256.ormlite.stmt.QueryBuilder;

public class PesquisaFotoDAO extends BaseDAO<PesquisaFoto> {
	
	private static PesquisaFotoDAO dao;

	public static PesquisaFotoDAO getInstance(Context ctx) {
		if (dao == null) {
			dao = new PesquisaFotoDAO(ctx);
		}
		return dao;
	}

	private PesquisaFotoDAO(Context ctx) {
		super();
		super.ctx = ctx;
	}

	public List<PesquisaFoto> findByPesquisa(Pesquisa pesquisa) {

		List<PesquisaFoto> listPesquisaFoto = new ArrayList<PesquisaFoto>();
		try {
			QueryBuilder<PesquisaFoto, Object> queryBuilder = getConnection()
					.queryBuilder();
			queryBuilder.where().eq("pesquisa", pesquisa);
			listPesquisaFoto = getConnection().query(queryBuilder.prepare());

		} catch (SQLException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return listPesquisaFoto;
	}
}
