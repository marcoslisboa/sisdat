package com.frw.sisdat.dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.frw.sisdat.dao.ormlite.BaseDAO;
import com.frw.sisdat.dominio.Questionario;
import com.j256.ormlite.stmt.QueryBuilder;

public class QuestionarioDAO extends BaseDAO<Questionario> {

	private static QuestionarioDAO dao;
	
	public static QuestionarioDAO getInstance(Context ctx) {
		if(dao == null) {
			dao = new QuestionarioDAO(ctx);
		}
		return dao;
	}
	
	private QuestionarioDAO(Context ctx) {
		super();
		super.ctx = ctx; 
	}
	
	public boolean createOrUpdateAll(List<Questionario> questionarios) {
		for (Questionario questionario : questionarios) {
			createOrUpdate(questionario);
		}
		return true;
	}
	
	@Override
	public List<Questionario> findAll() {
		QueryBuilder<Questionario, Object> queryBuilder = getConnection().queryBuilder();

		queryBuilder.orderBy("ordem", true);

		try {
			return getConnection().query(queryBuilder.prepare());
		} catch (SQLException e) {
			e.printStackTrace();
			return new ArrayList<Questionario>();
		}
	}
}
