package com.frw.sisdat.dao.cadastro;

import java.util.Date;

import com.frw.sisdat.dominio.IEntidade;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class ControleSincronizacao implements IEntidade{

	@DatabaseField(id=true)
	public Long id;	

	@DatabaseField(dataType=DataType.DATE)
	public Date dataUltimaSincronizacao;

}
