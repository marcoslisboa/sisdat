package com.frw.sisdat.dao;

import android.content.Context;

import com.frw.sisdat.dao.ormlite.BaseDAO;
import com.frw.sisdat.dominio.cadastro.UrlWS;

public class UrlWSDAO extends BaseDAO<UrlWS>{

	private static UrlWSDAO dao;
	
	public static UrlWSDAO getInstance(Context ctx) {
		if(dao == null) {
			dao = new UrlWSDAO(ctx);
		}
		return dao;
	}
	
	private UrlWSDAO(Context ctx) {
		super();
		super.ctx = ctx; 
	}
	
	public String getIp() {
		dao.findAll();
		UrlWS ws = dao.findByPK(1l);
		return ws.ip;
	}

}
