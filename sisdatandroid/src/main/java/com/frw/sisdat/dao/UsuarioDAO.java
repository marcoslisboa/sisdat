package com.frw.sisdat.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.util.Log;

import com.frw.sisdat.dao.ormlite.BaseDAO;
import com.frw.sisdat.dao.ormlite.DatabaseHelper;
import com.frw.sisdat.dominio.Usuario;

public class UsuarioDAO extends BaseDAO<Usuario> {

	private static UsuarioDAO dao;
	
	public static UsuarioDAO getInstance(Context ctx) {
		if(dao == null) {
			dao = new UsuarioDAO(ctx);
		}
		return dao;
	}
	
	private UsuarioDAO(Context ctx) {
		super();
		super.ctx = ctx; 
	}
	
	
	public Usuario login(String username, String senha) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("login", username);
		parameters.put("password", senha);
		
		try {
			List<Usuario> usuarios = getConnection().queryForFieldValuesArgs(parameters);
			
			if(!usuarios.isEmpty()) {
				return usuarios.get(0);
			}
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during UsuarioDAO.login", e);
			throw new RuntimeException(e);
		}
		
		return null;
	}
	
	

}
