package com.frw.sisdat.dao;

import java.sql.SQLException;
import java.util.List;

import android.content.Context;
import android.util.Log;

import com.frw.sisdat.dao.ormlite.BaseDAO;
import com.frw.sisdat.dao.ormlite.DatabaseHelper;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Resposta;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;

public class RespostaDAO extends BaseDAO<Resposta> {

	private static RespostaDAO dao;
	
	public static RespostaDAO getInstance(Context ctx) {
		if(dao == null) {
			dao = new RespostaDAO(ctx);
		}
		return dao;
	}
	
	private RespostaDAO(Context ctx) {
		super();
		super.ctx = ctx; 
	}
	
	public boolean createOrUpdateAll(List<Resposta> respostas) {
		for (Resposta resposta : respostas) {
			createOrUpdate(resposta);
		}
		return true;
	}
	
	public List<Resposta> pesquisaOpcoesPorPergunta(Pergunta pergunta) {
		
		List<Resposta> listResposta = null;
		
		try {
			if (pergunta != null) {
				QueryBuilder<Resposta, Object> queryBuilder = getConnection().queryBuilder();
				Where<Resposta, Object> where = queryBuilder.where();
				where.eq("pergunta_id", pergunta);
				queryBuilder.orderBy("ordem", true);
				listResposta = getConnection().query(queryBuilder.prepare());
			} else {
				QueryBuilder<Resposta, Object> queryBuilder = getConnection().queryBuilder();
				listResposta = getConnection().query(queryBuilder.prepare());
			}
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during RespostaDAO.pesquisaPorPergunta", e);
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		
		return listResposta;
	}

	

}
