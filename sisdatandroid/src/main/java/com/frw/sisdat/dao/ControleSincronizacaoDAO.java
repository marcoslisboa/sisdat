package com.frw.sisdat.dao;

import android.content.Context;

import com.frw.sisdat.dao.cadastro.ControleSincronizacao;
import com.frw.sisdat.dao.ormlite.BaseDAO;

public class ControleSincronizacaoDAO extends BaseDAO<ControleSincronizacao>{

	private static ControleSincronizacaoDAO dao;
	
	public static ControleSincronizacaoDAO getInstance(Context ctx) {
		if(dao == null) {
			dao = new ControleSincronizacaoDAO(ctx);
		}
		return dao;
	}
	
	private ControleSincronizacaoDAO(Context ctx) {
		super();
		super.ctx = ctx; 
	}
	
}
