package com.frw.sisdat.dao.ormlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.frw.sisdat.dao.cadastro.ControleSincronizacao;
import com.frw.sisdat.dominio.ColetaOpiniao;
import com.frw.sisdat.dominio.IEntidade;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.dominio.PesquisaFoto;
import com.frw.sisdat.dominio.Questionario;
import com.frw.sisdat.dominio.Resposta;
import com.frw.sisdat.dominio.Usuario;
import com.frw.sisdat.dominio.cadastro.UrlWS;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
	
	private Context context;

	//nome do arquivo do database
	private static final String DATABASE_FILE_NAME = "eco.db";
	 
	// sempre que voce mudar objetos em seu database incremente a versao
	private static final int DATABASE_VERSION = 2;

	private Map<Class, Dao<IEntidade, Object>> daos = new HashMap<Class, Dao<IEntidade, Object>>();

	public DatabaseHelper(Context context) {
		super(context, DATABASE_FILE_NAME, null, DATABASE_VERSION);
		this.context = context;
	}

	public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
		try {
			Log.i(DatabaseHelper.class.getName(), "onCreate Database");

			Log.i(DatabaseHelper.class.getName(), "Create table Questionario");
			TableUtils.createTable(connectionSource, Questionario.class);

			Log.i(DatabaseHelper.class.getName(), "Create table Pergunta");
			TableUtils.createTable(connectionSource, Pergunta.class);

			Log.i(DatabaseHelper.class.getName(), "Create table Resposta");
			TableUtils.createTable(connectionSource, Resposta.class);
			
			Log.i(DatabaseHelper.class.getName(), "Create table ControleSincronizacao");
			TableUtils.createTable(connectionSource, ControleSincronizacao.class);

			Log.i(DatabaseHelper.class.getName(), "Create table Usuario");
			TableUtils.createTable(connectionSource, Usuario.class);
			
			Log.i(DatabaseHelper.class.getName(), "Create table Pesquisa");
			TableUtils.createTable(connectionSource, Pesquisa.class);
			
			Log.i(DatabaseHelper.class.getName(), "Create table PesquisaFoto");
			TableUtils.createTable(connectionSource, PesquisaFoto.class);
			
			Log.i(DatabaseHelper.class.getName(), "Create table ColetaOpiniao");
			TableUtils.createTable(connectionSource, ColetaOpiniao.class);

			TableUtils.createTable(connectionSource, UrlWS.class);
			UrlWS.createDefault(context);
			
		} catch (SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
			throw new RuntimeException(e);
		}
	}
	
	public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
		try {
			List<String> allSql = new ArrayList<String>();
			switch (oldVersion) {
				case 1:
			}
			
			for (String sql : allSql) {
				db.execSQL(sql);
			}

			UrlWS.createDefault(context);

		} catch (android.database.SQLException e) {
			Log.e(DatabaseHelper.class.getName(), "exception during onUpgrade", e);
			throw new RuntimeException(e);
		}

	}

	public <T> Dao<T, Object> getDAO(Class<T> entidadeClass) {
		Dao<IEntidade, Object> dao = null;
		if (daos.get(entidadeClass) == null) {
			try {
				dao = (Dao<IEntidade, Object>) getDao(entidadeClass);
			} catch (SQLException e) {
				Log.e(DatabaseHelper.class.getName(), "exception during getDAO", e);
				throw new RuntimeException(e);
			}
			daos.put(entidadeClass, dao);
		}
		
		return (Dao<T, Object>) daos.get(entidadeClass);
	}

}
