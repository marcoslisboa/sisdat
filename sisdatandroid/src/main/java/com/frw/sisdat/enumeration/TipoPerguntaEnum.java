package com.frw.sisdat.enumeration;

public enum TipoPerguntaEnum {
	UNICA_ESCOLHA(1), MULTIPLA_ESCOLHA(2), CAMPO_LIVRE(3), MULTIPLA_LIVRE(4);
	
	private int id;
	
	private TipoPerguntaEnum(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}
