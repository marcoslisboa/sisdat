package com.frw.sisdat;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import com.frw.sisdat.dao.UrlWSDAO;
import com.frw.sisdat.dominio.cadastro.UrlWS;
import com.frw.sisdat.ui.util.FuncoesUteis;

public class SettingsActivity extends Activity {

	private EditText txtEnderecoIp;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
	        final ActionBar ab = getActionBar();
	        
			// set defaults for logo & home up
			ab.setDisplayShowTitleEnabled(true);
			ab.setDisplayShowHomeEnabled(true);
			ab.setDisplayHomeAsUpEnabled(true);
        }
        		
        txtEnderecoIp = (EditText) findViewById(R.id.txtEnderecoIp);
        txtEnderecoIp.setText(UrlWSDAO.getInstance(this).getIp());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_settings, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	
    	switch (item.getItemId()) {
		case R.id.menu_salvar:
			if (salvar()) {
				return true;
			} else {
				FuncoesUteis.showDialog(this, "Informe o endereço do servidor.");
				return false;
			}
			
		case android.R.id.home:
			finish();
			return true;

		default:
			return false;
		}
    } 

    private boolean salvar() {
    	
    	String enderecoIp = txtEnderecoIp.getText().toString();
    	if (!enderecoIp.isEmpty()) {
    		UrlWS ip = new UrlWS(1l,enderecoIp);
    		UrlWSDAO.getInstance(this).createOrUpdate(ip);
    		
    		final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
    		alertDialog.setTitle(this.getString(R.string.app_name));
    		alertDialog.setButton(this.getString(R.string.ok), new DialogInterface.OnClickListener() {
    			public void onClick(DialogInterface dialog, int which) {
    				alertDialog.hide();
    				SettingsActivity.this.finish();
    			}
    		});
    		alertDialog.setIcon(R.drawable.icon_trafego);

    		alertDialog.setMessage("Alterações salvas com sucesso.");
    		alertDialog.show();
    		return true;
    	} else {
    		return false;
    	}
    }
}
