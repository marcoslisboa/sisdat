package com.frw.sisdat.adapter;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;

import java.util.concurrent.atomic.AtomicBoolean;

abstract public class EndlessAdapter extends AdapterWrapper {

	public final static int TYPE_SECTION_HEADER = 0;

	//Faltando quantos ele elementos pra chegar ao final da lista ele dispara o evento de loading.
	private final static int LIMITE_PRA_CARREGAR = 10;
	
	protected LayoutInflater mInflater;

	private AppendTask mAppendTask = null;

	abstract protected boolean cacheInBackground() throws Exception;

	abstract protected void appendCachedData();

	abstract protected void taskCancelada();

	private View pendingView = null;
	protected AtomicBoolean keepOnAppending = new AtomicBoolean(true);
	protected Context context;
	private int pendingResource = -1;

	/**
	 * Constructor wrapping a supplied ListAdapter
	 */
	public EndlessAdapter(Context context, ListAdapter wrapped) {
		super(wrapped);
		this.context = context;

		mInflater = LayoutInflater.from(context);
	}

	/**
	 * Constructor wrapping a supplied ListAdapter and providing a id for a
	 * pending view.
	 * 
	 * @param context
	 * @param wrapped
	 * @param pendingResource
	 */
	public EndlessAdapter(Context context, ListAdapter wrapped,
			int pendingResource) {
		super(wrapped);
		this.context = context;
		this.pendingResource = pendingResource;

		mInflater = LayoutInflater.from(context);
	}

	public boolean areAllItemsSelectable() {
		return false;
	}

	public boolean isEnabled(int position) {

		if ((position == 0) || (position == super.getCount() && keepOnAppending.get()))
			return false;
		else
			return true;
	}

	/**
	 * How many items are in the data set represented by this Adapter.
	 */
	@Override
	public int getCount() {
		if (keepOnAppending.get()) {
			return (super.getCount() + 1); // one more for "pending"
		}

		return (super.getCount());
	}

	/**
	 * Masks ViewType so the AdapterView replaces the "Pending" row when new
	 * data is loaded.
	 */
	public int getItemViewType(int position) {
		if (position == getWrappedAdapter().getCount()) {
			return (IGNORE_ITEM_VIEW_TYPE);
		}

		return (super.getItemViewType(position));
	}

	/**
	 * Masks ViewType so the AdapterView replaces the "Pending" row when new
	 * data is loaded.
	 * 
	 * @see #getItemViewType(int)
	 */
	public int getViewTypeCount() {
		return (super.getViewTypeCount() + 1);
	}

	/**
	 * Get a View that displays the data at the specified position in the data
	 * set. In this case, if we are at the end of the list and we are still in
	 * append mode, we ask for a pending view and return it, plus kick off the
	 * background task to append more data to the wrapped adapter.
	 * 
	 * @param position
	 *            Position of the item whose data we want
	 * @param convertView
	 *            View to recycle, if not null
	 * @param parent
	 *            ViewGroup containing the returned View
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		if (position == (super.getCount() - LIMITE_PRA_CARREGAR) && keepOnAppending.get()) {

			iniciaTask();
		}

		if (position == super.getCount() && keepOnAppending.get()) {
			if (pendingView == null) {
				pendingView = getPendingView(parent);
			}

			// Tenta iniciar a TASK de novo, pois pode não ter conseguido
			// conexao e está no carregando...
			iniciaTask();

			return (pendingView);
		}

		return (super.getView(position, convertView, parent));
	}

	private void iniciaTask() {
		if (mAppendTask == null
				|| mAppendTask.getStatus() == AsyncTask.Status.FINISHED) {
			mAppendTask = new AppendTask();
			mAppendTask.execute();
		}
	}

	/**
	 * Called if cacheInBackground() raises a runtime exception, to allow the UI
	 * to deal with the exception on the main application thread.
	 * 
	 * @param pendingView
	 *            View representing the pending row
	 * @param e
	 *            Exception that was raised by cacheInBackground()
	 * @return true if should allow retrying appending new data, false otherwise
	 */
	protected boolean onException(View pendingView, Exception e) {
		Log.e("EndlessAdapter", "Exception in cacheInBackground()", e);

		return (false);
	}

	public void CancelaTask() {
		if (mAppendTask != null
				&& mAppendTask.getStatus() != AsyncTask.Status.FINISHED) {

			mAppendTask.cancel(true);
		}
	}

	/**
	 * A background task that will be run when there is a need to append more
	 * data. Mostly, this code delegates to the subclass, to append the data in
	 * the background thread and rebind the pending view once that is done.
	 */
	class AppendTask extends AsyncTask<Void, Void, Exception> {
		@Override
		protected Exception doInBackground(Void... params) {
			Exception result = null;

			try {
				keepOnAppending.set(cacheInBackground());
			} catch (Exception e) {
				result = e;
			}

			return (result);
		}

		protected void onCancelled() {

			pendingView = null;

			taskCancelada();
			notifyDataSetChanged();
		}

		@Override
		protected void onPostExecute(Exception e) {
			if (e == null) {
				appendCachedData();
			} else {
				keepOnAppending.set(onException(pendingView, e));
			}

			pendingView = null;
			notifyDataSetChanged();
		}
	}

	/**
	 * Inflates pending view using the pendingResource ID passed into the
	 * constructor
	 * 
	 * @param parent
	 * @return inflated pending view, or null if the context passed into the
	 *         pending view constructor was null.
	 */
	protected View getPendingView(ViewGroup parent) {
		if (context != null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			return inflater.inflate(pendingResource, parent, false);
		}

		throw new RuntimeException(
				"You must either override getPendingView() or supply a pending View resource via the constructor");
	}

	/**
	 * Getter method for the Context being held by the adapter
	 * 
	 * @return Context
	 */
	protected Context getContext() {
		return (context);
	}
}