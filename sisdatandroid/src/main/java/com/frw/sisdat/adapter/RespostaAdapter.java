package com.frw.sisdat.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.frw.sisdat.R;
import com.frw.sisdat.dominio.Resposta;

public class RespostaAdapter extends ArrayAdapter<Resposta> {
	
	private LayoutInflater mInflater;
	private List<Resposta> listaPergunta;
	
	public RespostaAdapter(Context context, List<Resposta> objects) {
		super(context, R.layout.row_descricao, objects);
		mInflater = LayoutInflater.from(context);
		this.listaPergunta = objects;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		
		view = mInflater.inflate(R.layout.row_descricao, null);

		Resposta resposta = getItem(position);

		TextView txtDescricao = (TextView) view.findViewById(R.id.txtDescricao);
		if (resposta.getValor() != null && !resposta.getValor().equals("null")) {
			txtDescricao.setText(resposta.getValor()+" - "+resposta.getDescricao());
		} else {
			txtDescricao.setText(resposta.getDescricao());
		}

		return view;
	}
	
	public List<Resposta> getListPergunta() {
		return listaPergunta;
	}

}
