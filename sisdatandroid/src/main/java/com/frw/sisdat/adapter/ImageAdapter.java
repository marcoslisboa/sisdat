package com.frw.sisdat.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Gallery.LayoutParams;

public class ImageAdapter extends BaseAdapter {
	
	private Context context;
	private List<Bitmap> fotos;
	
	public ImageAdapter(Context context, List<Bitmap> fotos) {
		this.context = context;
		this.fotos = fotos;
	}

	@Override
	public int getCount() {
		return fotos.size();
	}

	@Override
	public Object getItem(int position) {
		return fotos.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView i = new ImageView(context);
        i.setImageBitmap(fotos.get(position));
        i.setAdjustViewBounds(true);
        i.setLayoutParams(new Gallery.LayoutParams(
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
//        i.setBackgroundResource(R.drawable.picture_frame);
        return i;
	}

	
	
}
