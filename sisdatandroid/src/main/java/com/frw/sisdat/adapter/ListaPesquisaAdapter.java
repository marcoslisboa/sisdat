package com.frw.sisdat.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.frw.sisdat.R;
import com.frw.sisdat.dominio.Pesquisa;

import java.text.SimpleDateFormat;
import java.util.List;

public class ListaPesquisaAdapter extends ArrayAdapter<Pesquisa> {

	private LayoutInflater mInflater;

	public final static int TYPE_SECTION_HEADER = 0;
	public final static int TYPE_NORMAL = 1;

	private List<Pesquisa> listaPesquisa;

	private Typeface mTypeface;
	private Typeface mTypefaceBold;

	private SimpleDateFormat dateHourFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	public ListaPesquisaAdapter(Context context, List<Pesquisa> objects) {
		super(context, R.layout.row_pesquisa, objects);
		mInflater = LayoutInflater.from(context);
		this.listaPesquisa = objects;
	}

	public int getCount() {
		return super.getCount() + 1;
	}

	public int getViewTypeCount() {
		return 2;
	}

	public int getItemViewType(int position) {

		if (position == 0)
			return TYPE_SECTION_HEADER;
		else
			return TYPE_NORMAL;
	}

	public boolean areAllItemsSelectable() {
		return false;
	}

	public boolean isEnabled(int position) {
		return (getItemViewType(position) != TYPE_SECTION_HEADER);
	}

	public Pesquisa getItem(int position) {
		return listaPesquisa.get(position - 1);
	}

	private String FormataCabecalho() {

		if (listaPesquisa.isEmpty())
			return "Nenhuma Pesquisa encontrada";
		else {
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(listaPesquisa.size() == 1 ? listaPesquisa.size()
					+ " Pesquisa encontrada" : listaPesquisa.size()
					+ " Pesquisas encontradas");

			return strBuilder.toString();

		}// else
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		if (position == 0) {

			view = mInflater.inflate(R.layout.listheader, null);

			((TextView) view).setTypeface(mTypeface);

			((TextView) view).setText(FormataCabecalho());

			return view;
		}

		if (view == null) {
			view = mInflater.inflate(R.layout.row_pesquisa, null);

			TextView txtNumero = (TextView) view.findViewById(R.id.txtNumero);
			txtNumero.setTypeface(mTypefaceBold);

		}

		Pesquisa pesquisa = getItem(position);

		if (pesquisa != null) {

			// Obtem os elementos de tela
			TextView txtPesquisador = (TextView) view.findViewById(R.id.txtPesquisad);
			TextView txtNumero = (TextView) view.findViewById(R.id.txtNumero);
			TextView txtTipo = (TextView) view.findViewById(R.id.txtTipo);
			TextView txtDataAbertura = (TextView) view.findViewById(R.id.txtDataAbertura);
			TextView txtDataFechamento = (TextView) view.findViewById(R.id.txtDataFechamento);
			TextView txtStatus = (TextView) view.findViewById(R.id.txtStatus);
			TextView txtDataSincronizacao = (TextView) view.findViewById(R.id.txtDataSincronizacao);

			// Popula os elementos de tela
			txtPesquisador.setText("Pesquisador: " + pesquisa.getUsuario().getNome());
			txtNumero.setText(" " + String.valueOf(pesquisa.getId()));

			txtTipo.setText(" " + pesquisa.getQuestionario().getDescricao());

			if (pesquisa.getDataAbertura() != null)
				txtDataAbertura.setText(" " + dateHourFormat.format(pesquisa.getDataAbertura()));
			else
				txtDataAbertura.setText(" " + "-");
			
			if (pesquisa.getDataFechamento() != null)
				txtDataFechamento.setText(" " + dateHourFormat.format(pesquisa.getDataFechamento()));
			else
				txtDataFechamento.setText(" " + "-");

			txtStatus.setText(" " + pesquisa.getStatus());

			if (pesquisa.getDataSincronizacao() != null)
				txtDataSincronizacao.setText(" " + dateHourFormat.format(pesquisa.getDataSincronizacao()));
			else
				txtDataSincronizacao.setText(" " + "NÃO SINCRONIZADA");

		}

		return view;
	}

	public List<Pesquisa> getListVendedor() {
		return listaPesquisa;
	}

}
