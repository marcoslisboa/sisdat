package com.frw.sisdat.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.frw.sisdat.R;
import com.frw.sisdat.dominio.Questionario;

import java.text.SimpleDateFormat;
import java.util.List;

public class ListaQuestionarioAdapter extends ArrayAdapter<Questionario> {

	private LayoutInflater mInflater;

	public final static int TYPE_SECTION_HEADER = 0;
	public final static int TYPE_NORMAL = 1;

	private List<Questionario> listaQuestionario;

	private Typeface mTypeface;

	private SimpleDateFormat dateHourFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

	public ListaQuestionarioAdapter(Context context, List<Questionario> objects) {
		super(context, R.layout.row_pesquisa, objects);
		mInflater = LayoutInflater.from(context);
		this.listaQuestionario = objects;
	}

	public int getCount() {
		return super.getCount() + 1;
	}

	public int getViewTypeCount() {
		return 2;
	}

	public int getItemViewType(int position) {

		if (position == 0)
			return TYPE_SECTION_HEADER;
		else
			return TYPE_NORMAL;
	}

	public boolean areAllItemsSelectable() {
		return false;
	}

	public boolean isEnabled(int position) {
		return (getItemViewType(position) != TYPE_SECTION_HEADER);
	}

	public Questionario getItem(int position) {
		return listaQuestionario.get(position - 1);
	}

	private String FormataCabecalho() {

		if (listaQuestionario.isEmpty())
			return "Nenhum questionário encontrado";
		else {
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append(listaQuestionario.size() == 1 ? listaQuestionario.size()
					+ " Questionário disponível" : listaQuestionario.size()
					+ " Questionários disponíveis");

			return strBuilder.toString();

		}// else
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;

		if (position == 0) {

			view = mInflater.inflate(R.layout.listheader, null);

			((TextView) view).setTypeface(mTypeface);

			((TextView) view).setText(FormataCabecalho());

			return view;
		}

		if (view == null) {
			view = mInflater.inflate(R.layout.row_questionario, null);
		}

		Questionario questionario = getItem(position);

		if (questionario != null) {

			// Obtem os elementos de tela
			TextView txtQuestionario = (TextView) view.findViewById(R.id.txtQuestionario);
			TextView txtCodigo = (TextView) view.findViewById(R.id.txtCodigo);
			TextView txtDataCriacao = (TextView) view.findViewById(R.id.txtDataCriacao);
			TextView txtResumo = (TextView) view.findViewById(R.id.txtResumo);

			// Popula os elementos de tela
			txtQuestionario.setText(questionario.getDescricao());
			txtCodigo.setText(questionario.getCodigoQuestionario());
			txtDataCriacao.setText("");
			txtResumo.setText(questionario.getResumo());

		}

		return view;
	}


}
