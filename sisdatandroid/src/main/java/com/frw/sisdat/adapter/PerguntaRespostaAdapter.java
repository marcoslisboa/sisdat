package com.frw.sisdat.adapter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.frw.sisdat.R;
import com.frw.sisdat.dominio.DataType;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Resposta;
import com.frw.sisdat.enumeration.TipoPerguntaEnum;
import com.frw.sisdat.ui.lookup.RespostaLookup;
import com.frw.sisdat.ui.util.DatePickerFragment;
import com.frw.sisdat.ui.util.FuncoesUteis;

public class PerguntaRespostaAdapter extends ArrayAdapter<Pergunta> {

	private LayoutInflater mInflater;
	private List<Pergunta> listaPergunta;
	private int count = 0;
	private FragmentManager fragmentManager;

	public PerguntaRespostaAdapter(Context context, List<Pergunta> objects, FragmentManager fragmentManager) {
		super(context, R.layout.row_descricao, objects);
		mInflater = LayoutInflater.from(context);
		this.listaPergunta = objects;
		this.fragmentManager = fragmentManager;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		final Pergunta pergunta = getItem(position);
		// Apenas para teste
		// pergunta.setTipo(3);

		if (TipoPerguntaEnum.UNICA_ESCOLHA.getId() == pergunta.getTipo()) {
			view = mInflater.inflate(R.layout.row_lookup_pergunta_resposta, null);
			EditText txtLookup = (EditText) view.findViewById(R.id.txtlookup);
			boolean isCreated = false;

			final RespostaLookup respostaLookup = new RespostaLookup(txtLookup, pergunta) {
				@Override
				protected void onAfterItemSelected(Resposta item) {
					pergunta.setRespostas(new ArrayList<Resposta>());
					pergunta.getRespostas().add(item);
				}
			};

			if (pergunta.getRespostas() != null && pergunta.getRespostas().size() > 0) {
				respostaLookup.setItemSelecionado(pergunta.getRespostas().get(0));
			}

			txtLookup.setFocusable(false);
			txtLookup.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					respostaLookup.show(getContext());
				}
			});
			TextView txtLabelLookup = (TextView) view.findViewById(R.id.txtLabelLookup);
			txtLabelLookup.setText(pergunta.getOrdem() + " - " + pergunta.getDescricao());

			TextView txtLabelLookupSubDescricao = (TextView) view.findViewById(R.id.txtLabelLookupSubDescricao);
			if (pergunta.getSubDescricao() != null && !pergunta.getSubDescricao().isEmpty() && pergunta.getSubDescricao() != "" && !pergunta.getSubDescricao().equals("null")) {
				txtLabelLookupSubDescricao.setText("(" + pergunta.getSubDescricao() + ")");
			} else {
				txtLabelLookupSubDescricao.setVisibility(View.GONE);
			}

		} else if (TipoPerguntaEnum.MULTIPLA_ESCOLHA.getId() == pergunta.getTipo()) {
			view = mInflater.inflate(R.layout.row_pergunta_resposta, null);
			LinearLayout containerMultiplaEscolha = (LinearLayout) view.findViewById(R.id.containerPerguntaResposta);

			TextView txtLabel = (TextView) view.findViewById(R.id.txtLabel);
			txtLabel.setText(pergunta.getOrdem() + " - " + pergunta.getDescricao());

			TextView txtLabelSubDescricao = (TextView) view.findViewById(R.id.txtLabelSubDescricao);
			if (pergunta.getSubDescricao() != null
					&& !pergunta.getSubDescricao().isEmpty()
					&& pergunta.getSubDescricao() != ""
					&& !pergunta.getSubDescricao().equals("null")) {
				txtLabelSubDescricao.setText("(" + pergunta.getSubDescricao() + ")");
			} else {
				txtLabelSubDescricao.setVisibility(View.GONE);
			}

			for (final Resposta opcao : pergunta.getOpcoes()) {
				CheckBox cb = new CheckBox(getContext());
				cb.setText(opcao.getDescricao());
				containerMultiplaEscolha.addView(cb);
				
				if (pergunta.getRespostas() != null && pergunta.getRespostas().size() > 0) {
					for (Resposta resposta : pergunta.getRespostas()) {
						if (opcao.getId().equals(resposta.getId())) {
							cb.setChecked(true);
						}
					}
				}

				cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						for (Resposta resposta : pergunta.getRespostas()) {
							if (opcao.getId().equals(resposta.getId())) {
								pergunta.getRespostas().remove(opcao);
								break;
							}
						}
						if (isChecked) {
							pergunta.getRespostas().add(opcao);
						}
					}
				});
				
			}
		} else if (TipoPerguntaEnum.CAMPO_LIVRE.getId() == pergunta.getTipo()) {
			view = mInflater.inflate(R.layout.row_pergunta_resposta, null);
			LinearLayout containerPerguntaResposta = (LinearLayout) view.findViewById(R.id.containerPerguntaResposta);
			TextView txtLabel = (TextView) view.findViewById(R.id.txtLabel);
			txtLabel.setText(pergunta.getOrdem() + " - " + pergunta.getDescricao());

			TextView txtLabelSubDescricao = (TextView) view.findViewById(R.id.txtLabelSubDescricao);
			if (pergunta.getSubDescricao() != null
					&& !pergunta.getSubDescricao().isEmpty()
					&& pergunta.getSubDescricao() != ""
					&& !pergunta.getSubDescricao().equals("null")) {
				
				txtLabelSubDescricao.setText("(" + pergunta.getSubDescricao() + ")");
			} else {
				txtLabelSubDescricao.setVisibility(View.GONE);
			}

			EditText txtCampoLivre = new EditText(getContext());
			txtCampoLivre.setBackgroundResource(android.R.drawable.edit_text);
			txtCampoLivre.setId(pergunta.getId().intValue());

			// Prepara o tipo de teclado
			if (pergunta.getDataType() == DataType.TEXTO) {
				txtCampoLivre.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
			} else if (pergunta.getDataType() == DataType.INTEIRO) {
				txtCampoLivre.setInputType(InputType.TYPE_CLASS_NUMBER);
			} else if (pergunta.getDataType() == DataType.FRACIONARIO) {
				txtCampoLivre.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
			} else if (pergunta.getDataType() == DataType.DATA) {
				txtCampoLivre.setFocusable(false);
				txtCampoLivre.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(final View v) {
						DialogFragment newFragment = new DatePickerFragment() {
							
							@Override
							public void onChanged(Date dataSelecionada) {
								((EditText) v).setText(FuncoesUteis.formatDate(dataSelecionada));
							}
						};
						newFragment.show(fragmentManager, "datePicker");
					}
				});
			} else {
				txtCampoLivre.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
			}

			if (pergunta.getCampoLivre() != null) {
				txtCampoLivre.setText(pergunta.getCampoLivre());
			}

			txtCampoLivre.addTextChangedListener(new TextWatcher() {

				@Override
				public void onTextChanged(CharSequence s, int start,
						int before, int count) {
					pergunta.setCampoLivre(s.toString());
					pergunta.getRespostas().add(new Resposta());
				}

				@Override
				public void beforeTextChanged(CharSequence s, int start,
						int count, int after) {
				}

				@Override
				public void afterTextChanged(Editable s) {
				}
			});
			

			containerPerguntaResposta.addView(txtCampoLivre);

		} else if (TipoPerguntaEnum.MULTIPLA_LIVRE.getId() == pergunta
				.getTipo()) {

			view = mInflater.inflate(R.layout.row_pergunta_resposta, null);
			LinearLayout containerMultiplaEscolha = (LinearLayout) view
					.findViewById(R.id.containerPerguntaResposta);

			TextView txtLabel = (TextView) view.findViewById(R.id.txtLabel);
			txtLabel.setText(pergunta.getOrdem() + " - "
					+ pergunta.getDescricao());

			TextView txtLabelSubDescricao = (TextView) view
					.findViewById(R.id.txtLabelSubDescricao);
			if (pergunta.getSubDescricao() != null
					&& !pergunta.getSubDescricao().isEmpty()
					&& pergunta.getSubDescricao() != ""
					&& !pergunta.getSubDescricao().equals("null")) {
				txtLabelSubDescricao.setText("(" + pergunta.getSubDescricao()
						+ ")");
			} else {
				txtLabelSubDescricao.setVisibility(View.GONE);
			}

			for (final Resposta opcao : pergunta.getOpcoes()) {

				pergunta.getRespostas().add(opcao);

				TextView tv = new TextView(getContext());
				tv.setText(opcao.getOrdem() + ") " + opcao.getDescricao());
				containerMultiplaEscolha.addView(tv);

				EditText et = new EditText(getContext());
				et.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
				
				LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
				lp.setMargins(0, 0, 0, 20);
	            et.setLayoutParams(lp);

				if ((opcao.getCampoLivre() != null)	&& (!opcao.getCampoLivre().trim().equals(""))) {
					et.setText(opcao.getCampoLivre());
				}

				et.addTextChangedListener(new TextWatcher() {

					@Override
					public void onTextChanged(CharSequence s, int start,
							int before, int count) {
						opcao.setCampoLivre(s.toString());
					}

					@Override
					public void beforeTextChanged(CharSequence s, int start,
							int count, int after) {
					}

					@Override
					public void afterTextChanged(Editable s) {
					}
				});

				containerMultiplaEscolha.addView(et);

			}

		}

		return view;
	}

	public List<Pergunta> getListPergunta() {
		return listaPergunta;
	}

	@Override
	public int getViewTypeCount() {
		return ++count;
	}

}
