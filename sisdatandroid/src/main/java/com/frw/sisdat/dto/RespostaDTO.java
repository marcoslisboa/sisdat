package com.frw.sisdat.dto;

import com.frw.sisdat.dominio.Pergunta;

public class RespostaDTO extends AbstractDTO {

	public String codigo;
    public String descricao;
    public Pergunta pergunta;	
}
