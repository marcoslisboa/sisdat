package com.frw.sisdat.dto;

public class PerguntaDTO extends AbstractDTO {

	private String id;
    private String descricao;

	public String getCodigo() {
		return id;
	}

	public void setCodigo(String id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
}
