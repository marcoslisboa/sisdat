package com.frw.sisdat.location;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetService {

	// Verifica se possui um 3G ou wireless ligado
	public static boolean isDeviceOnline(Context pContext) {
		ConnectivityManager cm = (ConnectivityManager) pContext
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

}
