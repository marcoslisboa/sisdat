package com.frw.sisdat.negocio;

import android.content.Context;

import com.frw.sisdat.dao.ControleSincronizacaoDAO;
import com.frw.sisdat.dao.cadastro.ControleSincronizacao;

public class ControleSincronizacaoFacade {

	private static ControleSincronizacaoFacade facade;
	private Context context;
	
	public static ControleSincronizacaoFacade getInstance(Context ctx) {
		if(facade == null) {
			facade = new ControleSincronizacaoFacade(ctx);
		}
		return facade;
	}
	
	private ControleSincronizacaoFacade(Context context) {
		this.context = context; 
	}
	
	public void salva(ControleSincronizacao cs) {
		ControleSincronizacaoDAO.getInstance(context).createOrUpdate(cs);
	}		
		
	
}
