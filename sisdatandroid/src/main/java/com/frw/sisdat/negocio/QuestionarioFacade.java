package com.frw.sisdat.negocio;

import java.util.List;

import android.content.Context;

import com.frw.sisdat.dao.QuestionarioDAO;
import com.frw.sisdat.dominio.Questionario;

public class QuestionarioFacade {

	private static QuestionarioFacade INSTANCE;
	private Context context;
	
	private QuestionarioFacade(Context context) {
		this.context = context;
	}
	
	public static QuestionarioFacade getInstance(Context context) {
		if(INSTANCE == null)
			INSTANCE = new QuestionarioFacade(context);
		
		return INSTANCE;
	}

	public List<Questionario> buscarTodosQuestionarios() {
		return QuestionarioDAO.getInstance(context).findAll();
	}
	
	public void insereQuestionarios(List<Questionario> questionarios) {
		
		if ((questionarios != null) && (questionarios.size() > 0)) {
			for (Questionario u : questionarios) {
				insere(u);
			}
		}
	}

	public void deleteQuestionarios() {
		QuestionarioDAO dao = QuestionarioDAO.getInstance(context);
		List<Questionario> list = dao.findAll();
		if(list != null && !list.isEmpty()){
			for (Questionario questionario : list) {
				dao.delete(questionario);
			}
		}
	}
	
	public void insere(Questionario u) {
		QuestionarioDAO.getInstance(context).createOrUpdate(u);
	}

	

}
