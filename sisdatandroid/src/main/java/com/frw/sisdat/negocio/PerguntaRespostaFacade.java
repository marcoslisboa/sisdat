package com.frw.sisdat.negocio;

import java.util.List;

import android.content.Context;

import com.frw.sisdat.dao.PerguntaDAO;
import com.frw.sisdat.dao.RespostaDAO;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Questionario;
import com.frw.sisdat.dominio.Resposta;

public class PerguntaRespostaFacade {

	private static PerguntaRespostaFacade INSTANCE;
	private Context context;
	
	private PerguntaRespostaFacade(Context context) {
		this.context = context;
	}
	
	public static PerguntaRespostaFacade getInstance(Context context) {
		if(INSTANCE == null)
			INSTANCE = new PerguntaRespostaFacade(context);
		
		return INSTANCE;
	}

	public List<Pergunta> buscarTodasPerguntas(Questionario questionario) {
		List<Pergunta> perguntas = PerguntaDAO.getInstance(context).findByTipoPesquisaId(questionario.getCodigo());
		for (Pergunta pergunta : perguntas) {
			pergunta.setOpcoes(buscarOpcoesPorPergunta(pergunta));
		}
		return perguntas;
	}
	
	public List<Resposta> buscarOpcoesPorPergunta(Pergunta pergunta) {
		return RespostaDAO.getInstance(context).pesquisaOpcoesPorPergunta(pergunta);
	}

}
