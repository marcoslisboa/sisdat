package com.frw.sisdat.negocio;

import java.util.List;

import android.content.Context;

import com.frw.sisdat.dao.UsuarioDAO;
import com.frw.sisdat.dominio.Usuario;

public class UsuarioFacade {

	private static UsuarioFacade INSTANCE;
	private Context context;
	
	private UsuarioFacade(Context context) {
		this.context = context;
	}
	
	public static UsuarioFacade getInstance(Context context) {
		if(INSTANCE == null)
			INSTANCE = new UsuarioFacade(context);
		
		return INSTANCE;
	}
	
	public void insereUsuarios(List<Usuario> usuarios) {
		for (Usuario u : usuarios) {
			insere(u);
		}
	}
	
	public void insere(Usuario u) {
		UsuarioDAO.getInstance(context).createOrUpdate(u);
	}

	public Usuario login(String username, String password) {
		return UsuarioDAO.getInstance(context).login(username, password);
	}

}
