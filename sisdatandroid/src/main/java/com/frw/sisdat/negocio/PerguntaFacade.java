package com.frw.sisdat.negocio;

import java.util.List;

import android.content.Context;

import com.frw.sisdat.dao.PerguntaDAO;
import com.frw.sisdat.dao.QuestionarioDAO;
import com.frw.sisdat.dao.RespostaDAO;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Questionario;
import com.frw.sisdat.dominio.Resposta;

public class PerguntaFacade {

	public static int SINGLE = 1; 
	public static int MULTIPLE = 2;

	private static PerguntaFacade INSTANCE;
	private Context context;
	
	private PerguntaFacade(Context context) {
		this.context = context;
	}
	
	public static PerguntaFacade getInstance(Context context) {
		if(INSTANCE == null)
			INSTANCE = new PerguntaFacade(context);
		
		return INSTANCE;
	}
	
	public void inserePerguntas(List<Pergunta> perguntas) {
		if ((perguntas != null) && (perguntas.size() > 0)) {
			for (Pergunta u : perguntas) {
				insere(u);
			}
		}
	}
	
	public void insere(Pergunta u) {
		Questionario questionario = QuestionarioDAO.getInstance(context).findByPK(u.getQuestionario().getCodigo());
		u.setQuestionario(questionario);
		PerguntaDAO.getInstance(context).createOrUpdate(u);
	}

	public void insereRespostas(List<Resposta> respostas) {
		for (Resposta u : respostas) {
			insere(u);
		}
	}
	
	public void insere(Resposta u) {
		Pergunta pergunta = PerguntaDAO.getInstance(context).findByPK(u.getPergunta().getId());
		u.setPergunta(pergunta);
		RespostaDAO.getInstance(context).createOrUpdate(u);
	}

}
