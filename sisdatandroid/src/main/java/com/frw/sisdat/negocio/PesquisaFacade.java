package com.frw.sisdat.negocio;

import java.util.Date;
import java.util.List;

import android.content.Context;

import com.frw.sisdat.dao.ColetaOpiniaoDAO;
import com.frw.sisdat.dao.PesquisaDAO;
import com.frw.sisdat.dao.PesquisaFotoDAO;
import com.frw.sisdat.dao.QuestionarioDAO;
import com.frw.sisdat.dominio.AbstractColeta;
import com.frw.sisdat.dominio.ColetaOpiniao;
import com.frw.sisdat.dominio.Pergunta;
import com.frw.sisdat.dominio.Pesquisa;
import com.frw.sisdat.dominio.PesquisaFoto;
import com.frw.sisdat.dominio.Questionario;
import com.frw.sisdat.dominio.Resposta;
import com.frw.sisdat.enumeration.TipoPerguntaEnum;
import com.frw.sisdat.ui.util.DateUtil;

public class PesquisaFacade {

	private static PesquisaFacade INSTANCE;
	private Context context;

	public static final String ABERTA = "ABERTA";
	public static final String FECHADA = "FECHADA";

	private PesquisaFacade(Context context) {
		this.context = context;
	}

	public static PesquisaFacade getInstance(Context context) {
		if (INSTANCE == null)
			INSTANCE = new PesquisaFacade(context);

		return INSTANCE;
	}

	/**
	 * 
	 * @param p
	 */
	public void insere(Pesquisa p) {

		Questionario questionario = QuestionarioDAO.getInstance(context).findByPK(p.getQuestionario().getCodigo());
		p.setQuestionario(questionario);
		PesquisaDAO.getInstance(context).createOrUpdate(p);
		if (p.fotos != null && !p.fotos.isEmpty()) {
			for (PesquisaFoto pf : p.fotos) {
				PesquisaFotoDAO.getInstance(context).createOrUpdate(pf);
			}
		}
	}

	/**
	 * 
	 * @param c
	 */
	public void insere(ColetaOpiniao c) {
		ColetaOpiniaoDAO.getInstance(context).createOrUpdate(c);
	}

	/**
	 * 
	 * @return
	 */
	public List<Pesquisa> findAll() {
		return PesquisaDAO.getInstance(context).findAll();
	}

	/**
	 * 
	 * @return
	 */
	public List<Pesquisa> findAllPesquisaNotSynchronied() {

		List<Pesquisa> list = PesquisaDAO.getInstance(context)
				.findByPesquisaNaoSincronizada();

		if ((list != null) && (list.size() > 0)) {

			for (Pesquisa p : list) {

				List<AbstractColeta> coletas = null;

				coletas = ColetaOpiniaoDAO.getInstance(context)
						.findByPesquisaId(p.getId());

				if (p != null) {
					p.fotos = PesquisaFotoDAO.getInstance(context).findByPesquisa(p);
				}
				p.setColetas(coletas);
			}
		}

		return list;
	}

	/**
	 * 
	 * @return
	 */
	public List<Pesquisa> findAllPesquisaSynchronied() {

		List<Pesquisa> list = PesquisaDAO.getInstance(context)
				.findByPesquisaSincronizada();

		if ((list != null) && (list.size() > 0)) {

			for (Pesquisa p : list) {

				List<AbstractColeta> coletas = null;

				// Obtem as coletas para cada tipo de pesquisa
				coletas = ColetaOpiniaoDAO.getInstance(context)
						.findByPesquisaId(p.getId());

				p.setColetas(coletas);
			}
		}

		return list;
	}

	/**
	 * 
	 * @param codigo
	 * @return
	 */
	public Pesquisa findUltimaAbertaByUsuarioTipo(String codigo,
			Questionario tipoPesquisa) {
		Pesquisa pesquisa = PesquisaDAO.getInstance(context).findUltimaAbertaByUsuarioTipo(
				codigo, tipoPesquisa.getCodigo());
		if (pesquisa != null) {
			pesquisa.fotos = PesquisaFotoDAO.getInstance(context).findByPesquisa(pesquisa);
		}
		
		return pesquisa;
	}

	/**
	 * 
	 * @param codigo
	 * @return
	 */
	public Pesquisa findUltimaByUsuarioTipo(String codigo,
			Questionario tipoPesquisa) {
		Pesquisa pesquisa = PesquisaDAO.getInstance(context).findUltimaByUsuarioTipo(codigo,
				tipoPesquisa.getCodigo());
		
		if (pesquisa != null) {
			pesquisa.fotos = PesquisaFotoDAO.getInstance(context).findByPesquisa(pesquisa);
		}
		
		return pesquisa;
	}

	/**
	 * 
	 * @param codigo
	 * @return
	 */
	public Pesquisa findUltimaAbertaByUsuario(String codigo) {
		Pesquisa pesquisa = PesquisaDAO.getInstance(context).findUltimaAbertaByUsuario(
				codigo);
		
		if (pesquisa != null) {
			pesquisa.fotos = PesquisaFotoDAO.getInstance(context).findByPesquisa(pesquisa);
		}
		
		return pesquisa;
	}

	/**
	 * 
	 * @param pesquisa
	 */
	public void Synchronize(Pesquisa pesquisa) {
		PesquisaDAO.getInstance(context).createOrUpdate(pesquisa);
		if (pesquisa.fotos != null && !pesquisa.fotos.isEmpty()) {
			for (PesquisaFoto pf : pesquisa.fotos) {
				PesquisaFotoDAO.getInstance(context).createOrUpdate(pf);
			}
		}
	}

	/**
	 * 
	 * @param pesquisa
	 */
	public void fecharPesquisaManualmente(Pesquisa pesquisa, String fechamento) {
		pesquisa.setDataFechamento(new Date());
		pesquisa.setStatus("FECHADA");
		pesquisa.setFechamento("F");
		PesquisaDAO.getInstance(context).createOrUpdate(pesquisa);
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public List<AbstractColeta> obterColetasOpiniaoByPesquisa(int id) {
		return ColetaOpiniaoDAO.getInstance(context).findByPesquisaId(id);
	}

	/**
	 * 
	 * @param pesquisa
	 */
	public void removePesquisa(Pesquisa pesquisa) {
		if (pesquisa.fotos != null && !pesquisa.fotos.isEmpty()) {
			for (PesquisaFoto pf : pesquisa.fotos) {
				PesquisaFotoDAO.getInstance(context).delete(pf);
			}
		}
		PesquisaDAO.getInstance(context).delete(pesquisa);
	}

	public void removePesquisaCompleta(Pesquisa pesquisa) {

		try {
			List<AbstractColeta> coletas = null;

			for (AbstractColeta c : pesquisa.getColetas())
				ColetaOpiniaoDAO.getInstance(context).delete((ColetaOpiniao) c);

			if (pesquisa.fotos != null && !pesquisa.fotos.isEmpty()) {
				for (PesquisaFoto pf : pesquisa.fotos) {
					PesquisaFotoDAO.getInstance(context).delete(pf);
				}
			}
			PesquisaDAO.getInstance(context).delete(pesquisa);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void salvaColetas(List<Pergunta> perguntas, Pesquisa pesquisa) {
		if ((perguntas != null) && (perguntas.size() > 0)) {
			for (Pergunta pergunta : perguntas) {
				if (TipoPerguntaEnum.CAMPO_LIVRE.getId() == pergunta.getTipo()) {
					// Cria uma coleta para a pesquisa
					ColetaOpiniao coletaOpiniao = new ColetaOpiniao();
					coletaOpiniao.setPesquisa(pesquisa);
					coletaOpiniao.setPergunta(pergunta);
					coletaOpiniao.setCampoLivre(pergunta.getCampoLivre());
					// Insere a coleta no banco de dados
					insere(coletaOpiniao);

				} else {
					if ((pergunta != null) && (pergunta.getRespostas() != null)) {
						for (Resposta resposta : pergunta.getRespostas()) {
							// Cria uma coleta para a pesquisa
							ColetaOpiniao coletaOpiniao = new ColetaOpiniao();
							coletaOpiniao.setPesquisa(pesquisa);
							coletaOpiniao.setPergunta(pergunta);
							coletaOpiniao.setResposta(resposta);
							coletaOpiniao.setCampoLivre(resposta.getCampoLivre());
							// Insere a coleta no banco de dados
							insere(coletaOpiniao);
						}
					}
				}
			}
		}
	}
	
	public void removeColetas(Pesquisa pesquisa) {
		try {
			for (AbstractColeta c : pesquisa.getColetas())
				ColetaOpiniaoDAO.getInstance(context).delete((ColetaOpiniao) c);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void remove(ColetaOpiniao coleta) {
		ColetaOpiniaoDAO.getInstance(context).delete(coleta);
	}

	public void removePesquisasPassadasNaData() {

		Date hoje = new Date();

		List<Pesquisa> lista = findAllPesquisaSynchronied();

		if ((lista != null) && (lista.size() > 0)) {

			for (Pesquisa p : lista) {

				if (p.getDataSincronizacao() != null) {

					Date d = DateUtil.addDays(p.getDataSincronizacao(), 30);

					if (hoje.after(d))
						removePesquisaCompleta(p);
				}
			}
		}

	}

	public void removePesquisasPassadas() {

		List<Pesquisa> lista = findAllPesquisaSynchronied();

		if ((lista != null) && (lista.size() > 0)) {

			for (Pesquisa p : lista)
				removePesquisaCompleta(p);
		}
	}
}
