package com.frw.sisdat.ws;

import com.frw.sisdat.ui.service.BaseService;
import com.google.gson.Gson;

import org.apache.http.NameValuePair;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class GsonHandler {

	public final Map<String, Object> dataMapper = new HashMap<String, Object>();	
	
	public GsonHandler() {
	}
	
	public Object getData(String key) {
		return dataMapper.get(key);
	}	

	public void parseAndApply(Gson gson, String resultado) throws JsonHandlerException {
		try {
			parse(gson, resultado);
		} catch (JsonHandlerException e) {
			throw e;
		} catch (IOException e) {
			throw new JsonHandlerException("Problema lendo resposta - "
					+ e.getMessage(), e);
		} catch (Exception e) {
			throw new JsonHandlerException("Problemas aplicando a operação de lote"+ e.getMessage(), e);
		}
	}

	public List<NameValuePair> getPostParameters() throws JSONException {
		return new ArrayList<NameValuePair>();
	}	
	
	public abstract void parse(Gson gson, String resultado) throws JsonHandlerException, IOException, JSONException;

	public static class JsonHandlerException extends IOException {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public boolean mostrarMensagem = true;
		public boolean deslogar = false;
		public boolean cancelado = false;
		
		public JsonHandlerException(String message) {
			super(message);
		}
		
		public JsonHandlerException(String message, Throwable cause) {
			super(message);
			initCause(cause);
			
			//Quando vier de outro exception não deve mostrar a mensagem de erro
			this.mostrarMensagem = false;
		}

		@Override
		public String toString() {
			if (getCause() != null) {
				return getLocalizedMessage() + ": " + getCause();
			} else {
				return getLocalizedMessage();
			}
		}
		
		public int getStatusMensagemReceiver() {
			if (deslogar)
				return BaseService.STATUS_DESLOGADO;
			else if (cancelado)
				return BaseService.STATUS_CANCELADO;
			else
				return BaseService.STATUS_ERRO;
		}		
		
	}
	
}
