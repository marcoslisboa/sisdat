package com.frw.sisdat.ws.extension.ksoap2;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.ksoap2.transport.ServiceConnection;
import org.kxml2.io.KXmlSerializer;
import org.xmlpull.v1.XmlSerializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.Proxy;

public class MyHttpTransport extends HttpTransportSE {
	
	/*Variáveis privadas da superclasse */
    private int bufferLength = ServiceConnection.DEFAULT_BUFFER_SIZE;
    private String xmlVersionTag = "";
	
	private boolean escapeParameters = true;

	public MyHttpTransport(Proxy proxy, String url, int timeout, int contentLength) {
		super(proxy, url, timeout, contentLength);
		this.bufferLength = contentLength;
	}

	public MyHttpTransport(Proxy proxy, String url, int timeout) {
		super(proxy, url, timeout);
	}

	public MyHttpTransport(Proxy proxy, String url) {
		super(proxy, url);
	}

	public MyHttpTransport(String url, int timeout, int contentLength) {
		super(url, timeout, contentLength);
		this.bufferLength = contentLength;		
	}

	public MyHttpTransport(String url, int timeout) {
		super(url, timeout);
	}

	public MyHttpTransport(String url) {
		super(url);
	}
	
    /**
     * Sets the version tag for the outgoing soap call. Example <?xml
     * version=\"1.0\" encoding=\"UTF-8\"?>
     * 
     * @param tag
     *            the xml string to set at the top of the soap message.
     */
    public void setXmlVersionTag(String tag) {
        super.setXmlVersionTag(tag);
    	xmlVersionTag = tag;
    }	
	
	public void setEscapeParameters(boolean escape) {
		this.escapeParameters = escape;
	}
	
	@Override
	protected byte[] createRequestData(SoapEnvelope envelope, String encoding) throws IOException {
        ByteArrayOutputStream bos = new ByteArrayOutputStream(bufferLength);
        byte result[] = null;
        bos.write(xmlVersionTag.getBytes());
        XmlSerializer xw = null;
        if(escapeParameters) {
        	xw = new KXmlSerializer();
        } else {
        	xw = new MyXmlSerializable();
        }
        xw.setOutput(bos, encoding);
        envelope.write(xw);
        xw.flush();
        bos.write('\r');
        bos.write('\n');
        bos.flush();
        result = bos.toByteArray();
        xw = null;
        bos = null;
        return result;		
	}
	
}
