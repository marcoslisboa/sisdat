package com.frw.sisdat.ws;

import android.content.ContentResolver;
import android.util.Log;

import com.frw.sisdat.ws.WSHandler.WSHandlerException;
import com.frw.sisdat.ws.extension.ksoap2.MyHttpTransport;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.MarshalDate;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;

import java.io.IOException;

public class RemoteExecutorSOAP {

	private final ContentResolver mResolver; 
	
	private static final String TAG = RemoteExecutorSOAP.class.getName();
	
	public RemoteExecutorSOAP(ContentResolver resolver) {

		mResolver = resolver;
	}

	public void execute(WSHandler handler) throws WSHandlerException {

		SoapObject request = new SoapObject(handler.soapNamespace, handler.soapName);

		Log.v(TAG, "Calling SOAP ACTION " + handler.soapAction + "...");		
		
		for (final ParametroWS p : handler.getParameters()) {
			request.addProperty(p.nome, p.valor);
		}

		final SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
		envelope.dotNet = false;
		envelope.setOutputSoapObject(request);

		MarshalDate marshalDate = new MarshalDate();
		marshalDate.register(envelope);
		
		MarshalFloat mashalFloat = new MarshalFloat();
		mashalFloat.register(envelope);
		
		handler.registerObject(envelope);
		
		final MyHttpTransport androidHttpTransport = new MyHttpTransport(handler.wsUrl);
		androidHttpTransport.setEscapeParameters(false);
     	androidHttpTransport.debug = true;

		try {
			long inicio = System.currentTimeMillis();
			
			androidHttpTransport.call(handler.soapAction, envelope);
			
			Log.v(TAG, "SOAP ACTION ["+handler.soapAction+"] execution lasts " + (System.currentTimeMillis() - inicio) + " ms...");
			
			handler.parseAndApply(envelope.bodyIn, mResolver);

			Log.v(TAG, "Calling SOAP ACTION " + handler.soapAction + " sucessfully.");
			
		} catch (WSHandlerException e) {
			throw e;
		} catch (final IOException e) {
			e.printStackTrace();
			throw new WSHandlerException(
					"Serviço temporariamente indisponível", e);
		} catch (final Exception e) {
			e.printStackTrace();
			throw new WSHandlerException("Erro acessando serviço", e);
		}

	}

}
