package com.frw.sisdat.ws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.json.JSONException;
import org.json.JSONObject;

import com.frw.sisdat.ws.JsonHandler.JsonHandlerException;
import com.frw.sisdat.ws.WSHandler.WSHandlerException;

import android.content.ContentResolver;


public class RemoteExecutorJSON {

	private final ContentResolver mResolver; 

	public RemoteExecutorJSON(ContentResolver resolver) {

		mResolver = resolver;
	}
	
	public void executeGet(String url, JsonHandler handler)
			throws JsonHandlerException, WSHandlerException {
		final HttpUriRequest request = new HttpGet(url);
		execute(request, handler);
	}

	public void executePost(String url, JsonHandler handler)
			throws JsonHandlerException, WSHandlerException {
		final HttpUriRequest request = new HttpPost(url);
		execute(request, handler);
	}

	public void execute(HttpUriRequest request, JsonHandler handler) throws WSHandlerException, JsonHandlerException {
		
		HttpClient httpClientTemp = null;
		
		try {

			httpClientTemp = HttpClientFactory.getHttpClient();
			httpClientTemp.getParams().setIntParameter("http.connection.timeout", 60000);
			
			if (request instanceof HttpPost) {
				((HttpPost) request).setEntity(new UrlEncodedFormEntity(handler
						.getPostParameters()));
			}

			final HttpResponse resp = httpClientTemp.execute(request);
			final int status = resp.getStatusLine().getStatusCode();
		
			if (status != HttpStatus.SC_OK) {
				throw new JsonHandlerException(
						"Resposta inexperada do servidor "
								+ resp.getStatusLine() + " para "
								+ request.getRequestLine());
			}

			InputStream input = null;
			try {
				input = resp.getEntity().getContent();
				String resultado = concatenaStream(input);
				//resultado = new String(Base64.decode(resultado), "UTF8");

				handler.parseAndApply(new JSONObject(resultado));

			} catch (JSONException e) {
				throw new JsonHandlerException("Resposta mal formada para "
						+ request.getRequestLine(), e);
			} finally {
				if (input != null)
					input.close();
			}
		} catch (JsonHandlerException e) {
			throw e;
		} catch (Exception e) {
			throw new JsonHandlerException(
					"Problema lendo a resposta remota para "
							+ request.getRequestLine(), e);
		} finally {
		}

	}
	
	
	private String concatenaStream(InputStream is) {
		/*
		 * 
		 * To convert the InputStream to String we use the
		 * BufferedReader.readLine()
		 * 
		 * method. We iterate until the BufferedReader return null which means
		 * 
		 * there's no more data to read. Each line will appended to a
		 * StringBuilder
		 * 
		 * and returned as String.
		 */

		BufferedReader reader = new BufferedReader(new InputStreamReader(is),
				8192);

		StringBuilder sb = new StringBuilder();
		String line = null;

		try {

			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}

		} catch (IOException e) {
			// e.printStackTrace();

		}

		return sb.toString();
	}

}
