package com.frw.sisdat.ws;

import android.content.ContentResolver;

import com.frw.sisdat.ui.service.BaseService;

import org.ksoap2.serialization.SoapSerializationEnvelope;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class WSHandler {

	public final String soapNamespace;
	public final String soapName;
	public final String soapAction;
	public final String wsUrl;
	public final Map<String, Object> dataMapper = new HashMap<String, Object>();
	
	public WSHandler(String soapNamespace, String soapName, String soapAction, String wsUrl) {
		
		this.soapNamespace = soapNamespace;
		this.soapName = soapName;
		this.soapAction = soapAction;
		this.wsUrl = wsUrl;
	}

	public Object getData(String key) {
		return dataMapper.get(key);
	}
	
	public void parseAndApply(Object objectResponse, ContentResolver resolver)
			throws WSHandlerException {
		try {
			parse(objectResponse, resolver);

		} catch (WSHandlerException e) {
			throw e;
		} catch (IOException e) {
			throw new WSHandlerException(
					"Serviço temporariamente indisponível", e);
		}
	}

	public abstract void parse(Object objectResponse, ContentResolver resolver)
			throws WSHandlerException, IOException;
	
	public abstract void registerObject(SoapSerializationEnvelope envelope);

	public List<ParametroWS> getParameters() {

		return new ArrayList<ParametroWS>();
	}

	public static class WSHandlerException extends IOException {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public boolean mostrarMensagem = true;
		public boolean deslogar = false;
		public boolean cancelado = false;

		public WSHandlerException(String message) {
			super(message);
		}

		public WSHandlerException(String message, Throwable cause) {
			super(message);
			initCause(cause);

			// Quando vier de outro exception não deve mostrar a mensagem de
			// erro
			this.mostrarMensagem = false;
		}

		@Override
		public String toString() {
			if (getCause() != null) {
				return getLocalizedMessage() + ": " + getCause();
			} else {
				return getLocalizedMessage();
			}
		}

		public int getStatusMensagemReceiver() {
			if (deslogar)
				return BaseService.STATUS_DESLOGADO;
			else if (cancelado)
				return BaseService.STATUS_CANCELADO;
			else
				return BaseService.STATUS_ERRO;
		}
	}
}
