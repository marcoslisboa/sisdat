package com.frw.sisdat.ws;

public class ParametroWS {

	public String nome;
	public Object valor;
	
	public ParametroWS(String nome, Object valor){
		this.nome = nome;
		this.valor = valor;
	}
	
}
