package com.frw.sisdat.ws;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.apache.http.client.HttpClient;
import org.apache.http.client.params.HttpClientParams;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;

public class HttpClientFactory {

	private static HttpClient mHttpClient = null;

	public static HttpClient getHttpClient() {

		if (mHttpClient != null)
			return mHttpClient;
 
		try {
			Class<?> ClassTemp = Class
					.forName("android.net.http.AndroidHttpClient");
			Method MethodTemp = ClassTemp.getMethod("newInstance",
					new Class[] { String.class });
			mHttpClient = (HttpClient) MethodTemp.invoke(null, "Android");

		} catch (ClassNotFoundException e) {

		} catch (SecurityException e) {

		} catch (NoSuchMethodException e) {

		} catch (InvocationTargetException e) {

		} catch (IllegalAccessException e) {

		}

		if (mHttpClient == null) {
			mHttpClient = InicializaDefaultHttpClient();
		}

		return mHttpClient;
	}

	private static HttpClient InicializaDefaultHttpClient() {
		HttpParams params = new BasicHttpParams();

		// Turn off stale checking. Our connections break all the time anyway,
		// and it's not worth it to pay the penalty of checking every time.
		HttpConnectionParams.setStaleCheckingEnabled(params, false);

		// Default connection and socket timeout of 20 seconds. Tweak to taste.
		HttpConnectionParams.setConnectionTimeout(params, 20 * 1000);
		HttpConnectionParams.setSoTimeout(params, 20 * 1000);
		HttpConnectionParams.setSocketBufferSize(params, 8192);

		// Don't handle redirects -- return them to the caller. Our code
		// often wants to re-POST after a redirect, which we must do ourselves.
		HttpClientParams.setRedirecting(params, false);

		// Set the specified user agent and register standard protocols.
		HttpProtocolParams.setUserAgent(params, "Android");
		SchemeRegistry schemeRegistry = new SchemeRegistry();
		schemeRegistry.register(new Scheme("http", PlainSocketFactory
				.getSocketFactory(), 80));
		schemeRegistry.register(new Scheme("https", SSLSocketFactory
				.getSocketFactory(), 443));
		ClientConnectionManager manager = new ThreadSafeClientConnManager(
				params, schemeRegistry);
		
		return new DefaultHttpClient(manager, params);
	}

}
