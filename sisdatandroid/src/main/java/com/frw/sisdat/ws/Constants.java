package com.frw.sisdat.ws;

public class Constants {

	public static final String NAMESPACE = "http://interfaces.webservice.web.suprasoft.frw.com/";
	
	public static String URL_CLIENTE_WS = "http://10.10.101.8:8080/suprasoft/ClienteWS";
	public static String URL_USUARIO_WS = "http://10.10.101.8:8080/suprasoft/UsuarioWS";
	
	public static final String METHOD_NAME_BUSCA_TODOS_CLIENTES = "BuscarTodos";
	public static final String SOAP_ACTION_BUSCA_TODOS_CLIENTES = "http://interfaces.webservice.web.suprasoft.frw.com/BuscarTodos";
	
	public static final String METHOD_NAME_PRE_CADASTRO_CLIENTES = "RealizarPreCadastroCliente";
	public static final String SOAP_ACTION_PRE_CADASTRO_CLIENTES = "http://interfaces.webservice.web.suprasoft.frw.com/RealizarPreCadastroCliente";	
	
	public static final String METHOD_NAME_BUSCA_USUARIOS = "BuscarTodosUsuarios";
	public static final String SOAP_ACTION_BUSCA_USUARIOS = "http://interfaces.webservice.web.suprasoft.frw.com/BuscarTodosUsuarios";	

}