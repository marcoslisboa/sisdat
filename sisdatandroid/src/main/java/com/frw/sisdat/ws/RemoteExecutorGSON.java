package com.frw.sisdat.ws;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;

import android.content.ContentResolver;

import com.frw.sisdat.ws.GsonHandler.JsonHandlerException;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class RemoteExecutorGSON {

	private final ContentResolver mResolver; 

	public RemoteExecutorGSON(ContentResolver resolver) {

		mResolver = resolver;
	}
	
	public void executeGet(String url, GsonHandler handler) throws JsonHandlerException {
		final HttpUriRequest request = new HttpGet(url);
		execute(request, handler);
	}

	public void executePost(String url, GsonHandler handler)
			throws JsonHandlerException {
		final HttpUriRequest request = new HttpPost(url);
		execute(request, handler);
	}

	public void execute(HttpUriRequest request, GsonHandler handler) throws JsonHandlerException {
		
		HttpClient httpClientTemp = null;
		try {
			httpClientTemp = HttpClientFactory.getHttpClient();
			httpClientTemp.getParams().setIntParameter("http.connection.timeout", 60000);
			
			if (request instanceof HttpPost) {
				((HttpPost) request).setEntity(new UrlEncodedFormEntity(handler.getPostParameters()));
			} 

			final HttpResponse resp = httpClientTemp.execute(request);
			final int status = resp.getStatusLine().getStatusCode();
			if (status != HttpStatus.SC_OK) {
				throw new JsonHandlerException(
						"Resposta inexperada do servidor "
								+ resp.getStatusLine() + " para "
								+ request.getRequestLine());
			}

			InputStream input = null;
			try {
				input = resp.getEntity().getContent();
				String resultado = concatenaStream(input);
				//resultado = new String(Base64.decode(resultado), "UTF8");

				Gson gson = new Gson();
				
				handler.parseAndApply(gson, resultado);

			} catch (JsonSyntaxException e) {
				throw new JsonHandlerException("Resposta mal formada para " + request.getRequestLine(), e);
			} finally {
				if (input != null)
					input.close();
			}
		} catch (JsonHandlerException e) {
			throw e;
		} catch (Exception e) {
			throw new JsonHandlerException(
					"Problema lendo a resposta remota para "
							+ request.getRequestLine(), e);
		} finally {
//			if (httpClientTemp != null) {
//				if (httpClientTemp instanceof AndroidHttpClient) {
//					AndroidHttpClient androidHttpClient = (AndroidHttpClient)httpClientTemp;
//					androidHttpClient.close();
//				}
//			}
		}

	}
	
	
	private String concatenaStream(InputStream is) {
		/*
		 * 
		 * To convert the InputStream to String we use the
		 * BufferedReader.readLine()
		 * 
		 * method. We iterate until the BufferedReader return null which means
		 * 
		 * there's no more data to read. Each line will appended to a
		 * StringBuilder
		 * 
		 * and returned as String.
		 */

		BufferedReader reader = new BufferedReader(new InputStreamReader(is),
				8192);

		StringBuilder sb = new StringBuilder();
		String line = null;

		try {

			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}

		} catch (IOException e) {
			// e.printStackTrace();

		}

		return sb.toString();
	}

}
