package com.frw.sisdat.ws;

import org.apache.http.NameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class JsonHandler {

	public JsonHandler() {
	}

	public void parseAndApply(JSONObject pJSONObject) throws JsonHandlerException {
		try {
			parseAndInsert(pJSONObject);
		} catch (JsonHandlerException e) {
			throw e;
		} catch (JSONException e) {
			throw new JsonHandlerException("Problema analisando a resposta - "
					+ e.getMessage(), e);
		} catch (IOException e) {
			throw new JsonHandlerException("Problema lendo resposta - "
					+ e.getMessage(), e);
		} catch (Exception e) {
			throw new JsonHandlerException("Problemas aplicando a operação de lote"+ e.getMessage(), e);
		}
	}

	public List<NameValuePair> getPostParameters() throws JSONException {
		return new ArrayList<NameValuePair>();
	}	
	
	public abstract void parseAndInsert(JSONObject pJSONObject) throws JsonHandlerException, IOException, JSONException;

	public static class JsonHandlerException extends IOException {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public boolean mostrarMensagem = true;
		public boolean deslogar = false;
		public boolean cancelado = false;
		
		public JsonHandlerException(String message) {
			super(message);
		}
		
		public JsonHandlerException(String message, Throwable cause) {
			super(message);
			initCause(cause);
			
			//Quando vier de outro exception não deve mostrar a mensagem de erro
			this.mostrarMensagem = false;
		}

		@Override
		public String toString() {
			if (getCause() != null) {
				return getLocalizedMessage() + ": " + getCause();
			} else {
				return getLocalizedMessage();
			}
		}
		
	}
	
}
