package com.frw.sisdat.ws.dominio;

import java.util.Date;

public class PesquisaWSDTO {

	private int id;
//	private UsuarioWSDTO usuario;
//	private TipoPesquisaWSDTO tipoPesquisa;
//	private VeiculoWSDTO carro;
//	private RotaWSDTO linha;
	private Date dataAbertura;
	private Date dataFehamento;
	private Date dataSincronizacao;
	private double latitudeInicial;
	private double longitudeInicial;
	private double latitudeFinal;
	private double longitudeFinal;
//	private PortaWSDTO porta;
//	private List<ColetaEmbarqueDesembarqueWSDTO> listaColetas;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDataAbertura() {
		return dataAbertura;
	}
	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
	public Date getDataFehamento() {
		return dataFehamento;
	}
	public void setDataFehamento(Date dataFehamento) {
		this.dataFehamento = dataFehamento;
	}
	public Date getDataSincronizacao() {
		return dataSincronizacao;
	}
	public void setDataSincronizacao(Date dataSincronizacao) {
		this.dataSincronizacao = dataSincronizacao;
	}
	public double getLatitudeInicial() {
		return latitudeInicial;
	}
	public void setLatitudeInicial(double latitudeInicial) {
		this.latitudeInicial = latitudeInicial;
	}
	public double getLongitudeInicial() {
		return longitudeInicial;
	}
	public void setLongitudeInicial(double longitudeInicial) {
		this.longitudeInicial = longitudeInicial;
	}
	public double getLatitudeFinal() {
		return latitudeFinal;
	}
	public void setLatitudeFinal(double latitudeFinal) {
		this.latitudeFinal = latitudeFinal;
	}
	public double getLongitudeFinal() {
		return longitudeFinal;
	}
	public void setLongitudeFinal(double longitudeFinal) {
		this.longitudeFinal = longitudeFinal;
	}


}
