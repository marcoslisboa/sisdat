package com.frw.sisdat.dominio.cadastro;

import android.content.Context;

import com.frw.sisdat.dao.UrlWSDAO;
import com.frw.sisdat.dominio.IEntidade;
import com.j256.ormlite.field.DatabaseField;

public class UrlWS implements IEntidade{

	@DatabaseField(id = true)
	public Long codigo;
	
	@DatabaseField
	public String ip;

	public UrlWS(Long codigo, String ip) {
		this.codigo = codigo;
		this.ip = ip;
	}

	public UrlWS(){}

	public static void createDefault(Context ctx) {
		UrlWS ws = new UrlWS(1l, "http://54.233.100.229:9090/sisdat-web/");
		UrlWSDAO.getInstance(ctx).createOrUpdate(ws);
	}

}
