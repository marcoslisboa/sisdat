package com.frw.sisdat.dominio;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.graphics.Bitmap;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Pesquisa implements IEntidade {

	@DatabaseField(allowGeneratedIdInsert=true, generatedId=true)
	private Integer id;

	@DatabaseField
	private Date dataAbertura;

	@DatabaseField
	private Date dataFechamento;

	@DatabaseField
	private double latitudeInicial;
	
	@DatabaseField
	private double longitudeInicial;

	@DatabaseField
	private double latitudeFinal;
	
	@DatabaseField
	private double longitudeFinal;
	
	@DatabaseField
	private Date dataSincronizacao;
	
	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=false)
	private Usuario usuario;
	
	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=false)
	private Questionario questionario;

	@DatabaseField
	private String status;
	
	@DatabaseField
	private String device;

	@DatabaseField
	private String fechamento;

	@DatabaseField
	private String numero;
	
	@DatabaseField
	private String observacao;
	
	public List<PesquisaFoto> fotos = new ArrayList<PesquisaFoto>();

	private List<AbstractColeta> coletas = new ArrayList<AbstractColeta>();
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Date getDataAbertura() {
		return dataAbertura;
	}
	public void setDataAbertura(Date dataAbertura) {
		this.dataAbertura = dataAbertura;
	}
	public Date getDataFechamento() {
		return dataFechamento;
	}
	public void setDataFechamento(Date dataFechamento) {
		this.dataFechamento = dataFechamento;
	}
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public List<AbstractColeta> getColetas() {
		return coletas;
	}
	public void setColetas(List<AbstractColeta> coletas) {
		this.coletas = coletas;
	}
	public double getLatitudeInicial() {
		return latitudeInicial;
	}
	public void setLatitudeInicial(double latitudeInicial) {
		this.latitudeInicial = latitudeInicial;
	}
	public double getLongitudeInicial() {
		return longitudeInicial;
	}
	public void setLongitudeInicial(double longitudeInicial) {
		this.longitudeInicial = longitudeInicial;
	}
	public double getLatitudeFinal() {
		return latitudeFinal;
	}
	public void setLatitudeFinal(double latitudeFinal) {
		this.latitudeFinal = latitudeFinal;
	}
	public double getLongitudeFinal() {
		return longitudeFinal;
	}
	public void setLongitudeFinal(double longitudeFinal) {
		this.longitudeFinal = longitudeFinal;
	}
	public Date getDataSincronizacao() {
		return dataSincronizacao;
	}
	public void setDataSincronizacao(Date dataSincronizacao) {
		this.dataSincronizacao = dataSincronizacao;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Questionario getQuestionario() {
		return questionario;
	}
	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}
	public String getDevice() {
		return device;
	}
	public void setDevice(String device) {
		this.device = device;
	}
	public String getFechamento() {
		return fechamento;
	}
	public void setFechamento(String fechamento) {
		this.fechamento = fechamento;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getObservacao() {
		return observacao;
	}
	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}
}
