package com.frw.sisdat.dominio;

import com.j256.ormlite.field.DatabaseField;

public class ColetaOpiniao extends AbstractColeta {

	@DatabaseField(allowGeneratedIdInsert=true, generatedId=true)
	private int id;
	
	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=false)
	private Pesquisa pesquisa;
	
	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=false)
	private Pergunta pergunta;
	
	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=true)
	private Resposta resposta;

	@DatabaseField
	private String campoLivre;

	public String getCampoLivre() {
		return campoLivre;
	}

	public void setCampoLivre(String campoLivre) {
		this.campoLivre = campoLivre;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Pesquisa getPesquisa() {
		return pesquisa;
	}

	public void setPesquisa(Pesquisa pesquisa) {
		this.pesquisa = pesquisa;
	}

	public Pergunta getPergunta() {
		return pergunta;
	}

	public void setPergunta(Pergunta pergunta) {
		this.pergunta = pergunta;
	}

	public Resposta getResposta() {
		return resposta;
	}

	public void setResposta(Resposta resposta) {
		this.resposta = resposta;
	}
}
