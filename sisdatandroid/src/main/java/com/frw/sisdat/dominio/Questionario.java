package com.frw.sisdat.dominio;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class Questionario implements IEntidade {

	@DatabaseField(id = true)
	private String codigo;
	
	@DatabaseField()
    private String descricao;
	
	@DatabaseField()
	private boolean ativo;

	@DatabaseField()
	private Integer ordem;

	@DatabaseField()
	private String resumo;

	@DatabaseField()
	private String codigoQuestionario;

	@DatabaseField()
	private String versao;

	@DatabaseField()
	private String orientacao;
	
	public Questionario() {
	}

	public Questionario(String codigo) {
		this.codigo = codigo;
	}
	
	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean getAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public String getResumo() {
		return resumo;
	}

	public void setResumo(String resumo) {
		this.resumo = resumo;
	}

	public String getCodigoQuestionario() {
		return codigoQuestionario;
	}

	public void setCodigoQuestionario(String codigoQuestionario) {
		this.codigoQuestionario = codigoQuestionario;
	}

	public String getVersao() {
		return versao;
	}

	public void setVersao(String versao) {
		this.versao = versao;
	}

	public String getOrientacao() {
		return orientacao;
	}

	public void setOrientacao(String orientacao) {
		this.orientacao = orientacao;
	}

}
