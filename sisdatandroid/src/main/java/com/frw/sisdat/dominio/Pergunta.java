package com.frw.sisdat.dominio;

import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.field.DatabaseField;

public class Pergunta implements IEntidade{
	
	@DatabaseField(id = true)
	private Long id;
	
	@DatabaseField
	private String descricao;

	@DatabaseField
	private String subDescricao;
	
	@DatabaseField
	private int ordem;

	@DatabaseField
	private int tipo;
	
	@DatabaseField
	private Integer dataType;

	@DatabaseField
	private Double precisaoInicial;

	@DatabaseField
	private Double precisaoFinal;

	@DatabaseField
	private String comentario;

	@DatabaseField
	private Boolean obrigatoria;

	@DatabaseField
	private String descricaoResumida;

	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=false)
	private Questionario questionario;
	
	private List<Resposta> respostas = new ArrayList<Resposta>();

	private List<Resposta> opcoes;
	
	private String campoLivre;

	public List<Resposta> getOpcoes() {
		if (opcoes == null)
			opcoes = new ArrayList<Resposta>();
		return opcoes;
	}

	public void setOpcoes(List<Resposta> respostas) {
		this.opcoes = respostas;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getSubDescricao() {
		return subDescricao;
	}

	public void setSubDescricao(String subDescricao) {
		this.subDescricao = subDescricao;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public Questionario getQuestionario() {
		return questionario;
	}

	public void setQuestionario(Questionario questionario) {
		this.questionario = questionario;
	}

	public List<Resposta> getRespostas() {
		return respostas;
	}

	public void setRespostas(List<Resposta> respostas) {
		this.respostas = respostas;
	}

	public int getTipo() {
		return tipo;
	}

	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	
	public String getCampoLivre() {
		return campoLivre;
	}

	public void setCampoLivre(String campoLivre) {
		this.campoLivre = campoLivre;
	}

	public Integer getDataType() {
		return dataType;
	}

	public void setDataType(Integer dataType) {
		this.dataType = dataType;
	}

	public Double getPrecisaoInicial() {
		return precisaoInicial;
	}

	public void setPrecisaoInicial(Double precisaoInicial) {
		this.precisaoInicial = precisaoInicial;
	}

	public Double getPrecisaoFinal() {
		return precisaoFinal;
	}

	public void setPrecisaoFinal(Double precisaoFinal) {
		this.precisaoFinal = precisaoFinal;
	}

	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Boolean getObrigatoria() {
		return obrigatoria;
	}

	public void setObrigatoria(Boolean obrigatoria) {
		this.obrigatoria = obrigatoria;
	}

	public String getDescricaoResumida() {
		return descricaoResumida;
	}

	public void setDescricaoResumida(String descricaoResumida) {
		this.descricaoResumida = descricaoResumida;
	}
	
}