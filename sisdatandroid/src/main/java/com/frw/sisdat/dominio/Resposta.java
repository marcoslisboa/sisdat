package com.frw.sisdat.dominio;

import com.j256.ormlite.field.DatabaseField;

public class Resposta implements IEntidade{

	@DatabaseField(id = true)
	private Long id;
	
	@DatabaseField
	private String descricao;
	
	@DatabaseField
	private int ordem;
	
	@DatabaseField
	private String valor;
	
	@DatabaseField
	private Boolean correta;

	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=false)
	private Pergunta pergunta;

	private String campoLivre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getOrdem() {
		return ordem;
	}

	public void setOrdem(int ordem) {
		this.ordem = ordem;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

	public Pergunta getPergunta() {
		return pergunta;
	}

	public void setPergunta(Pergunta pergunta) {
		this.pergunta = pergunta;
	}

	public Boolean getCorreta() {
		return correta;
	}

	public void setCorreta(Boolean correta) {
		this.correta = correta;
	}

	public String getCampoLivre() {
		return campoLivre;
	}

	public void setCampoLivre(String campoLivre) {
		this.campoLivre = campoLivre;
	}

}
