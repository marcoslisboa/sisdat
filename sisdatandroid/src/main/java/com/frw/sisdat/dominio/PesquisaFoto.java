package com.frw.sisdat.dominio;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable
public class PesquisaFoto implements IEntidade {

	@DatabaseField(allowGeneratedIdInsert=true, generatedId=true)
	private Integer id;

	@DatabaseField(dataType=com.j256.ormlite.field.DataType.BYTE_ARRAY)
	private byte[] foto;

	@DatabaseField(foreign=true, foreignAutoRefresh=true, canBeNull=false, columnName="pesquisa")
	private Pesquisa pesquisa;
	
	public PesquisaFoto() {
		// TODO Auto-generated constructor stub
	}

	public PesquisaFoto(byte[] foto, Pesquisa pesquisa) {
		super();
		this.foto = foto;
		this.pesquisa = pesquisa;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public Pesquisa getPesquisa() {
		return pesquisa;
	}

	public void setPesquisa(Pesquisa pesquisa) {
		this.pesquisa = pesquisa;
	}
}
