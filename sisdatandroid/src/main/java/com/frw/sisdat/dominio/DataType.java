package com.frw.sisdat.dominio;

public class DataType {

	public static final int INTEIRO = 1;
	public static final int TEXTO = 2;
	public static final int FRACIONARIO = 3;
	public static final int DATA = 4;

}
